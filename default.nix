{ stdenv, lib, callPackage, makeWrapper, librsvg, pdf2svg, pdftk, poppler_utils, haskellPackages, xz
}:

let zauboard-web-client = callPackage ./web-client/default.nix {};
    zauboard-server = callPackage ./server/default.nix { zauboard-web-client = zauboard-web-client;};
in zauboard-server
