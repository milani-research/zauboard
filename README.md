# Zauboard
![Zauboard.](public/images/Zauboard-logo.svg?raw=true "Zauboard logo.")

A collaborative whiteboard web application that works like magic.

## Some Features
* Standard drawing functions: pen, stroke eraser, undo, basic shapes...
* Visible cursors can be used as pointers.
* Infinite whiteboard.
* Each whiteboard can contain several pages.
* PDF import and export.
* Whiteboards are automatically saved to disk on the server.
* Compatible with standard browsers.

## Installation

To launch zauboard with the default setup, run:

1. Install `ghc`, `cabal-install`, `pdf2svg` and `rsvg-convert` and make sure they are on your `PATH`.
2. Run `cabal run zauboard -- --public-directory ../web-client/public` inside the `server` folder.
3. Change settings on `~/.config/zauboard/config` (requires service restart to take effect).
4. Visit http://localhost:8080

You can add users through the command line (see `zauboard --help` for more information).
For example, to add the user `merlin` with password `opensesame`, run `cabal run zauboard -- create-user merlin set-password merlin opensesame`.

### Basic nginx configuration

If you want `http://localhost/whiteboard/app` to redirect to the whiteboard, you can use the following nginx configuration (`/etc/nginx/sites-enabled/your-site` on linux):

    http {
        server {
            listen 0.0.0.0:80 ;
            listen [::]:80 ;
            server_name localhost ;
            location /whiteboard/app/ {
                proxy_set_header HOST $host;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection upgrade;
                proxy_pass http://localhost:8080/;
            }
        }
    }

To use `https` instead of `http`, replace the initial lines with

    https {
        server {
            listen 0.0.0.0:443 ;
            listen [::]:443 ;
