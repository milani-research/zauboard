{ mkDerivation, aeson, base, base64-bytestring, bytestring
, case-insensitive, containers, cryptonite, descrilo, directory
, filepath, hasql, http-types, HUnit, lib, mtl, network
, network-uri, process, sqlite-simple, strict-concurrency, text
, time, transformers, unordered-containers, utf8-string, vector
, wai, wai-websockets, warp, websockets, poppler_utils, pdf2svg, librsvg, pdftk
}:
mkDerivation {
  pname = "zauboard";
  version = "2.4.0.1";
  src = ./.;
  configureFlags = [ "--ghc-options=-threaded" ];
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    aeson base base64-bytestring bytestring case-insensitive containers
    cryptonite descrilo directory filepath hasql http-types mtl
    network-uri process sqlite-simple strict-concurrency text time
    transformers unordered-containers utf8-string vector wai
    wai-websockets warp websockets poppler_utils pdf2svg librsvg pdftk
  ];
  executableHaskellDepends = [ base ];
  testHaskellDepends = [
    base base64-bytestring bytestring containers directory HUnit mtl
    network text time transformers websockets poppler_utils pdf2svg librsvg pdftk
  ];
  description = "A collaborative whiteboard";
  license = lib.licenses.gpl3Only;
}
