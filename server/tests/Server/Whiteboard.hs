module Main where

import Zauboard.Whiteboard
import qualified Zauboard.Database as D
import Zauboard

import Control.Monad.Except
import Control.Monad (mplus, when, forM)
import Control.Concurrent.MVar
import Control.Monad.Trans.State
import Data.Time.Calendar
import Data.Time.Clock
import Data.Time.Clock.System
import qualified Data.Map as M
import System.Directory
import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)

tests = TestList                         
  [ TestLabel "Insert, erase, restore" $ TestCase
    ( do
      let dbName = "tests/Server/test-wb-0.db"
          t0 = systemToUTCTime MkSystemTime{systemSeconds = 0, systemNanoseconds = 0}
      doesFileExist dbName >>= flip when (removeFile dbName)
      eSQL <- runExceptT $ D.openSQLite dbName
      runDatabase dbName eSQL $ \sql -> do
        wb <- fmap (\w -> w{cxPageDir = "tests/Server/whiteboards-0"}) $ create sql
        let wb0 = WhiteboardPageID 0 1
        evalStateT (accessWhiteboard "Key" 1) wb
          >>= assertEqual "No whiteboard" Nothing
        evalStateT (info "Key") wb
          >>= assertEqual "No whiteboard" Nothing
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "Empty whiteboard" []
        now <- getCurrentTime
        D.createWhiteboard "My board" "Key" 0 t0 True sql
        evalStateT (accessWhiteboard "Key" 1) wb
          >>= assertEqual "whiteboard" (Just $ WhiteboardPageID 0 1)
        evalStateT (listPages "Key") wb
          >>= assertEqual "whiteboard" ([WhiteboardPageID 0 1])
        evalStateT (info "Key") wb
          >>= assertEqual "whiteboard" (Just
                          (WhiteboardInfo
                            { wiName = "My board"
                            , wiOwner = Nothing
                            , wiKey = "Key"
                            , wiModified = t0
                            , wiPublic = True
                            , wiPages = 1
                            , wiArchived = False
                            }
                          , WhiteboardTopLevelID 0))
        let d0 = Drawing (id0, c0)
            c0 = Circle (0,0) 5 "#FF0000" 5
            id0 = ElementID 0 0
        evalStateT (withWhiteboard (insert [d0]) wb0) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0" ([Drawing (ElementID 0 0, c0)])
        let d1 = Drawing (id1, p0)
            p0 = Path [(0,0), (1,1)] [4] "#FF0000" (Just ((0,0), (1,1)))
            id1 = ElementID 0 1
        evalStateT (withWhiteboard (insert [d1]) wb0) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0, P0" ([Drawing (id0, c0), Drawing (id1, p0)])
        evalStateT (withWhiteboard (erase [id0]) wb0) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "P0" ([Drawing (id1, p0)])
        evalStateT (withWhiteboard (restore [id0]) wb0) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0, P0" ([Drawing (id0, c0), Drawing (id1, p0)])
        evalStateT (withWhiteboard (takeDrawings [id0]) wb0) wb
          >>= assertEqual "C0" ([Drawing (id0, c0)], [])
        evalStateT (withWhiteboard clear wb0) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "empty board" []
    )
  , TestLabel "Store, load, unload, no xz" $ TestCase
    ( do
      let dbName = "tests/Server/test-wb-1.db"
      doesFileExist dbName >>= flip when (removeFile dbName)
      eSQL <- runExceptT $ D.openSQLite dbName
      runDatabase dbName eSQL $ \sql -> do
        wb <- fmap (\w -> w{cxXZCompression = False, cxPageDir = "tests/Server/whiteboards-1"}) $ create sql
        let wb0 = WhiteboardPageID 0 1
            d0 = Drawing (id0, c0)
            c0 = Circle (0,0) 5 "#FF0000" 5
            id0 = ElementID 0 0
            d1 = Drawing (id1, p0)
            p0 = Path [(0,0), (1,1)] [4] "#FF0000" (Just ((0,0), (1,1)))
            id1 = ElementID 0 1
        evalStateT (store wb0 [d0,d1]) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0, P0" ([Drawing (id0, c0), Drawing (id1, p0)])
        evalStateT (unload wb0) wb
        readMVar (cxPages wb)
          >>= assertEqual "No pages" 0 . M.size
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0, P0" ([Drawing (id0, c0), Drawing (id1, p0)])
    )
  , TestLabel "Store, load, unload, xz" $ TestCase
    ( do
      let dbName = "tests/Server/test-wb-2.db"
      doesFileExist dbName >>= flip when (removeFile dbName)
      eSQL <- runExceptT $ D.openSQLite dbName
      runDatabase dbName eSQL $ \sql -> do
        wb <- fmap (\w -> w{cxXZCompression = True, cxPageDir = "tests/Server/whiteboards-2"}) $ create sql
        let wb0 = WhiteboardPageID 0 1
            d0 = Drawing (id0, c0)
            c0 = Circle (0,0) 5 "#FF0000" 5
            id0 = ElementID 0 0
            d1 = Drawing (id1, p0)
            p0 = Path [(0,0), (1,1)] [4] "#FF0000" (Just ((0,0), (1,1)))
            id1 = ElementID 0 1
        evalStateT (store wb0 [d0,d1]) wb
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0, P0" ([Drawing (id0, c0), Drawing (id1, p0)])
        evalStateT (unload wb0) wb
        readMVar (cxPages wb)
          >>= assertEqual "No pages" 0 . M.size
        evalStateT (withWhiteboard takeAll wb0) wb
          >>= assertEqual "C0, P0" ([Drawing (id0, c0), Drawing (id1, p0)])
    )
  ]

runDatabase dbName eSQL f = 
  case eSQL of
    Left err -> do
      removeFile dbName
      assertBool err False
    Right sql -> do
      f sql
      D.close sql
      removeFile dbName
main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
