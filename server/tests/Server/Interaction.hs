module Main where

import Zauboard.Interaction
import Zauboard

import Control.Monad.Trans.State
import Control.Concurrent
import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)
import qualified Data.Map as M
import qualified Data.Set as S

tests = TestList                         
  [ TestLabel "Join and leave" $ TestCase
    ( do
      interaction <- create
      room <- evalStateT (getRoom $ WhiteboardPageID 0 1) interaction
      evalStateT roomInfo room >>= assertEqual "Empty room" (M.empty, S.empty, S.empty)
      -- test-user joins
      let tg0 = UserTag{uUserName = "test-user", uTagBG = "black", uTagFG = "white", uID = Just 1}
      tg0M <- newMVar tg0
      runStateT (authenticate 0 tg0M) interaction
      evalStateT (getTags [0]) interaction >>=
        assertEqual "Tag 0" [tg0]
      runStateT (join 0) room
      evalStateT roomInfo room >>= assertEqual "Room 1" (M.empty, S.fromList [0], S.fromList [])
      runStateT (joinGlobal 0) room
      evalStateT roomInfo room >>= assertEqual "Room 1" (M.empty, S.fromList [0], S.fromList [0])
      -- test-user-2 joins
      runStateT (join 1) room
      evalStateT roomInfo room >>= assertEqual "Room 2" (M.empty, S.fromList [0,1], S.fromList [0])
      runStateT (joinGlobal 1) room
      evalStateT roomInfo room >>= assertEqual "Room 2" (M.empty, S.fromList [0,1], S.fromList [0,1])
      evalStateT (others 0) room >>=
        assertEqual "User 1" [1]
      -- test-user leaves
      evalStateT (leave 0 >> roomInfo) room >>= assertEqual "Leave 0" (M.empty, S.fromList [1], S.fromList [0,1])
      evalStateT (leaveGlobal 0 >> roomInfo) room >>= assertEqual "Leave 0" (M.empty, S.fromList [1], S.fromList [1])
      room' <- evalStateT (getRoom $ WhiteboardPageID 0 1) interaction
      evalStateT roomInfo room' >>= assertEqual "Room'" (M.empty, S.fromList [1], S.fromList [1])
      room2 <- evalStateT (getRoom $ WhiteboardPageID 0 2) interaction
      runStateT (joinGlobal 0) room2
      evalStateT roomInfo room2 >>= assertEqual "Room'" (M.empty, S.fromList [], S.fromList [0,1])
    )
  , TestLabel "Request, provide and pending" $ TestCase
    ( do
      interaction <- create
      room <- evalStateT (getRoom $ WhiteboardPageID 0 1) interaction
      evalStateT roomInfo room >>=
        assertEqual "Empty room" (M.empty, S.empty, S.empty)
      evalStateT (join 0 >> roomInfo) room >>=
        assertEqual "Room 1" (M.empty, S.fromList [0], S.fromList [])
      evalStateT (join 1 >> roomInfo) room >>=
        assertEqual "Room 1" (M.empty, S.fromList [0,1], S.fromList [])
      evalStateT (pending [ElementID 0 0] 0 >> roomInfo) room >>=
        assertEqual "Pending 0" (M.fromList [(ElementID 0 0, [0])], S.fromList [0,1], S.fromList [])
      evalStateT (pending [ElementID 0 1, ElementID 1 0] 0 >> roomInfo) room >>=
        assertEqual "Pending 0" (M.fromList [ (ElementID 0 1, [0])
                                            , (ElementID 1 0, [0])
                                            , (ElementID 0 0, [0])
                                            ]
                                , S.fromList [0,1], S.fromList [])
      let d0 = Drawing (ElementID 0 0, Circle (0,0) 5 "#FF0000" 1)
      evalStateT (provide [d0]) room >>=
        assertEqual "Provide 0" (M.fromList [(0,[d0])])
      evalStateT roomInfo room >>=
        assertEqual "Pending" ( M.fromList [ (ElementID 1 0, [0])
                                             , (ElementID 0 1, [0])
                                             ]
                                , S.fromList [0,1], S.fromList [])
      let d1 = Drawing (ElementID 1 0, Circle (0,0) 5 "#FF0000" 1)
          d2 = Drawing (ElementID 1 1, Circle (0,0) 5 "#FF0000" 1)
      evalStateT (provide [d1,d2]) room >>=
        assertEqual "Provide 1,2" (M.fromList [(0, [d1])])
      evalStateT roomInfo room >>=
        assertEqual "Provide 1,2" ( M.fromList [ (ElementID 0 1, [0])]
                                  , S.fromList [0,1], S.fromList [])
      evalStateT (pending [ElementID 0 1, ElementID 2 0] 1 >> roomInfo) room >>=
        assertEqual "Pending 01,20" ( M.fromList [ (ElementID 0 1, [1, 0])
                                                 , (ElementID 2 0, [1])
                                                 ]
                                    , S.fromList [0,1], S.fromList [])
      let d3 = Drawing (ElementID 0 1, Circle (0,0) 5 "#FF0000" 1)
      evalStateT (provide [d3]) room >>=
        assertEqual "Provide 3" (M.fromList [(0, [d3]), (1, [d3])])
      evalStateT roomInfo room >>=
        assertEqual "Provide 3" ( M.fromList [ (ElementID 2 0, [1])]
                                , S.fromList [0,1], S.fromList [])
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
