module Main where

import Zauboard.Export.PDF
import Zauboard

import System.Exit (exitFailure, exitSuccess)
import Test.HUnit hiding (Node)

tests = TestList                         
  [ TestLabel "Pack drawings" $ TestCase
    ( do
      assertEqual "1"    (Just [PageRange (Just 1) (Just 1)]) (parsePageSelection "1")
      assertEqual "1-2"  (Just [PageRange (Just 1) (Just 2)]) (parsePageSelection "1-2")
      assertEqual "-2"   (Just [PageRange Nothing (Just 2)]) (parsePageSelection "-2")
      assertEqual "-"    (Just [PageRange Nothing Nothing]) (parsePageSelection "-")
      assertEqual "1,2,3" (Just [ PageRange (Just 1) (Just 1)
                                , PageRange (Just 2) (Just 2)
                                , PageRange (Just 3) (Just 3)
                                ]
                          )
                          (parsePageSelection "1,2,3")
    )
  ]

main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
