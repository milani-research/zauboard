module Main where

import Zauboard.Server hiding (Command(..))
import qualified Zauboard.Server as Z
import qualified Zauboard.Database as D
import Zauboard.Package
import Zauboard

import Control.Concurrent
import Control.Exception
import Control.Monad.Except
import Control.Monad (mplus, when, forM)
import Data.Maybe
import qualified Data.Text          as T
import qualified Data.Text.Encoding as T
import Data.Time
import Network.Socket
import Network.WebSockets as WS hiding (requestHeaders, Request)
import qualified Data.ByteString        as BS
import qualified Data.ByteString.Lazy   as BSL
import qualified Data.ByteString.Base64 as BS64
import qualified Data.Set as S
import System.Directory
import System.Exit (exitFailure, exitSuccess, ExitCode(..), exitWith)
import Test.HUnit
import Test.HUnit.Lang (HUnitFailure(..))
import TestUtils

tests = TestList                         
  [ TestLabel "Load configuration" $ TestCase
    ( do
      doesDirectoryExist "tests/Server/server-0" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-0")
      opts <- runExceptT $ loadConfig "tests/Server/server-0.conf" noOptions
      assertEqual "Right options"
                  (Right noOptions
                    { oPageDir = Just "tests/Server/server-0"
                    , oPort    = Just 1234
                    , oDBType  = Just SQLiteDB
                    , oDBFile  = Just "tests/Server/server-0/database.db"
                    , oXZCompression = Just True
                    , oAllowGuestLogin = Just True
                    }
                  )
                  opts
    )
  , TestLabel "Guest connection" $ TestCase
    ( do
      let conf = "tests/Server/server-1.conf"
      doesDirectoryExist "tests/Server/server-1" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-1")
      forkIO $ start ["--configuration", conf]
      connectLoop 100 1235 $ \conn -> do
        WS.sendTextData conn (LoginAsGuest)
        WS.receiveData conn >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn >>= \reply -> do
          when (reply /= (SetUserID $ -1) && reply /= (SetUserID $ -2)) $ do
            assertEqual "User ID" (SetUserID $ -1) reply
    )
  , TestLabel "Global password" $ TestCase
    ( do
      let conf = "tests/Server/server-2.conf"
      doesDirectoryExist "tests/Server/server-2" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-2")
      forkIO $ start ["--configuration", conf]
      connectLoop 100 1236 $ \conn -> do
        WS.sendTextData conn (LoginAsGuest)
        WS.receiveData conn >>=
          assertEqual "Access denied" AccessDenied
        WS.sendTextData conn (LoginWithGlobalPassword "secret-password")
        WS.receiveData conn >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn >>=
          assertEqual "User ID" (SetUserID $ -1)
    )
  , TestLabel "Whiteboard interaction 1" $ TestCase $ timeout 5000 "Whiteboard interaction 1" $ 
    ( do
      let conf = "tests/Server/server-3.conf"
          dbName = "tests/Server/server-3/database.db"
          now = UTCTime{utctDay = fromGregorian 2021 05 16, utctDayTime = secondsToDiffTime 0}
          info0 = WhiteboardInfo
                  { wiKey = "Key"
                  , wiOwner = Nothing
                  , wiModified = now
                  , wiName = "my whiteboard"
                  , wiPages = 1
                  , wiArchived = False
                  , wiPublic = True
                  }
          info1 = WhiteboardInfo
                  { wiKey = "Key2"
                  , wiOwner = Just "test-user"
                  , wiModified = now
                  , wiName = "private whiteboard"
                  , wiPages = 1
                  , wiArchived = False
                  , wiPublic = False
                  }
      doesDirectoryExist "tests/Server/server-3" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-3")
      eSQL <- runExceptT $ D.openSQLite dbName
      -- setup database
      runDatabase dbName eSQL $ \sql -> do
        D.createUser "test-user" "password-hash" sql
        D.createWhiteboard "my whiteboard" "Key" 0 now True sql
        D.createWhiteboard "private whiteboard" "Key2" 0 now False sql
        D.giveWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql
        D.getWhiteboardID "Key" sql >>=
          assertEqual "my whiteboard" (Just (WhiteboardTopLevelID 0))
      -- start server
      forkIO $ start ["--configuration", conf]
      -- start client
      connectLoop 100 1237 $ \conn -> do
        WS.sendTextData conn (LoginAsGuest)
        WS.receiveData conn >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn >>=
          assertEqual "User ID" (SetUserID $ -1)
        WS.sendTextData conn RequestUserToken
        WS.receiveData conn >>=
          assertEqual "Invalid request" SyntaxError
        WS.sendTextData conn (ListWhiteboards)
        WS.receiveData conn >>=
          assertEqual "Only public whiteboards" (Whiteboards [info0])
        WS.sendTextData conn (JoinWhiteboard "Key2" 1)
        WS.receiveData conn >>=
          assertEqual "Drawings 1" (Drawings [])
        WS.sendTextData conn (ListWhiteboards)
        WS.receiveData conn >>=
          assertEqual "Only public whiteboards" (Whiteboards [info0])
        WS.sendTextData conn (JoinWhiteboard "Key" 1)
        WS.receiveData conn >>=
          assertEqual "Drawings 1" (Drawings [])
        let c0 = Circle (0,0) 1 "#FF0000" 10
            d0 = Drawing (ElementID (-2) 0, c0)
            d1 = Drawing (ElementID (-2) 1, c0)
            d2 = Drawing (ElementID (-2) 2, c0)
        WS.sendTextData conn (Drawings [d0])
        WS.sendTextData conn (JoinWhiteboard "Key" 2)
        WS.receiveData conn >>=
          assertEqual "Drawings 2" (Drawings [])
        WS.sendTextData conn (Drawings [d0, d1])
        WS.sendTextData conn (JoinWhiteboard "Key" 1)
        reply <- WS.receiveData conn
        if reply == (Drawings []) then do
          reply1 <- WS.receiveData conn
          assertEqual "Drawings 1" (Drawings [d0]) reply1
        else
          assertEqual "Drawings 1" (Drawings [d0]) reply
        WS.sendTextData conn (Erase [ElementID (-2) 0])
        WS.sendTextData conn (JoinWhiteboard "Key" 2)
        reply <- WS.receiveData conn
        if reply == (Drawings []) || reply == (Request [ElementID (-2) 0]) then do
          reply1 <- WS.receiveData conn
          if reply1 == (Request [ElementID (-2) 0]) then do
            reply2 <- WS.receiveData conn
            assertEqual "Drawings 2" (Drawings [d0,d1]) reply2
          else
            assertEqual "Drawings 2" (Drawings [d0,d1]) reply1
        else
          assertEqual "Drawings 2" (Drawings [d0,d1]) reply
        WS.sendTextData conn (Erase [ElementID (-2) 2])
        WS.receiveData conn >>=
          assertEqual "Request 2" (Request [ElementID (-2) 2])
        WS.sendTextData conn (Provide [d2])
        WS.sendTextData conn (JoinWhiteboard "Key" 1)
        reply <- WS.receiveData conn
        if reply == (Drawings [d0]) then do
          reply1 <- WS.receiveData conn
          assertEqual "Drawings 1" (Drawings []) reply1
        else
          assertEqual "Drawings 1" (Drawings []) reply
        WS.sendTextData conn (Restore [ElementID (-2) 0])
        WS.sendTextData conn (JoinWhiteboard "Key" 1)
        reply <- WS.receiveData conn
        if reply == (Drawings []) then do
          reply1 <- WS.receiveData conn
          assertEqual "Drawings 1" (Drawings [d0]) reply1
        else
          assertEqual "Drawings 1" (Drawings [d0]) reply
        WS.sendTextData conn (JoinWhiteboard "Key" 2)
        reply <- WS.receiveData conn
        if reply == (Drawings [d0,d1]) then do
          reply1 <- WS.receiveData conn
          assertEqual "Drawings 1" (Drawings [d0,d1,d2]) reply1
        else
          assertEqual "Drawings 1" (Drawings [d0,d1,d2]) reply
        -- Send pdf
        pdf <- fmap (T.unpack . T.decodeUtf8 . BS64.encode) $ BS.readFile "tests/Server/pdfs/drawing.pdf"
        svg <- readFile "tests/Server/pdfs/drawing.svg"
        WS.sendTextData conn (UploadPDF pdf)
        WS.receiveData conn >>=
          assertEqual "PDF 1" (PDFSummary "930f272c19150aed4f060a2d2c7437ba19ddc7b4" 1)
        let pdfImport = PDFImport{ pdfID = "930f272c19150aed4f060a2d2c7437ba19ddc7b4"
                                                 , pdfPages = [PageRange (Just 1) (Just 1)]
                                                 , pdfToWhiteboardPage = 2
                                                 , pdfScale = 1
                                                 , pdfSinglePage = False
                                                 }
        WS.sendTextData conn (ImportPDF pdfImport)
        WS.receiveData conn >>= \package -> case package of
          Drawings [Drawing (eID, RawSVG pos s w h svgStr)] -> do
            assertEqual "SVG 1 eid" (ElementID (-1) 0) eID
            assertEqual "SVG 1 pos" (20,40) pos
            assertEqual "SVG 1 scale" 1.0 s
            assertEqual "SVG 1 width" (129) $ round w
            assertEqual "SVG 1 height" (129) $ round h
            assertEqual "SVG 1 svg" (T.encodeUtf8 $ T.pack svg) svgStr
          _ ->            
            assertEqual "SVG 1" (Drawings [Drawing (ElementID 0 0,
                                                   (RawSVG (0,0) 1.0 128 128 $ T.encodeUtf8 $ T.pack svg))])
                                package
    )
  , TestLabel "Whiteboard interaction 2" $ TestCase $ timeout 5000 "Whiteboard interaction 2" $
    ( do
      let conf = "tests/Server/server-4.conf"
          dbName = "tests/Server/server-4/database.db"
          now = UTCTime{utctDay = fromGregorian 2021 05 16, utctDayTime = secondsToDiffTime 0}
      doesDirectoryExist "tests/Server/server-4" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-4")
      eSQL <- runExceptT $ D.openSQLite dbName
      -- setup database
      runDatabase dbName eSQL $ \sql -> do
        D.createUser "test-user" "password-hash" sql
        D.createUser "user 1" "$2b$10$eyH8MYLzLsqBz19b.QBEZ.344xNg2fwsNleukVp4tH8oDo18urH2q" sql
        D.createWhiteboard "my whiteboard" "Key4" 0 now True sql
        D.giveWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql
        D.getWhiteboardID "Key4" sql >>=
          assertEqual "my whiteboard" (Just (WhiteboardTopLevelID 0))
      let wb0 = WhiteboardInfo
                { wiKey = "Key4"
                , wiOwner = Nothing
                , wiName = "my whiteboard"
                , wiModified = now
                , wiPages = 1
                , wiPublic = True
                , wiArchived = False
                }
      -- start server
      forkIO $ start ["--configuration", conf]
      -- start client
      connectLoop 100 1238 $ \conn1 -> connectLoop 10 1238 $ \conn2 -> do
        -- login users
        WS.sendTextData conn1 (LoginWithUserPassword "user 1" "password")
        WS.receiveData conn1 >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn1 >>=
          assertEqual "User ID" (SetUserID $ 1)
        WS.sendTextData conn2 (LoginAsGuest)
        WS.receiveData conn2 >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn2 >>= \reply -> do
          when (not $ (reply == (SetUserID $ -2) || reply == (SetUserID $ -3) || reply == (SetUserID $ -4))) $ do
            assertEqual "User ID" (SetUserID $ -2) reply
        -- join whiteboard
        WS.sendTextData conn1 (JoinWhiteboard "Key4" 1)
        WS.receiveData conn1 >>=
          assertEqual "Drawings 1" (Drawings [])
        WS.sendTextData conn2 (JoinWhiteboard "Key4" 1)
        WS.receiveData conn2 >>=
          assertEqual "SetTag 0" (SetTag UserTag{uUserName = "guest", uTagFG = "#000000", uTagBG = "#FFFFFF", uID = Just 1})
        WS.receiveData conn2 >>=
          assertEqual "Drawings 1" (Drawings [])
        WS.receiveData conn1 >>= \reply -> do
          case reply of
            (SetTag utag) ->
              if uID utag == (Just $ -2) || uID utag == (Just $ -3) || uID utag == (Just $ -4) then
                assertEqual "SetTag 2"
                            (SetTag UserTag{ uUserName = "guest"
                                           , uID = uID utag
                                           , uTagBG = "#FFFFFF"
                                           , uTagFG = "#000000"})
                            reply
              else
                assertEqual "SetTag 2"
                            (SetTag UserTag{ uUserName = "guest"
                                           , uID = Just $ -2
                                           , uTagBG = "#FFFFFF"
                                           , uTagFG = "#000000"})
                            reply
            _ -> do
              assertEqual "SetTag 2"
                          (SetTag UserTag{ uUserName = "guest"
                                         , uID = Just $ -2
                                         , uTagBG = "#FFFFFF"
                                         , uTagFG = "#000000"})
                          reply
        -- change tag
        let tg1 = UserTag{uUserName = "user-1", uTagFG = "#FF0000", uTagBG = "#00FF00", uID = Just 2}
        let tg2 = UserTag{uUserName = "user-2", uTagFG = "#FF0000", uTagBG = "#00FF00", uID = Nothing}
        WS.sendTextData conn2 (SetTag tg2)
        WS.receiveData conn1 >>=
          assertEqual "SetTag 2" (SetTag tg2{uID = Just $ -2})
        WS.sendTextData conn1 (SetTag tg1)
        WS.receiveData conn2 >>=
          assertEqual "SetTag 1" (SetTag tg1{uID = Just $ 1})
        -- cursor update
        WS.sendTextData conn1 (CursorUpdate (0, 0) Nothing)
        WS.receiveData conn2 >>=
          assertEqual "Cursor 1" (CursorUpdate (0, 0) (Just $ 1))
        -- send drawings
        let c0 = Circle (4,4) 1 "#FF0000" 10
            d0 = Drawing (ElementID (1) 0, c0)
            d1 = Drawing (ElementID (1) 1, c0)
            d2 = Drawing (ElementID (1) 2, c0)
            d3 = Drawing (ElementID (1) 3, c0)
        WS.sendTextData conn1 (Drawings [d0])
        WS.receiveData conn2 >>=
          assertEqual "Drawings 1" (Drawings [d0])
        WS.sendTextData conn1 (Erase [ElementID (1) 0])
        WS.receiveData conn2 >>=
          assertEqual "Erase 1" (Erase [ElementID (1) 0])
        WS.sendTextData conn1 (Restore [ElementID (1) 1])
        WS.receiveData conn1 >>=
          assertEqual "Request 1" (Request [ElementID (1) 1])
        WS.sendTextData conn1 (Provide [d1])
        WS.receiveData conn2 >>=
          assertEqual "Restore 1" (Restore [ElementID (1) 1])
        WS.sendTextData conn2 (Request [ElementID (1) 1])
        WS.receiveData conn2 >>=
          assertEqual "Provide 1" (Drawings [d1])
        WS.sendTextData conn2 (Request [ElementID (1) 1])
        WS.receiveData conn2 >>=
          assertEqual "Provide 1" (Drawings [d1])
        --
        WS.sendTextData conn1 (Restore [ElementID (1) 2])
        WS.receiveData conn2 >>=
          assertEqual "Restore 2" (Restore [ElementID (1) 2])
        WS.sendTextData conn2 (Request [ElementID (1) 2])
        WS.receiveData conn1 >>=
          assertEqual "Request 2" (Request [ElementID (1) 2])
        WS.sendTextData conn1 (Provide [d2])
        WS.receiveData conn2 >>=
          assertEqual "Provide 2" (Drawings [d2])
        -- 
        WS.sendTextData conn1 (SwitchPage 2)
        WS.receiveData conn2 >>=
          assertEqual "SwitchPage 2" (SwitchPage 2)
        WS.sendTextData conn1 (JoinWhiteboard "Key4" 2)
        WS.receiveData conn1 >>=
          assertEqual "Drawings 2" (Drawings [])
        WS.sendTextData conn1 (Drawings [d3])
        WS.sendTextData conn1 (JoinWhiteboard "Key4" 3)
        WS.receiveData conn1 >>=
          assertEqual "Drawings 1" (Drawings [])
        WS.sendTextData conn1 (JoinWhiteboard "Key4" 2)
        reply <- WS.receiveData conn1
        if reply == (Drawings []) then do
          reply1 <- WS.receiveData conn1
          assertEqual "Drawings 2" (Drawings [d3]) reply1
        else
          assertEqual "Drawings 2" (Drawings [d3]) reply
        WS.sendTextData conn2 (JoinWhiteboard "Key4" 1)
        WS.receiveData conn2 >>=
          assertEqual "Drawings 1" (Drawings [d1,d2])
        WS.sendTextData conn2 (JoinWhiteboard "Key4" 2)
        WS.receiveData conn2 >>=
          assertEqual "SetTag 1" (SetTag tg1{uID = Just $ 1})
        WS.receiveData conn1 >>=
          assertEqual "SetTag 2" (SetTag tg2{uID = Just $ -2})
        WS.receiveData conn2 >>=
          assertEqual "Drawings 2" (Drawings [d3])
        WS.sendTextData conn1 (CreateWhiteboard "New board" True)
        wb <- WS.receiveData conn1
        wb1 <- case wb of
          Whiteboards [wbi] -> do
            assertEqual "New board name"      "New board" (wiName wbi)
            assertEqual "New board owner"     (Nothing)   (wiOwner wbi)
            assertEqual "New board pages"     1           (wiPages wbi)
            assertEqual "New board is public" True        (wiPublic wbi)
            return wbi
          _ -> do
            assertBool ("Incorrect package: " ++ show wb) False
            return wb0
        WS.sendTextData conn1 ListWhiteboards
        WS.receiveData conn1 >>= (\(Whiteboards wbs) ->
          assertEqual "Whiteboards 1" (S.fromList [wb1, wb0{wiPages = 1}]) (S.fromList wbs)
          )
        WS.sendTextData conn1 (CreateWhiteboard "New board 2" False)
        wb' <- WS.receiveData conn1
        wb2 <- case wb' of
          Whiteboards [wbi] -> do
            assertEqual "New board name"      "New board 2" (wiName wbi)
            assertEqual "New board owner"     (Just "user 1")   (wiOwner wbi)
            assertEqual "New board pages"     1           (wiPages wbi)
            assertEqual "New board is public" False       (wiPublic wbi)
            return wbi
          _ -> do
            assertBool ("Incorrect package: " ++ show wb) False
            return wb0
        WS.sendTextData conn1 ListWhiteboards
        WS.receiveData conn1 >>= (\(Whiteboards wbs) ->
          assertEqual "Whiteboards 2" (S.fromList [wb2, wb1, wb0{wiPages = 1}]) (S.fromList wbs)
          )
        WS.sendClose conn1 (BSL.pack [])
        WS.sendClose conn2 (BSL.pack [])
        _ <- WS.receive conn1
        _ <- WS.receive conn2
        threadDelay (10^4)
        return ()
      threadDelay ( 5 * (10^4))
      -- second session
      connectLoop 100 1238 $ \conn1 -> do
        WS.sendTextData conn1 (LoginWithUserPassword "user 1" "password")
        WS.receiveData conn1 >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn1 >>=
          assertEqual "User ID" (SetUserID $ 1)
        -- join whiteboard
        WS.sendTextData conn1 (JoinWhiteboard "Key4" 4)
        WS.receiveData conn1 >>=
          assertEqual "Drawings 1" (Drawings [])
        connectLoop 100 1238 $ \conn2 -> do
          WS.sendTextData conn2 (LoginAsGuest)
          WS.receiveData conn2 >>=
            assertEqual "Access granted" AccessGranted
          reply <- WS.receiveData conn2
          when (reply /= (SetUserID $ -1) && reply /= (SetUserID $ -2) && reply /= (SetUserID $ -3) && reply /= (SetUserID $ -4)) $
            assertEqual "User ID" (SetUserID $ -1) reply
          WS.sendTextData conn2 (JoinWhiteboard "Key4" 4)
          WS.receiveData conn2 >>=
            assertEqual "SetTag 0" (SetTag UserTag{uUserName = "guest", uTagFG = "#000000", uTagBG = "#FFFFFF", uID = Just 1})
          WS.receiveData conn2 >>=
            assertEqual "Drawings 1" (Drawings [])
          WS.sendTextData conn1 (CursorUpdate (0, 0) Nothing)
          WS.receiveData conn2 >>=
            assertEqual "Cursor 1" (CursorUpdate (0, 0) (Just 1))
          WS.sendClose conn2 (BSL.pack [])
          WS.receive conn2
          return ()
        threadDelay (10^4)
        connectLoop 100 1238 $ \conn2 -> do
          WS.sendTextData conn2 (LoginAsGuest)
          WS.receiveData conn2 >>=
            assertEqual "Access granted" AccessGranted
          reply <- WS.receiveData conn2
          when (reply /= (SetUserID $ -1) && reply /= (SetUserID $ -2)) $
            assertEqual "User ID" (SetUserID $ -1) reply
          WS.sendTextData conn2 (JoinWhiteboard "Key4" 5)
          WS.receiveData conn2 >>=
            assertEqual "Drawings 1" (Drawings [])
          WS.sendTextData conn1 (CursorUpdate (0, 0) Nothing)
          WS.sendTextData conn2 (JoinWhiteboard "Key4" 5)
          WS.receiveData conn2 >>=
            assertEqual "Drawings 1" (Drawings [])
          WS.sendClose conn2 (BSL.pack [])
          WS.receive conn2
          return ()
        WS.sendClose conn1 (BSL.pack [])
        WS.receive conn1
        return ()
    )
  , TestLabel "Commands 1" $ TestCase $ timeout 5000 "Commands 1" $
    ( do
      let conf = "tests/Server/server-5.conf"
      doesDirectoryExist "tests/Server/server-5" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-5")
      eConf <- runExceptT $ loadZauboard ["--configuration", conf]
      case eConf of
        Right zb -> do
          command zb Z.ListUsers >>= 
            assertEqual "No users" ""
          command zb (Z.CreateUser "test-user") >>= 
            assertEqual "User created" "User \"test-user\" created."
          command zb Z.ListUsers >>= 
            assertEqual "One user" "test-user"
          command zb (Z.CreateUser "test-user-2") >>= 
            assertEqual "User created" "User \"test-user-2\" created."
          command zb Z.ListUsers >>= 
            assertEqual "Two users" "test-user\ntest-user-2"
          command zb Z.ListWhiteboards >>= 
            assertEqual "No whiteboards" ""
          command zb (Z.CreateWhiteboard "my-whiteboard" "test-user")
          fmap (take (length "my-whiteboard")) (command zb Z.ListWhiteboards) >>= 
            assertEqual "One whiteboard" "my-whiteboard"
          command zb (Z.ListUserWhiteboards "test-user-2") >>= 
            assertEqual "No whiteboards" ""
          mwb0 <- D.getWhiteboardInfo (WhiteboardTopLevelID 0) (zbDatabase zb)
          assertBool "Still no whiteboards" (isJust mwb0)
          let Just wb0 = mwb0
          command zb (Z.GiveAccess "test-user-2" (wiKey wb0)) >>= 
            assertEqual "One whiteboard" ("User \"test-user-2\" has now access to whiteboard " ++ (wiKey wb0) ++ ".")
          fmap (take (length "my-whiteboard")) (command zb (Z.ListUserWhiteboards "test-user-2")) >>= 
            assertEqual "One whiteboard" "my-whiteboard"
          fmap (take (length "my-whiteboard")) (command zb (Z.SearchWhiteboard "y-w")) >>=
            assertEqual "One whiteboard" "my-whiteboard"
          command zb (Z.RemoveAccess "test-user-2" (wiKey wb0)) >>= 
            assertEqual "One whiteboard" ("Removed access to whiteboard " ++ wiKey wb0 ++ " from user \"test-user-2\".")
          command zb (Z.DeleteUser "test-user-2") >>= 
            assertEqual "One whiteboard" "Removed user \"test-user-2\"."
          command zb Z.ListUsers >>= 
            assertEqual "One user" "test-user"
          command zb (Z.DeleteWhiteboard (wiKey wb0)) >>= 
            assertEqual "One whiteboard" ("Removed whiteboard " ++ wiKey wb0 ++ ".")
          command zb Z.ListWhiteboards >>= 
            assertEqual "No whiteboards" ""
          command zb (Z.ListUserWhiteboards "test-user-2") >>= 
            assertEqual "User does not exist" "User \"test-user-2\" does not exist."
          command zb (Z.ListUserWhiteboards "test-user") >>= 
            assertEqual "No whiteboards" ""
          command zb (Z.SetPassword "test-user" "secret-password") >>= 
            assertEqual "Updated password" "Updated password of user \"test-user\"."
        Left err -> do
          assertBool err False
    )
  , TestLabel "Change password" $ TestCase $ timeout 5000 "Change password" $
    ( do
      let conf = "tests/Server/server-6.conf"
          dbName = "tests/Server/server-6/database.db"
          now = UTCTime{utctDay = fromGregorian 2023 12 28, utctDayTime = secondsToDiffTime 0}
      doesDirectoryExist "tests/Server/server-6" >>= (flip when)
        (removeDirectoryRecursive "tests/Server/server-6")
      eSQL <- runExceptT $ D.openSQLite dbName
      -- setup database
      runDatabase dbName eSQL $ \sql -> do
        D.createUser "test-user" "password-hash" sql
        D.createUser "user 1" "$2b$10$eyH8MYLzLsqBz19b.QBEZ.344xNg2fwsNleukVp4tH8oDo18urH2q" sql
        D.createWhiteboard "my whiteboard" "Key6" 0 now True sql
        D.giveWhiteboardAccess 0 (WhiteboardTopLevelID 0) sql
        D.getWhiteboardID "Key6" sql >>=
          assertEqual "my whiteboard" (Just (WhiteboardTopLevelID 0))
      let wb0 = WhiteboardInfo
                { wiKey = "Key6"
                , wiOwner = Nothing
                , wiName = "my whiteboard"
                , wiModified = now
                , wiPages = 1
                , wiPublic = True
                , wiArchived = False
                }
      -- start server
      forkIO $ start ["--configuration", conf]
      -- start client
      connectLoop 100 1239 $ \conn1 -> do
        -- login users
        WS.sendTextData conn1 (LoginWithUserPassword "user 1" "password")
        WS.receiveData conn1 >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn1 >>=
          assertEqual "User id" (SetUserID 1)
        -- change password, but provide wrong old password
        WS.sendTextData conn1 (ChangePassword "pssword" "newpassword")
        WS.receiveData conn1 >>=
          assertEqual "Wrong password" (Error IncorrectPassword)
        WS.sendTextData conn1 (ChangePassword "password" "newpassword")
        WS.receiveData conn1 >>=
          assertEqual "Success" Success
      connectLoop 100 1239 $ \conn1 -> do
        -- old password is invalid
        WS.sendTextData conn1 (LoginWithUserPassword "user 1" "password")
        WS.receiveData conn1 >>=
          assertEqual "Access denied" AccessDenied
        -- new password
        WS.sendTextData conn1 (LoginWithUserPassword "user 1" "newpassword")
        WS.receiveData conn1 >>=
          assertEqual "Access granted" AccessGranted
        WS.receiveData conn1 >>=
          assertEqual "User id" (SetUserID 1)
    )    
  ]

-- connectLoop 0 port _ = exitWith $ ExitFailure port 
connectLoop n port f = do 
  waitForServer n port
  withSocketsDo $ WS.runClient "127.0.0.1" port "/" f

waitForServer 0 port = exitWith $ ExitFailure port
waitForServer n port =
  catch (do 
         withSocketsDo $ WS.runClient "127.0.0.1" port "/" (\_ -> return ())
         -- threadDelay (10 * 10^3)
        )
        (\e -> seq (e :: IOException) $ threadDelay (10 * 10^3) >> waitForServer (n - 1) port)

--assertTransition title operation from to = assertTransition' (10^4) 10
--  where
--    assertTransition' _ 1 = do
--      result <- operation
--      assertEqual title to result
--    assertTransition' delay tries = do
--      result <- operation
--      if result == to then
--        return
--      else if result == from then do
--        threadDelay delay
--        assertTransition' delay (tries - 1)
--      else
--        assertEqual title to result

runDatabase dbName eSQL f = 
  case eSQL of
    Left err -> do
      removeFile dbName
      assertBool err False
    Right sql -> do
      f sql
      D.close sql
main = do 
  count <- runTestTT tests
  if errors count + failures count > 0 then exitFailure else exitSuccess
