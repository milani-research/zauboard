module Zauboard where

import Data.Int
import Data.Time.Clock
import Data.Time.Clock.System
import Data.Maybe
import qualified Data.ByteString as BS
import Data.List
import Control.Monad
import Text.Read (readMaybe)

type Position = (Double, Double)

type ConnectionID = Int64
type UserID = Int64
type UserKey = String
type DrawID = Int64
type PageID = Int64
type Color = String
newtype Drawing = Drawing (ElementID, DrawingElement) deriving (Eq, Ord, Show)
type Password = String
type Cursor = (Double, Double)
type WhiteboardName = String
type WhiteboardKey = String

data ElementID = ElementID UserID DrawID deriving (Eq, Ord, Show)
data WhiteboardID = WhiteboardTopLevelID Int64 | WhiteboardPageID Int64 Int64 deriving (Eq, Ord, Show)
data WhiteboardInfo = WhiteboardInfo
  { wiName     :: WhiteboardName
  , wiOwner    :: Maybe String
  , wiModified :: UTCTime
  , wiPages    :: Int64
  , wiKey      :: WhiteboardKey
  , wiPublic   :: Bool
  , wiArchived :: Bool
  } deriving (Eq, Ord)

instance Show WhiteboardInfo where
  show wbInfo = concat $ 
    [ wiKey wbInfo
    , " "
    , show $ wiPages wbInfo
    , " "
    , show $ systemSeconds $ utcToSystemTime $ wiModified wbInfo
    , " "
    , show $ wiPublic wbInfo
    , " "
    , show $ wiArchived wbInfo
    , " "
    , wiName wbInfo
    , "\n"
    , fromMaybe "" $ wiOwner wbInfo
    ]

prettyWhiteboardInfo wbInfo = 
  intercalate "\n  "
    [ wiName wbInfo
    , "Key: " ++ wiKey wbInfo
    , "Owner:" ++ fromMaybe "none" (wiOwner wbInfo)
    , "Pages:" ++ show (wiPages wbInfo)
    , "Modified: " ++ show (wiModified wbInfo)
    , "Public: " ++ show (wiPublic wbInfo)
    , "Archived: " ++ show (wiArchived wbInfo)
    ]

data DrawingElement = 
    Circle Position Double Color Double
  | Path [Position] [Double] Color (Maybe (Position, Position))
  | RawSVG Position Double Double Double BS.ByteString
  deriving (Eq, Ord, Show)

data UserTag = 
  UserTag
  { uUserName :: String
  , uID       :: Maybe UserID
  , uTagBG    :: Color
  , uTagFG    :: Color
  }
  deriving (Eq, Ord, Show)

defaultUserTag = UserTag{uUserName = "guest", uID = Nothing, uTagBG = "#FFFFFF", uTagFG = "#000000"}

topLevel wb@(WhiteboardTopLevelID _) = wb
topLevel (WhiteboardPageID wb _) = WhiteboardTopLevelID wb

topLevelID (WhiteboardTopLevelID wb) = wb
topLevelID (WhiteboardPageID wb _)   = wb

toPageID wbID page = WhiteboardPageID (topLevelID wbID) page

elementID (Drawing (eID, _)) = eID
drawing (Drawing (_, d)) = d

data PageSelection = PageRange (Maybe Int64) (Maybe Int64) deriving (Eq, Show, Ord)

parsePageSelection :: String -> Maybe [PageSelection]
parsePageSelection str = mapM parseSinglePageSelection $ splitBy (==',') str

parseSinglePageSelection str = 
  let (start, rs) = span (/='-') str
  in case rs of
    "-" -> if null start then
        Just $ PageRange Nothing Nothing
      else
        fmap (\v -> PageRange (Just v) Nothing) $ readMaybe start
    '-':end ->
      if null start then
        liftM (\u -> PageRange Nothing (Just u)) $ readMaybe end
      else
        liftM2 (\v u -> PageRange (Just v) (Just u)) (readMaybe start) (readMaybe end)
    [] -> fmap (\v -> PageRange (Just v) (Just v)) $ readMaybe start
    _ -> Nothing

formatPageSelection (PageRange begin end) = (maybe "" show begin) ++ "-" ++ (maybe "" show end)

pageSelectionToList minR maxR (PageRange begin end) = [fromMaybe minR begin .. fromMaybe maxR end]

splitBy :: (Char -> Bool) -> String -> [String]
splitBy _ [] = []
splitBy p xs =
  let (w, r) = break p (dropWhile p xs)
  in w : splitBy p r
