module Zauboard.Server
where

import           Control.Concurrent
import           Control.Exception
import           Control.Monad (mplus, when, forM)
import           Control.Monad.State
import           Control.Monad.Trans.Except
import           Crypto.KDF.BCrypt (hashPassword)
import           Data.Char
import           Data.Fixed
import           Data.List
import           Data.Maybe
import           Data.Time.Clock
import           Network.HTTP.Types
import           Network.Wai
import           Network.Wai.Handler.Warp
import           Network.Wai.Handler.WebSockets
import           Network.WebSockets hiding (requestHeaders)
import qualified Data.ByteString.Lazy.UTF8 as BLU
import qualified Data.ByteString.UTF8      as BU
import qualified Data.CaseInsensitive      as CI
import qualified Data.DescriLo             as De
import qualified Data.Text                 as T
import qualified Data.Text.Encoding        as T
import qualified Data.Text.Lazy            as TL
import qualified Data.Text.Lazy.Encoding   as TL
import           System.Directory
import           System.FilePath
import           Text.Read (readMaybe)
import qualified Zauboard.Communication as C
import qualified Zauboard.Database      as D
import qualified Zauboard.Export        as E
import qualified Zauboard.Export.PDF    as PDF
import qualified Zauboard.Interaction   as I
import qualified Zauboard.Whiteboard    as W
import           Zauboard.Utils
import           Zauboard

data Zauboard = 
  Zauboard
  { zbWhiteboardContext    :: W.Context
  , zbInteractionContext   :: I.Context
  , zbCommunicationWorld   :: C.World
  , zbDatabase             :: D.Database
  , zbPdfExport            :: E.Context WhiteboardID FilePath
  , zbPort                 :: Int
  , zbShowHelp             :: Bool
  , zbPublicDir            :: FilePath
  , zbCommands             :: [Command]
  , zbWrongOptions         :: Maybe [String]
  }

start args = do
  eConf <- runExceptT $ loadZauboard args
  case eConf of
    Left err -> do
      putStrLn "Failed to initialize zauboard:"
      putStrLn err
    Right zauboard -> do
      if zbShowHelp zauboard then do
        putStrLn help
        when (isJust $ zbWrongOptions zauboard) $ do
          putStrLn "\nThe following provided options are not valid:\n"
          mapM_ putStrLn $ fromJust $ zbWrongOptions zauboard
      else if null $ zbCommands zauboard then do
        putStrLn $ "Initializing zauboard on port " ++ (show $ zbPort zauboard) ++ " (accessible through http://localhost:" ++ (show $ zbPort zauboard) ++ ")"
        forkIO $ cleanUpLoop zauboard
        run (zbPort zauboard) $ websocketsOr connOpts (websocketApp zauboard) (app zauboard)
      else do
        mapM_ (execute zauboard) $ zbCommands zauboard
  where
    connOpts = defaultConnectionOptions

app zauboard req respond
  | requestMethod req == methodGet = do
    let conType = mimeType $ takeExtension flPath
    case flPath of
      "file" -> do
        let query = queryString req
            mkey = lookup (BU.fromString "board_key") query
            mfileID = lookup (BU.fromString "file_id") query
        case (mkey, mfileID) of
          (Just (Just key), Just (Just fileID)) -> do
            case (T.decodeUtf8' key, T.decodeUtf8' fileID) of
              (Right keyT, Right fileIDT) -> do
                mwbID   <- D.getWhiteboardID (T.unpack keyT) (zbDatabase zauboard)
                case mwbID of
                  Just wbID -> do
                    let pageDir = W.cxPageDir $ zbWhiteboardContext zauboard
                        path = pageDir </> (show $ topLevelID wbID) </> "pdf" </> (T.unpack fileIDT)
                    exists <- doesFileExist path
                    if exists then do
                      mwbInfo <- D.getWhiteboardInfo wbID (zbDatabase zauboard)
                      respond $ responseFile status200 
                        [ (CI.mk $ BU.fromString "Content-Type", BU.fromString $ mimeType $ takeExtension (T.unpack fileIDT))
                        , (CI.mk $ BU.fromString "Content-Disposition", BU.fromString $ "attachment; filename=\"" ++ (maybe "file.pdf" wiName mwbInfo) ++ ".pdf\"")
                        ]
                        path Nothing
                    else
                      respond $ responseLBS status404 [] $ BLU.fromString $ "Resource not found."
                  _ -> respond $ responseLBS status404 [] $ BLU.fromString $ "Resource not found."
              _ ->
                respond $ responseLBS status404 [] $ BLU.fromString $ "Resource not found."
          _ -> respond $ responseLBS status404 [] $ BLU.fromString $ "Resource not found."
      _ -> 
        respond $ responseFile status200 [(CI.mk $ BU.fromString "Content-Type", BU.fromString $ conType)]  (zbPublicDir zauboard </> flPath) Nothing 
  | otherwise = do
    respond $ responseLBS status404 [] $ BLU.fromString $ "Resource not found."
  where
    flPath' = intercalate "/" $ dropWhile (=="") (map T.unpack $ pathInfo req)
    flPath = if flPath' == "" then "./index.html" else flPath'

websocketApp zauboard pendingConnection = do
  conn <- acceptRequest pendingConnection
  cx <- evalStateT (C.newConnection conn) (zbCommunicationWorld zauboard)
  cxM0 <- newMVar cx
  let loop cxM = do
        incoming' <- try $ receiveDataMessage conn :: IO (Either ConnectionException DataMessage)
        case incoming' of
          Left _ -> return ()
          Right incoming -> do
            let str = case incoming of
                           Text inData _ -> either (\_ -> "") TL.unpack $ TL.decodeUtf8' inData
                           Binary inData -> either (\_ -> "") TL.unpack $ TL.decodeUtf8' inData
            forkIO $ do
              cx' <- takeMVar cxM
              resM <- newEmptyMVar
              forkFinally (runStateT (C.processIncomingData str) cx')
                          (\r -> case r of
                                      Left err -> do
                                        putStrLn "Communication:"
                                        print err
                                        putMVar cxM cx'
                                        putMVar resM False
                                      Right (res, cx'') -> putMVar cxM cx'' >> putMVar resM res)
              disconnect <- takeMVar resM
              when disconnect $ do
                sendClose conn (TL.pack "Connection closed")
            loop cxM
  withPingThread conn 30 (return ()) 
    (do
      exception <- try $ loop cxM0 :: IO (Either SomeException ())
      case exception of
        Left err -> putStrLn "Websocket app:" >> print err
        Right _ -> return ()
    )
  cx' <- takeMVar cxM0
  evalStateT C.releaseConnection cx'

cleanUpLoop zauboard = repeatLoop (secondsToNominalDiffTime $ MkFixed $ (30 * 60 * 10^12)) $ do
  now <- getCurrentTime
  D.removeExpiredData now (zbDatabase zauboard)
  return True
  
  

data Command = 
    CreateUser          String
  | CreateWhiteboard    String String
  | DeleteUser          String
  | DeleteWhiteboard    String
  | ExportPDF           String [PageSelection] FilePath
  | GiveAccess          String String
  | ImportWhiteboard    String String String UTCTime FilePath
  | ListUsers
  | ListUserWhiteboards String
  | ListWhiteboards
  | RemoveAccess        String String
  | SearchWhiteboard    String
  | SetPassword         String String
  | SetPasswordHash     String String
  deriving (Eq, Show)

command zb cmd = case cmd of
  CreateUser name -> do
    muid <- D.getUserIDFromName name db
    case muid of
      Nothing -> D.createUser name ("") db >> (return $ "User \"" ++ name ++ "\" created.")
      Just _ -> return $ "User " ++ name ++ " already exists."
  CreateWhiteboard wname uname -> do
    muid <- D.getUserIDFromName uname db
    case muid of
      Nothing -> return $ "User " ++ uname ++ " does not exist."
      Just uid -> do
        (key,_, _) <- evalStateT (W.createWhiteboard uid wname False) (zbWhiteboardContext zb)
        return $ "Created whiteboard " ++ wname ++ " with key " ++ key
  DeleteUser name -> do
    withUser name $ \uid -> do
      D.deleteUser uid db
      return $ "Removed user \"" ++ name ++ "\"."
  DeleteWhiteboard wkey -> do
    withWhiteboard wkey $ \wid -> do
      D.deleteWhiteboard wid db
      return $ "Removed whiteboard " ++ wkey ++ "."
  ExportPDF wkey ranges dest -> do
    withWhiteboard wkey $ \wid -> do
      winfo <- D.getWhiteboardInfo wid db
      case winfo of
        Just info -> do
          pdfPath <- PDF.generate dest (zbPdfExport zb) (zbWhiteboardContext zb) (wiPages info) wid ranges
          case pdfPath of
            Just path -> return $ "PDF written to " ++ path
            Nothing -> return $ "Failed to export pdf to " ++ dest
        Nothing -> return $ "No whiteboard with ID " ++ show wid
  GiveAccess uname wkey ->
    withUser uname $ \uid -> withWhiteboard wkey $ \wid -> do
      D.giveWhiteboardAccess uid wid db
      return $ "User \"" ++ uname ++ "\" has now access to whiteboard " ++ wkey ++ "."
  ImportWhiteboard wkey owner wname lastModified wpages -> do
    withUser owner $ \uid -> do
      D.createWhiteboard wname wkey uid lastModified False (zbDatabase zb)
      Just wbid <- D.getWhiteboardID wkey (zbDatabase zb)
      pages <- listDirectory wpages
      let dir = W.cxPageDir (zbWhiteboardContext zb) </> show (topLevelID wbid) </> "pages"
      createDirectoryIfMissing True dir
      forM pages $ \p -> do
        copyFile (wpages </> p) (dir </> p)
      D.updateNumberOfPages wbid (fromIntegral $ length pages) (zbDatabase zb)
      return $ "Imported whiteboard \"" ++ wname ++ "\"."
  ListUsers -> do
    users <- D.listUsers db
    return $ intercalate "\n" users
  ListUserWhiteboards name ->
    withUser name $ \uid -> do
      now <- getCurrentTime
      wbs <- D.accessibleWhiteboards uid now db
      return $ intercalate "\n" $ map prettyWhiteboardInfo wbs
  ListWhiteboards -> do
    wbs <- D.listWhiteboards db
    return $ intercalate "\n" $ map prettyWhiteboardInfo wbs
  RemoveAccess uname wkey ->
    withUser uname $ \uid -> withWhiteboard wkey $ \wid ->do
      D.removeWhiteboardAccess uid wid db
      return $ "Removed access to whiteboard " ++ wkey ++ " from user \"" ++ uname ++ "\"."
  SearchWhiteboard str -> do
    wbs <- fmap (filter (\wi -> str `isInfixOf` wiName wi)) $ D.listWhiteboards db
    if null wbs then
      return $ "No whiteboards containing " ++ str ++ " were found."
    else
      return $ intercalate "\n" $ map prettyWhiteboardInfo wbs
  SetPassword uname pass -> do
    muid <- D.getUserIDFromName uname db
    case muid of
      Nothing -> return $ "User \"" ++ uname ++ "\" does not exist."
      Just uid -> do
        hashedPass <- hashPassword 10 (T.encodeUtf8 $ T.pack pass)
        D.updatePasswordHash uid (T.unpack $ T.decodeUtf8 $ hashedPass :: String) db
        return $ "Updated password of user \"" ++ uname ++ "\"."
  SetPasswordHash uname pHash -> do
    withUser uname $ \uid -> do
      D.updatePasswordHash uid pHash db
      return $ "Updated password of user \"" ++ uname ++ "\"."
  where
    db = zbDatabase zb
    withUser uname f = do
      muid <- D.getUserIDFromName uname db
      case muid of
        Nothing -> return $ "User \"" ++ uname ++ "\" does not exist."
        Just uid -> f uid
    withWhiteboard key f = do
      mwid <- D.getWhiteboardID key db
      case mwid of
        Nothing -> return $ "Whiteboard with key \"" ++ key ++"\" does not exist."
        Just wid -> f wid

execute zb cmd = command zb cmd >>= putStrLn

data DatabaseType = PostGreDB | SQLiteDB deriving (Eq, Show)

data Options = 
  Options
  { oAutosave                   :: Maybe NominalDiffTime
  , oConfigFile                 :: Maybe FilePath
  , oPageDir                    :: Maybe FilePath
  , oPublicDir                  :: Maybe FilePath
  , oPort                       :: Maybe Int
  , oPass                       :: Maybe String
  , oUnload                     :: Maybe NominalDiffTime
  , oArchiveDuration            :: Maybe NominalDiffTime
  , oSessionDuration            :: Maybe NominalDiffTime
  , oXZCompression              :: Maybe Bool
  , oAllowGuestLogin            :: Maybe Bool
  , oAllowGuestCreateWhiteboard :: Maybe Bool
  , oDBType                     :: Maybe DatabaseType
  , oDBHost                     :: Maybe String
  , oDBUser                     :: Maybe String
  , oDBPass                     :: Maybe String
  , oDBDatabase                 :: Maybe String
  , oDBPort                     :: Maybe Int
  , oDBFile                     :: Maybe FilePath
  , oCommands                   :: [Command]
  , oHelp                       :: Maybe Bool
  , oWrongOption                :: Maybe [String]
  } deriving (Eq, Show)

noOptions =
  Options
  { oAutosave   = Nothing
  , oConfigFile = Nothing
  , oPageDir    = Nothing 
  , oPublicDir  = Nothing
  , oPort       = Nothing 
  , oPass       = Nothing 
  , oUnload     = Nothing 
  , oArchiveDuration = Nothing
  , oSessionDuration = Nothing
  , oXZCompression = Nothing
  , oAllowGuestLogin = Nothing
  , oAllowGuestCreateWhiteboard = Nothing
  , oDBType = Nothing
  , oDBHost = Nothing
  , oDBUser = Nothing
  , oDBPass = Nothing
  , oDBPort = Nothing
  , oDBFile = Nothing
  , oDBDatabase = Nothing
  , oCommands = []
  , oHelp = Nothing
  , oWrongOption = Nothing
  }

loadZauboard :: [String] -> ExceptT String IO Zauboard
loadZauboard args = do
  -- Check necessary executables
  mapM_ ensureExecutable ["pdfunite", "rsvg-convert", "xz", "pdf2svg", "pdftk"]
  confDir <- liftIO $ getXdgDirectory XdgConfig "zauboard"
  dataDir <- liftIO $ getXdgDirectory XdgData "zauboard"
  let cmdOpts   = parseArgs noOptions args
      confFile  = fromMaybe (confDir </> "config") $ oConfigFile cmdOpts
  liftIO $ createDirectoryIfMissing True (takeDirectory $ confFile)
  finalOpts <- loadConfig confFile cmdOpts
  let pageDir   = fromMaybe (dataDir </> "whiteboards") $ oPageDir finalOpts
  liftIO $ createDirectoryIfMissing True pageDir
  db <- case oDBType finalOpts of
             Just PostGreDB ->
               D.openPostGreSQL
                 (fromMaybe "localhost" $ oDBHost finalOpts)
                 (fromMaybe 5432 $ oDBPort finalOpts)
                 (fromMaybe "zauboard" $ oDBUser finalOpts)
                 (fromMaybe "zauboard" $ oDBPass finalOpts)
                 (fromMaybe "zauboard" $ oDBDatabase finalOpts)
                
             Just SQLiteDB -> D.openSQLite (fromMaybe (dataDir </> "database.db") $ oDBFile finalOpts)
             Nothing ->       D.openSQLite (fromMaybe (dataDir </> "database.db") $ oDBFile finalOpts)
  wb <- liftIO $ fmap (\w -> w
                { W.cxPageDir = pageDir
                , W.cxAutosaveInterval = oAutosave finalOpts
                , W.cxUnloadInterval = oUnload finalOpts
                , W.cxXZCompression = fromMaybe True $ oXZCompression finalOpts
                }
             )
             (W.create db)
  interaction <- liftIO $ I.create
  pdfExport <- liftIO $ E.create
  comm <- liftIO $ fmap (\w -> w{ C.woGuestLogin = fromMaybe True $ oAllowGuestLogin finalOpts
                                , C.woGuestCreateWhiteboard = fromMaybe True $ oAllowGuestCreateWhiteboard finalOpts
                                , C.woGlobalPassword = oPass finalOpts
                                , C.woArchiveDuration = fromMaybe (C.woArchiveDuration w) $ oArchiveDuration finalOpts
                                , C.woSessionDuration = fromMaybe (C.woSessionDuration w) $ oSessionDuration finalOpts
                                }) $
                        C.create wb interaction pdfExport db
  return Zauboard
    { zbWhiteboardContext    = wb
    , zbInteractionContext   = interaction
    , zbCommunicationWorld   = comm
    , zbDatabase             = db
    , zbPort = fromMaybe 8080 $ oPort finalOpts
    , zbPdfExport = pdfExport
    , zbShowHelp =  (not $ null $ oWrongOption cmdOpts) || (fromMaybe False $ oHelp cmdOpts)
    , zbWrongOptions = oWrongOption cmdOpts
    , zbCommands = reverse $ oCommands cmdOpts
    , zbPublicDir = fromMaybe "./public" $ oPublicDir finalOpts
    }

ensureExecutable path = do
  mpath <- liftIO $ findExecutable path
  when (isNothing mpath) $ do
    throwE $ "Executable " ++ path ++ " was not found. Please install it."

loadConfig :: FilePath -> Options -> ExceptT String IO Options
loadConfig confFile cmdOpts = do
  exists <- liftIO $ doesFileExist confFile
  when (not exists) $ liftIO $ do
    writeFile confFile $ "[Zauboard]\n\
                         \## How often whiteboard pages should be saved to disk.\n\
                         \## Comment out to disable autosave.\n\
                         \autosave interval = 1m\n\
                         \## How often whiteboard pages should be removed from memory.\n\
                         \## Comment out to disable unloading.\n\
                         \unload interval = 5m\n\
                         \## Directory in which to store whiteboard pages\n\
                         \## Defaults to the folder 'zauboard' under Xdg's data directory.\n\
                         \# page directory = zauboard\n\
                         \## Directory in which public data is kept for the client,\n\
                         \## like, for example, .js or .html files.\n\
                         \## Defaults to './public'.\n\
                         \# public directory = public\n\
                         \## Port in which the service should run.\n\
                         \port = 8080\n\
                         \## Whether to use xz compression for the whiteboard pages.\n\
                         \## Defaults to 'yes', set to 'no' to disable\n\
                         \# xz compression = yes\n\
                         \## Allow users to login without a password.\n\
                         \## Defaults to 'yes'.\n\
                         \# allow guest login = yes\n\
                         \## Allow guests to create whiteboards.\n\
                         \## Defaults to 'yes'.\n\
                         \# allow guest create whiteboard = yes\n\
                         \\n[Database]\n\
                         \## Database backend to use.\n\
                         \## Possible values are 'sqlite' and 'postgresql'.\n\
                         \## Defaults to 'sqlite'\n\
                         \type = sqlite\n\
                         \## File to use for the SQLite database.\n\
                         \## Defaults to (page directory)/database.db.\n\
                         \# file = zauboard/database.db\n"
  conf <- liftIO $ De.loadDescriptionFile confFile "zauboard"
  catchE (loadOpts conf cmdOpts) (\e -> throwE $ "When parsing " ++ confFile ++ ":\n " ++ (show e))
  where
    loadOpts [] opt = return opt
    loadOpts (d:ds) os =
      (case map toLower $ De.name d of
        "zauboard" -> zauboardOpts (De.values d) os
        "database" -> databaseOpts (De.values d) os
        _ -> throwE $ "Invalid section " ++ (De.name d) ++
                     " in configuration file " ++ confFile ++
                     ". Expected \"zauboard\" or \"database\" instead."
      ) >>= loadOpts ds
    zauboardOpts [] opt = return opt
    zauboardOpts (v:vs) opt = 
      (case map toLower $ fst v of
        "autosave interval" ->
          return $ opt{ oAutosave = fmap (secondsToNominalDiffTime . MkFixed . (* 10^12)) $
                                          eitherToMaybe $ parseInterval (snd v)}
        "page directory"  -> return $ opt{oPageDir = oPageDir opt `mplus` Just (snd v)}
        "public directory"  -> return $ opt{oPublicDir = oPublicDir opt `mplus` Just (snd v)}
        "port"            ->
          case readMaybe $ snd v of
            Just p -> return $ opt{oPort = oPort opt `mplus` Just p}
            Nothing -> parseError (snd v) ["a number"]
        "global password" -> return $ opt{oPass = oPass opt `mplus` Just (snd v)}
        "unload interval" ->
          return $ opt{ oUnload = fmap (secondsToNominalDiffTime . MkFixed . (* 10^12)) $
                                        eitherToMaybe $ parseInterval (snd v)}
        "archive duration" ->
          return $ opt{ oArchiveDuration = fmap (secondsToNominalDiffTime . MkFixed . (* 10^12)) $
                                        eitherToMaybe $ parseInterval (snd v)}
        "session duration" ->
          return $ opt{ oSessionDuration = fmap (secondsToNominalDiffTime . MkFixed . (* 10^12)) $
                                        eitherToMaybe $ parseInterval (snd v)}
        "xz compression"  ->
          case map toLower $ snd v of
            "true"  -> return $ opt{oXZCompression = oXZCompression opt `mplus` Just True}
            "yes"   -> return $ opt{oXZCompression = oXZCompression opt `mplus` Just True}
            "false" -> return $ opt{oXZCompression = oXZCompression opt `mplus` Just False}
            "no"    -> return $ opt{oXZCompression = oXZCompression opt `mplus` Just False}
            _ -> parseError (snd v) ["yes", "no"]
        "allow guest login" ->
          case map toLower $ snd v of
            "true"  -> return $ opt{oAllowGuestLogin = oAllowGuestLogin opt `mplus` Just True}
            "yes"   -> return $ opt{oAllowGuestLogin = oAllowGuestLogin opt `mplus` Just True}
            "false" -> return $ opt{oAllowGuestLogin = oAllowGuestLogin opt `mplus` Just False}
            "no"    -> return $ opt{oAllowGuestLogin = oAllowGuestLogin opt `mplus` Just False}
            _ -> parseError (snd v) ["yes", "no"]
        "allow guest create whiteboard" ->
          case map toLower $ snd v of
            "true"  -> return $ opt{oAllowGuestCreateWhiteboard = oAllowGuestCreateWhiteboard opt `mplus` Just True}
            "yes"   -> return $ opt{oAllowGuestCreateWhiteboard = oAllowGuestCreateWhiteboard opt `mplus` Just True}
            "false" -> return $ opt{oAllowGuestCreateWhiteboard = oAllowGuestCreateWhiteboard opt `mplus` Just False}
            "no"    -> return $ opt{oAllowGuestCreateWhiteboard = oAllowGuestCreateWhiteboard opt `mplus` Just False}
            _ -> parseError (snd v) ["yes", "no"]
        _ -> parseError (fst v)
               [ "autosave interval"
               , "page directory"
               , "port"
               , "global password"
               , "unload interval"
               , "xz compression"
               , "allow guest login"
               , "allow guest create whiteboard"
               ]
      )
      >>= zauboardOpts vs
    databaseOpts [] opt = return opt
    databaseOpts (v:vs) opt =
      case map toLower $ fst v of
        "type"     -> do
          either (\e -> throwE e)
                 (\t -> databaseOpts vs opt{oDBType = Just t}) $
                 parseDBType $ snd v 
        "host"     -> databaseOpts vs opt{oDBHost = oDBHost opt `mplus` Just (snd v)}
        "user"     -> databaseOpts vs opt{oDBUser = oDBUser opt `mplus` Just (snd v)}
        "password" -> databaseOpts vs opt{oDBPass = oDBPass opt `mplus` Just (snd v)}
        "port"     -> do
          port <- maybe (parseError (snd v) ["a number"]) return $ readMaybe (snd v)
          databaseOpts vs opt{oDBPort = oDBPort opt `mplus` Just port}
        "file"     -> databaseOpts vs opt{oDBFile = oDBFile opt `mplus` Just (snd v)}
        _ -> parseError (fst v) ["type", "host", "user", "password", "port", "file"]

parseArgs opts args =
  case args of
    "-a":t:as                  -> autosave t as
    "--autosave-interval":t:as -> autosave t as
    "-c":c:as                  -> config c as
    "--configuration":c:as     -> config c as
    "-h":as                    -> helpOn as
    "--help":as                -> helpOn as
    "-d":d:as                  -> pageDir d as
    "--page-directory":d:as    -> pageDir d as
    "--public-directory":d:as  -> publicDir d as
    "-g":as                    -> blockGuest as
    "--block-guest-login":as   -> blockGuest as
    "-s":p:as                  -> globalPass p as
    "--global-password":p:as   -> globalPass p as
    "-p":p:as                  -> port p as
    "--port":p:as              -> port p as
    "-x":as                    -> xzCompression True as
    "--xz-compression":as      -> xzCompression True as
    "--no-xz-compression":as   -> xzCompression False as
    "--database-type":t:as     -> databaseType t as
    "--database-file":p:as     -> databaseFile p as
    "--database-host":h:as     -> databaseHost h as
    "--database-port":p:as     -> databasePort p as
    "--database-user":u:as     -> databaseUser u as
    "--database-pass":p:as     -> databasePass p as
    "--database-database":d:as -> databaseName d as
    "create-user":uname:as     -> parseArgs opts{oCommands = CreateUser uname : oCommands opts} as
    "create-whiteboard":uname:wname:as ->
      parseArgs opts{oCommands = CreateWhiteboard uname wname : oCommands opts} as
    "delete-user":uname:as ->
      parseArgs opts{oCommands = DeleteUser uname : oCommands opts} as
    "delete-whiteboard":wkey:as ->
          parseArgs opts{oCommands = DeleteWhiteboard wkey : oCommands opts} as
    "export-pdf":wkey:pages:output:as ->
      let cmd = do
            pageSelection <- parsePageSelection pages
            return $ ExportPDF wkey pageSelection output
      in case cmd of
        Just c ->
          parseArgs opts{oCommands = c : oCommands opts} as
        Nothing ->
          let wrong = "export-pdf " ++ wkey ++ " " ++ pages ++ " " ++ output
          in opts{ oWrongOption = 
                     maybe (Just [wrong])
                           (\ws -> Just $ wrong:ws) $ oWrongOption opts
                 }
    "give-access":uname:wkey:as ->
      let cmd = do
            return $ GiveAccess uname wkey
      in case cmd of
        Just c ->
          parseArgs opts{oCommands = c : oCommands opts} as
        Nothing ->
          let wrong = "give-access " ++ uname ++ " " ++ wkey
          in opts{ oWrongOption = 
                     maybe (Just [wrong])
                           (\ws -> Just $ wrong:ws) $ oWrongOption opts
                 }
    "import-whiteboard":wkey:owner:wname:lastModified:wpages:as -> 
      case readMaybe lastModified of
        Just t ->         
          parseArgs opts {oCommands = ImportWhiteboard wkey owner wname (secondsToUTC t) wpages : oCommands opts} as
        Nothing ->
          let wrong = lastModified ++ " is not a valid time." in
          parseArgs opts
                    { oWrongOption = 
                        maybe (Just [wrong])
                              (\ws -> Just $ wrong:ws) $ oWrongOption opts
                    }
                    as
    "list-users":as -> parseArgs opts{oCommands = ListUsers : oCommands opts} as
    "list-user-whiteboards":uname:as ->
      parseArgs opts{oCommands = ListUserWhiteboards uname : oCommands opts} as
    "list-whiteboards":as -> parseArgs opts{oCommands = ListWhiteboards : oCommands opts} as
    "remove-access":uname:wkey:as ->
      let cmd = RemoveAccess uname wkey
      in parseArgs opts{oCommands = cmd : oCommands opts} as
    "search-whiteboard":query:as -> parseArgs opts{oCommands = SearchWhiteboard query : oCommands opts} as
    "set-password":username:password:as ->
      parseArgs opts{oCommands = SetPassword username password : oCommands opts} as
    "set-password-hash":username:pHash:as ->
      parseArgs opts{oCommands = SetPasswordHash username pHash : oCommands opts} as
    []                         -> opts
    wrong:as                   -> parseArgs opts
                                    { oWrongOption = 
                                        maybe (Just [wrong])
                                              (\ws -> Just $ wrong:ws) $ oWrongOption opts
                                    }
                                    as
  where
    autosave t      = parseArgs opts{oAutosave = fmap fromInteger $ eitherToMaybe $ parseInterval t}
    config t        = parseArgs opts{oConfigFile = Just t}
    helpOn          = parseArgs opts{oHelp = Just True}
    pageDir d       = parseArgs opts{oPageDir = Just d}
    publicDir d     = parseArgs opts{oPublicDir = Just d}
    blockGuest      = parseArgs opts{oAllowGuestLogin = Just False}
    globalPass p    = parseArgs opts{oPass = Just p}
    port p          = parseArgs opts{oPort = readMaybe p} 
    xzCompression v = parseArgs opts{oXZCompression = Just v}
    databaseType t  = parseArgs opts{oDBType = either (\_ -> Nothing) Just $ parseDBType t}
    databaseFile p  = parseArgs opts{oDBFile = Just p}
    databaseHost h  = parseArgs opts{oDBHost = Just h}
    databasePort p  = parseArgs opts{oDBPort = readMaybe p}
    databaseUser u  = parseArgs opts{oDBUser = Just u}
    databasePass p  = parseArgs opts{oDBPass = Just p}
    databaseName d  = parseArgs opts{oDBDatabase = Just d}

parseDBType :: String -> Either String DatabaseType
parseDBType str = do
  case map toLower $ str of
    "postgresql" -> return PostGreDB
    "postgre"    -> return PostGreDB
    "ps"         -> return PostGreDB
    "psql"       -> return PostGreDB
    "sqlite"     -> return SQLiteDB
    "sqlite3"    -> return SQLiteDB
    _ -> runExcept $ parseError str ["postgresql", "sqlite"]

parseInterval :: String -> Either String Integer
parseInterval [] = return $ 0
parseInterval xs = do
  let (nStr, ys) = span isNumber xs
  n <- maybe (runExcept $ parseError nStr ["a number"]) return $ readMaybe nStr
  case ys of
    ('s':ys') -> fmap (+ n) $ parseInterval ys'
    []        -> return n
    ('m':ys') -> fmap (+ (n * 60)) $ parseInterval ys'
    ('h':ys') -> fmap (+ (n * 24 * 60)) $ parseInterval ys'
    _ -> runExcept $ parseError (take 1 ys) ["s","m","h"]

parseError found expected = throwE $
  "Unexpected string \"" ++ found ++
  " found.\nExpected " ++
  csl expected ++ "instead."

eitherToMaybe (Left _)  = Nothing
eitherToMaybe (Right r) = Just r

csl xs
  | null xs = ""
  | null $ drop 1 xs = head xs
  | otherwise = intercalate ", " (init xs) ++ " or " ++ (last xs)

help = intercalate "\n" 
  [ "usage:"
  , "zauboard [OPTION...] [COMMANDS...]"
  , "\nwhere OPTION is one of"
  , " -a,--autosave-interval TIME  Regularly save each page."
  , "                              TIME is a number followed by a time unit"
  , "                              (for example \"5m\"). Possible units are"
  , "                              \"s\", \"m\" and \"h\"."
  , " -c,--configuration FILE      Read configuration from FILE."
  , " -d,--page-directory DIR      Save pages to DIR."
  , "    --public-directory DIR    Public data is kept on directory DIR."
  , " -g,--block-guest-login       Do not grant access to unauthenticated users."
  , " -p,--port PORT               Listen at PORT."
  , " -s,--global-password PASS    Require users to provide PASS in order to"
  , "                              access any whiteboard."
  , " -u,--unload-interval TIME    Remove a whiteboard from memory after"
  , "                              a period of inactivity."
  , " -x,--xz-compression          Use xz to compress pages."
  , " --no-xz-compression          Do not use xz to compress pages."
  , " --database-type TYPE         Database backend. Possible values are"
  , "                              \"sqlite\" and \"postgresql\"."
  , " --database-file PATH         Path where database is stored (sqlite only)."
  , " --database-host HOST         Hostname of database server (postgresql only)."
  , " --database-port PORT         Port of database server (postgresql only)."
  , " --database-user USER         Username for database server (postgresql only)."
  , " --database-pass PASS         Password for database server (postgresql only)."
  , " --database-database NAME     Name of the database to be used (postgresql only)."
  , ""
  , "and COMMAND is one of"
  , ""
  , "create-user USERNAME                 Create USERNAME (if it does not already exist)."
  , "create-whiteboard NAME USERNAME      Create whiteboard with the given name and set USERNAME as owner."
  , "                                     Returns unique whiteboard key and id."
  , "delete-user USERNAME                 Remove USERNAME from database."
  , "delete-whiteboard ID                 Delete whiteboard with the given id."
  , "export-pdf ID PAGE_RANGE FILE        Export PAGE_RANGE of the whiteboard ID and saves pdf to FILE."
  , "                                     PAGE_RANGE is given as a comma-separated list of ranges or"
  , "                                     individual pages. For example: 1-5,7,10-"
  , "give-access USERNAME KEY             Give USERNAME access to whiteboard with KEY."
  , "import-whiteboard KEY NAME           Import an existing whiteboard called NAME."
  , "                  USERNAME           Set USERNAME as owner."
  , "                  LAST_MODIFIED"
  , "                  PAGE_DIR" 
  , "list-users                           List all users."  
  , "list-user-whiteboards USERNAME       List all whiteboards that USERNAME has access to."
  , "list-whiteboards                     List all whiteboards."
  , "remove-access USERNAME KEY           Remove from USERNAME access to whiteboard with KEY."
  , "search-whiteboard STRING             Search for the key of whiteboards containing STRING in their name."
  , "set-password USERNAME PASSWORD       Change password of USERNAME."
  , "set-password-hash USERNAME HASH      Change password hash of USERNAME."
  , "\nCommands are executed in the order provided."
  , "The same type of command may be provided multiple times."
  ]

mimeType ".js" = "text/javascript"
mimeType ".html" = "text/html"
mimeType ".ico" = "image/vnd.microsoft.icon"
mimeType ".png" = "image/png"
mimeType ".svg" = "image/svg+xml"
mimeType ".css" = "text/css"
mimeType ".pdf" = "application/pdf"
mimeType _ = ""

