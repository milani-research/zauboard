module Zauboard.Package
       ( Package(..)
       , module D
       , parseLogin
       , parsePackage
       , pack
       , parseBoardInfos
       )
where

import Zauboard.Package.Definition as D
import Zauboard.Drawing
import Zauboard
import Control.Monad
import Network.WebSockets hiding (Request)
import qualified Data.ByteString.Lazy    as BS
import qualified Data.Text.Lazy.Encoding as T
import qualified Data.Text.Lazy          as T
import Data.Time.Clock.System
import Data.Int
import Data.Maybe
import Data.Char
import Data.List
import Text.Read (readMaybe)

pack :: Package -> BS.ByteString
pack = T.encodeUtf8 . T.pack . show

parseLogin str = 
  case map toLower $ dropWhile isSpace header of
    "loginasguest"            -> Just LoginAsGuest
    "loginwithglobalpassword" -> Just $ LoginWithGlobalPassword rs'
    "loginwithuserpassword"   ->
      case lines rs' of
        [user, password] -> Just $ LoginWithUserPassword user password
        _                -> Nothing
    "loginwithusertoken"      -> Just $ LoginWithUserToken rs'
    _ -> Nothing
  where
    (header, rs) = span (/='\n') str
    rs' = dropWhile isSpace rs

instance Show Package where
  show AccessDenied = "accessdenied"
  show AccessGranted = "accessgranted"
  show (ChangePassword oldPass newPass) = "changepassword\n" ++ oldPass ++ "\n" ++ newPass
  show Clear = "clear"
  show (CreateWhiteboard name public) = "createwhiteboard\n" ++ name ++ "\n" ++ (map toLower $ show public)
  show (CursorUpdate (x,y) muid) = "cursorupdate\n" ++ show x ++ " " ++ show y
                                   ++ (maybe "" ((' ':) . show) muid)
  show (DeleteWhiteboard whiteboardName) = "deletewhiteboard\n" ++ whiteboardName
  show (Drawings drawings) = "drawings\n1\n" ++ packDrawings drawings
  show (Erase eIDs)   = intercalate "\n" $ "erase":[show u ++ " " ++ show d | ElementID u d <- eIDs]
  show (Error msg)    = "error\n" ++ show msg
  show (ExportPDF eID pages) = "exportpdf\n" ++ show eID ++ "\n" ++ (intercalate "," $ map formatPageSelection pages)
  show (FileReady eID url) = "fileready\n" ++ show eID ++ "\n" ++ url
  show (GetWhiteboardInfo whiteboardName) = "getwhiteboardinfo\n" ++ whiteboardName
  show (ImportPDF pdfImport) = intercalate "\n" 
    [ "importpdf"
    , pdfID pdfImport 
    , intercalate "," $ map formatPageSelection $ pdfPages pdfImport
    , show $ pdfToWhiteboardPage pdfImport
    , show $ pdfScale pdfImport
    , show $ pdfSinglePage pdfImport
    ]
  show (JoinWhiteboard whiteboardName pageID) = "joinwhiteboard\n" ++ whiteboardName ++ "\n" ++ show pageID
  show ListWhiteboards = "listwhiteboards"
  show LoginAsGuest = "loginasguest"
  show (LoginWithGlobalPassword password) = "loginwithglobalpassword\n" ++ password
  show (LoginWithUserPassword user password) = "loginwithuserpassword\n" ++ user ++ "\n" ++ password
  show (LoginWithUserToken password) = "loginwithusertoken\n" ++ password
  show (PDFSummary pdfName numberOfPages) = "pdfsummary\n" ++ pdfName ++ "\n" ++ show numberOfPages
  show (Provide drawings) = "provide\n1\n" ++ packDrawings drawings
  show (ProvideUserToken token) = "provideusertoken\n" ++ token
  show (Request eIDs) = intercalate "\n" $ "request":[show u ++ " " ++ show d | ElementID u d <- eIDs]
  show RequestUserToken = "requestusertoken"
  show (Restore eIDs) = intercalate "\n" $ "restore":[show u ++ " " ++ show d | ElementID u d <- eIDs]
  show (RestoreWhiteboard whiteboardName) = "restorewhiteboard\n" ++ whiteboardName
  show (SetTag userTag) = intercalate "\n" $
    ["settag"
    , uUserName userTag
    , uTagFG userTag
    , uTagBG userTag
    ] ++ maybe [] (\i -> [show i]) (uID userTag)
  show (SetUserID userID) = "setuserid\n" ++ show userID
  show Success = "success"
  show (SwitchPage pgId) = "switchpage\n" ++ show pgId
  show SyntaxError = "syntaxerror"
  show (UploadPDF pdfBase64) = "uploadpdf\n" ++ pdfBase64
  show (Whiteboards wbs) = "whiteboards\n" ++ (intercalate "\n" $ map show wbs)

parsePackage str = 
  case map toLower $ dropWhile isSpace header of
    "accessdenied"   -> Just AccessDenied
    "accessgranted"  -> Just AccessGranted
    "changepassword" ->
      case lines rs' of
        [oldPassword, newPassword] ->
          Just $ ChangePassword oldPassword newPassword
        _ -> Nothing
    "clear"                -> Just Clear
    "createwhiteboard"     -> 
      case lines rs' of
        [whiteboardName, public] ->
          liftM (CreateWhiteboard whiteboardName)                
                (readBool public)
        _ -> Nothing
    "cursorupdate"   ->
      case words rs' of
        [x',y'] -> fmap (\p -> CursorUpdate p Nothing) $ liftM2 (,)
          (readMaybe x' :: Maybe Double)
          (readMaybe y' :: Maybe Double)
        [x',y',u'] -> liftM3 (\x y u -> CursorUpdate (x,y) (Just u))
          (readMaybe x' :: Maybe Double)
          (readMaybe y' :: Maybe Double)
          (readMaybe u' :: Maybe Int64)
        _ -> Nothing
    "deletewhiteboard" -> fmap DeleteWhiteboard $ if null rs' then Nothing else Just rs'
    "drawings"         -> fmap Drawings $ loadDrawings header' (T.encodeUtf8 $ T.pack rs'')
    "erase"            -> fmap Erase   $ parseElementIDs rs'
    "error" -> liftM Error (parseErrorMessage rs')
    "exportpdf"        -> 
      case lines rs' of
        [eIDStr, pagesStr] -> do
          eID <- readMaybe eIDStr
          pages <- fmap concat $ mapM parsePageSelection $ splitBy (','==) pagesStr
          return $ ExportPDF eID pages
        _ -> Nothing
    "getwhiteboardinfo" -> Just $ GetWhiteboardInfo rs'
    "importpdf" -> 
      case lines rs' of
        [name, pagesStr, toWhiteboardPageStr, scaleStr, singlePageStr] ->
          do
            pages <- fmap concat $ mapM parsePageSelection $ splitBy (','==) pagesStr
            toWhiteboardPage <- readMaybe toWhiteboardPageStr
            scale <- readMaybe scaleStr
            singlePage <- readBool singlePageStr
            return $ ImportPDF PDFImport
              { pdfID = name
              , pdfPages = pages
              , pdfToWhiteboardPage = toWhiteboardPage
              , pdfScale = scale
              , pdfSinglePage = singlePage
              }
        _ -> Nothing
    "joinwhiteboard" ->
      case lines rs' of
        [whiteboardName, page] ->
          liftM2 JoinWhiteboard 
            (parseWhiteboardName whiteboardName)
            (readMaybe page :: Maybe PageID)
        _ -> Nothing
    "listwhiteboards"         -> Just ListWhiteboards
    "loginasguest"            -> Just LoginAsGuest
    "loginwithglobalpassword" -> Just $ LoginWithGlobalPassword rs'
    "loginwithuserpassword" -> 
      case words rs' of
        [user, password] -> Just $ LoginWithUserPassword user password
        _                -> Nothing
    "loginwithusertoken" -> Just $ LoginWithUserToken rs'
    "pdfsummary" -> 
      case lines rs' of
        [pdfName, numberOfPages] -> fmap (PDFSummary pdfName) $ readMaybe numberOfPages
        _ -> Nothing
    "provide"           -> fmap Provide $ loadDrawings header' (T.encodeUtf8 $ T.pack rs'')
    "provideusertoken"  -> Just $ ProvideUserToken rs'
    "request"           -> fmap Request $ parseElementIDs rs'
    "requestusertoken"  -> Just RequestUserToken
    "restore"           -> fmap Restore $ parseElementIDs rs'
    "restorewhiteboard" -> fmap RestoreWhiteboard $ if null rs' then Nothing else Just rs'
    "settag"               ->
      case lines rs' of
        [username, tagFG, tagBG] -> Just $ SetTag 
          UserTag
            { uUserName = username
            , uTagFG = tagFG
            , uTagBG = tagBG
            , uID = Nothing
            }
        [username, tagFG, tagBG, uid] -> 
          fmap (\i -> SetTag 
                  UserTag
                    { uUserName = username
                    , uTagFG = tagFG
                    , uTagBG = tagBG
                    , uID = Just i
                    })
               (readMaybe uid :: Maybe Int64)
        _ -> Nothing
    "setuserid"   -> fmap SetUserID $ (readMaybe rs' :: Maybe UserID)
    "success"     -> Just Success
    "switchpage"  -> fmap SwitchPage $ (readMaybe rs' :: Maybe PageID)
    "syntaxerror" -> Just SyntaxError
    "uploadpdf"   -> Just $ UploadPDF rs'
    "whiteboards" -> fmap Whiteboards $ parseBoardInfos $ lines rs'
    _ -> Nothing
  where
    (header, rs) = span (/='\n') str
    rs' = dropWhile isSpace rs
    (header', rs'') = span (/='\n') rs'

parseErrorMessage msg =
  case map toLower msg of 
    "incorrectpassword" -> Just IncorrectPassword
    _ -> Nothing

parseBoardInfos [] = Just []
parseBoardInfos (i:rs) = do
  let (key, key')     = span (/= ' ') i
      (pages, pages') = span (/= ' ') $ dropWhile (==' ') key'
      (time, time')   = span (/= ' ') $ dropWhile (==' ') pages'
      (public, archived')  = span (/= ' ') $ dropWhile (==' ') time'
      (archived, name)  = span (/= ' ') $ dropWhile (==' ') archived'
  pagesI <- readMaybe pages :: Maybe Int64
  timeS  <- readMaybe time  :: Maybe Int64
  publicB <- readBool public
  archivedB <- readBool archived
  fmap (WhiteboardInfo
    { wiKey = key
    , wiPages = pagesI
    , wiName = drop 1 name
    , wiModified = systemToUTCTime $ MkSystemTime{systemSeconds = timeS, systemNanoseconds = 0}
    , wiOwner = if null rs then
                  Nothing
                else if publicB then
                  Nothing
                else
                  Just $ head rs
    , wiPublic = publicB
    , wiArchived = archivedB
    }:) $ parseBoardInfos $ drop 1 rs

parseWhiteboardName str = Just $ str

readBool str = 
  case map toLower str of
    "false" -> Just False
    "true"  -> Just True
    _ -> Nothing
                

instance WebSocketsData Package where
  fromDataMessage (Text msg _)   = fromLazyByteString msg
  fromDataMessage (Binary msg)   = fromLazyByteString msg
  fromLazyByteString msg = either (\_ -> SyntaxError) (\str -> fromMaybe SyntaxError $ parsePackage $ T.unpack str) $ T.decodeUtf8' msg
  toLazyByteString pkg = T.encodeUtf8 $ T.pack $ show pkg

