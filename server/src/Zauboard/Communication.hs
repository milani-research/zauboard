module Zauboard.Communication
       ( Context(..)
       , World(..)
       , ConnectionState
       , create
       , processIncomingData
       , newConnection
       , releaseConnection
       , stripSvgHeader
       )
where

import qualified Zauboard.Database    as D
import qualified Zauboard.Export      as E
import qualified Zauboard.Export.PDF  as PDF
import qualified Zauboard.Interaction as I
import qualified Zauboard.Whiteboard  as W
import           Zauboard
import           Zauboard.Package
import           Zauboard.Utils

import           Control.Concurrent
import           Control.Exception
import           Control.Monad (when, forM)
import           Control.Monad.State
import           Crypto.Hash hiding (Context)
import           Crypto.Random
import           Data.Char
import           Data.List
import           Data.Maybe
import           Data.Time.Clock
import           Network.WebSockets hiding (Request)
import qualified Crypto.KDF.BCrypt      as C (validatePassword, hashPassword)
import qualified Data.ByteString        as BS
import qualified Data.ByteString.Base64 as BS64
import qualified Data.ByteString.Char8  as BS8
import qualified Data.Map               as M
import qualified Data.Text              as T
import qualified Data.Text.Encoding     as T
import qualified Network.URI            as U
import           System.Directory
import           System.FilePath
import           System.IO
import           System.Process
import           Text.Read (readMaybe)

data World =
  World
  { woArchiveDuration       :: NominalDiffTime
  , woConnections           :: MVar (M.Map ConnectionID Connection)
  , woDatabase              :: D.Database
  , woFreeConnectionIDs     :: MVar [ConnectionID]
  , woGlobalPassword        :: Maybe Password
  , woGuestCreateWhiteboard :: Bool
  , woGuestLogin            :: Bool
  , woInteraction           :: I.Context
  , woSessionDuration       :: NominalDiffTime
  , woWhiteboard            :: W.Context
  , woExport                :: E.Context WhiteboardID FilePath
  }

data Context = 
  Context
  { cxWorld      :: World
  , cxConnection :: ConnectionState
  }

data Role = Guest | Member deriving (Eq, Show)

data ConnectionState = 
    UnauthenticatedConnection
    { cnID     :: ConnectionID
    , cnSocket :: Connection
    }
  | AuthenticatedConnection
    { cnID     :: ConnectionID
    , cnUserID :: UserID
    , cnTag    :: MVar UserTag
    , cnSocket :: Connection
    , cnRole   :: Role
    }
  | JoinedConnection
    { cnID           :: ConnectionID
    , cnUserID       :: UserID
    , cnWhiteboardID :: WhiteboardID
    , cnTag          :: MVar UserTag
    , cnRoom         :: I.Room
    , cnSocket       :: Connection
    , cnRole         :: Role
    }

hasJoined (JoinedConnection{}) = True
hasJoined _ = False

instance Show ConnectionState where
  show c@UnauthenticatedConnection{} = "UnauthenticatedConnection{cnID = " ++ show (cnID c) ++ "}"
  show c@AuthenticatedConnection{}   = "AuthenticatedConnection{cnID = " ++ show (cnID c) ++ "}"
  show c@JoinedConnection{} = "JoinedConnection{cnID = " ++ show (cnID c) ++ "}"

newConnection :: Connection -> StateT World IO Context
newConnection conn = do  
  world <- get
  freeIDs <- liftIO $ takeMVar $ woFreeConnectionIDs world
  liftIO $ if null freeIDs then do
    putMVar (woFreeConnectionIDs world) freeIDs
    c <- takeMVar (woConnections world)
    let cID = fromMaybe 0 $ fmap ((1 +) . fst) $ M.lookupMax c
    putMVar (woConnections world) $ M.insert cID conn c
    return $ Context{ cxConnection = UnauthenticatedConnection{cnID = cID, cnSocket = conn}
                    , cxWorld = world
                    }
  else do
    let cID = head freeIDs
    putMVar (woFreeConnectionIDs world) $ tail freeIDs
    modifyMVar_ (woConnections world) (\c -> return $ 
        M.insert cID conn c
      )
    return $ Context{ cxConnection = UnauthenticatedConnection{cnID = cID, cnSocket = conn}
                    , cxWorld = world
                    }

releaseConnection :: StateT Context IO ()
releaseConnection = do
  cx <- get
  let cID = cnID $ cxConnection cx
  let world = cxWorld cx
  when (hasJoined $ cxConnection cx) $ do
    runLocalInteraction I.leaveGlobal
    runLocalInteraction $ I.leave
  liftIO $ do
    freeIDs <- takeMVar $ woFreeConnectionIDs world
    connections <- takeMVar $ woConnections world
    putMVar (woConnections world) $ M.delete cID connections
    putMVar (woFreeConnectionIDs world) (cID : freeIDs)

create wbContext iContext exportContext db = do
  connM <- newMVar M.empty
  freeConnM <- newMVar []
  return World
    { woWhiteboard  = wbContext
    , woInteraction = iContext
    , woExport      = exportContext
    , woDatabase    = db
    , woConnections = connM 
    , woFreeConnectionIDs = freeConnM
    , woGlobalPassword = Nothing
    , woGuestLogin = False
    , woGuestCreateWhiteboard = False
    , woSessionDuration = 7 * 24 * 60 * 60
    , woArchiveDuration = 7 * 24 * 60 * 60
    }

processIncomingData bstr = do
  cx <- get
  let world = cxWorld cx
  case cxConnection cx of
    cn@UnauthenticatedConnection{} -> 
      case parseLogin bstr of
        Nothing -> do
          reply SyntaxError
        Just (LoginWithGlobalPassword pass) -> 
          case woGlobalPassword world of
            Nothing -> reply AccessDenied
            Just globalPass ->
              if globalPass == pass then do
                let uid = - (cnID cn) - 1
                tagM <- liftIO $ newMVar defaultUserTag{uID = Just $ uid}
                put cx{ cxConnection = 
                        AuthenticatedConnection
                        { cnID = cnID cn
                        , cnUserID = uid
                        , cnSocket = cnSocket cn
                        , cnTag = tagM
                        , cnRole = Guest
                        }
                      }
                runGlobalInteraction (I.authenticate (cnID cn) tagM)
                reply AccessGranted
                reply $ SetUserID uid
              else
                reply AccessDenied
        Just (LoginWithUserToken userToken) -> do
          uid <- runDatabase $ D.getUserIDFromKey userToken
          if isJust uid then
            loginUser cn cx (fromJust uid)
          else
            reply AccessDenied
        Just (LoginAsGuest) -> 
          if woGuestLogin world then do
            let uid = - (cnID cn) - 1
            tagM <- liftIO $ newMVar defaultUserTag{uID = Just uid}
            put cx{ cxConnection = 
                    AuthenticatedConnection
                    { cnID = cnID cn
                    , cnUserID = uid
                    , cnSocket = cnSocket cn
                    , cnTag = tagM
                    , cnRole = Guest
                    }
                  }
            runGlobalInteraction (I.authenticate (cnID cn) tagM)
            reply AccessGranted
            reply $ SetUserID uid
          else
            reply AccessDenied
        Just (LoginWithUserPassword uname password ) -> do
          muid <- runDatabase $ D.getUserIDFromName uname
          case muid of
            Just uid -> do
              passwordHash <- runDatabase $ D.getPasswordHash uid
              if (Just True) == fmap (checkPassword password) passwordHash then
                loginUser cn cx uid
              else
                reply AccessDenied
            Nothing ->
              reply AccessDenied
    cn@AuthenticatedConnection{cnRole = role} -> do
      let pkg = parsePackage bstr
      case pkg of
        Just (JoinWhiteboard whiteboardName page) ->
          joinWhiteboard whiteboardName page Nothing
        Just ListWhiteboards -> do
          tag <- liftIO $ readMVar $ cnTag cn
          now <- liftIO $ getCurrentTime
          whiteboards <- runDatabase $ D.accessibleWhiteboards (fromJust $ uID tag) now
          reply $ Whiteboards whiteboards
        Just (CreateWhiteboard name public) ->
          case role of
            Member -> do
              createWhiteboard name public
            Guest ->
              if woGuestCreateWhiteboard $ cxWorld cx then
                createWhiteboard name public
              else
                reply AccessDenied
        Just (GetWhiteboardInfo name) -> getWhiteboardInfo name
        Just (DeleteWhiteboard name) -> deleteWhiteboard name
        Just (RestoreWhiteboard name) -> restoreWhiteboard name
        Just (SetTag utag) -> setUserTag utag
        Just RequestUserToken -> 
          if (cnUserID cn >= 0) then do
            key <- createUserSession $ cnUserID cn
            reply (ProvideUserToken key)
          else
            reply SyntaxError
        Just (ChangePassword oldPass newPass) -> changePassword cn oldPass newPass
        _ -> do
          reply SyntaxError
    cn@JoinedConnection{cnRole = role} -> do
      let pkg = parsePackage bstr
      case pkg of
        Nothing -> do
          reply SyntaxError
        Just RequestUserToken -> do
          key <- createUserSession $ cnUserID cn
          reply (ProvideUserToken key)
        Just (DeleteWhiteboard name) -> deleteWhiteboard name
        Just (RestoreWhiteboard name) -> restoreWhiteboard name
        Just (GetWhiteboardInfo name) -> getWhiteboardInfo name
        Just (ExportPDF eID pages) -> do
          let wbID = cnWhiteboardID $ cn
          wbInfo <- runDatabase $ D.getWhiteboardInfo wbID
          case wbInfo of
            Just info -> do
              now <- liftIO $ getCurrentTime
              let fileID = show (hash $ T.encodeUtf8 $ (T.pack $ bstr ++ show now) :: Digest SHA1)
                  pdfDest = fileID <.> "pdf"
              result <- liftIO $ try $ PDF.generate pdfDest (woExport $ cxWorld cx) (woWhiteboard $ cxWorld cx) (wiPages info) wbID pages
              case result of
                Left err -> liftIO $ do
                  putStrLn "PDF Export:"
                  print (err :: SomeException)
                Right _ -> do
                  reply (FileReady eID $ "/file?file_id=" ++ 
                                         (U.escapeURIString U.isUnescapedInURIComponent pdfDest) ++ "&board_key=" ++ 
                                         (U.escapeURIString U.isUnescapedInURIComponent $ wiKey info))
            Nothing -> return ()
        Just package -> do
          tag <- liftIO $ readMVar $ cnTag cn
          when (isBroadcast package) $ do
            others <- runLocalInteraction $ I.others
            let package' = case package of
                  CursorUpdate p _ -> CursorUpdate p (uID tag)
                  SetTag utag      -> SetTag utag{uID = uID tag}
                  _ -> package
            mapM_ (sendPackage package') others
          case package of
            (JoinWhiteboard whiteboardName page) ->
              joinWhiteboard whiteboardName page (Just $ cnWhiteboardID $ cxConnection cx)
            (Drawings drawings) -> do
              runWhiteboard $ W.insert drawings
              return ()
            (Restore eIDs) -> do
              missing <- runWhiteboard $ W.restore eIDs
              when (not $ null missing) $ do
                runWhiteboard $ W.addMissing missing
                reply $ Request missing
            (Erase eIDs) -> do
              missing <- runWhiteboard $ W.erase eIDs
              when (not $ null missing) $ do
                runWhiteboard $ W.addMissing missing
                reply $ Request missing
            (Request eIDs) -> do
              runLocalInteraction $ \_ -> I.gotRequest
              (found, missing) <- runWhiteboard $ W.takeDrawings eIDs
              liftIO $ do
                print eIDs
                print found
              when (not $ null missing) $ do
                runLocalInteraction $ I.pending missing
              runLocalInteraction $ \_ -> I.finishedRequest
              when (not $ null found) $ 
                reply $ Drawings found
              return ()
            (Provide drawings) -> do
              runWhiteboard $ W.insertIfMissing drawings
              msgs <- runLocalInteraction $ \_ -> I.provide drawings
              let msgs' = M.map Drawings msgs
              mapM_ (uncurry $ flip sendPackage) $ M.assocs msgs'
            ListWhiteboards -> do
              now <- liftIO getCurrentTime
              whiteboards <- runDatabase $ D.accessibleWhiteboards (fromJust $ uID tag) now
              reply $ Whiteboards whiteboards
            (ChangePassword oldPass newPass) -> changePassword cn oldPass newPass
            Clear -> do
              runWhiteboard $ W.clear
            (CreateWhiteboard name public) -> do
              case role of
                Member -> do
                  createWhiteboard name public
                Guest ->
                  if woGuestCreateWhiteboard $ cxWorld cx then
                    createWhiteboard name public
                  else
                    reply AccessDenied
            (SetTag utag) -> setUserTag utag
            UploadPDF pdfBase64 -> do
              (pdfID', pdfPages') <- storePDF pdfBase64
              reply $ PDFSummary pdfID' pdfPages'
            ImportPDF pdfImport -> do
              let pdfPath = W.pdfPath (cnWhiteboardID $ cxConnection cx) (pdfID pdfImport) (woWhiteboard $ cxWorld cx)
              exists <- liftIO $ doesFileExist pdfPath
              if exists then
                do
                  numberOfPages <- liftIO $ getNumberOfPages pdfPath
                  addPdfPages pdfPath numberOfPages (pdfSinglePage pdfImport) (pdfScale pdfImport)
                              (pdfToWhiteboardPage pdfImport)
                              (pdfPages pdfImport)
              else
                reply SyntaxError
            _ ->
              return ()
  return False

addPdfPages path numberOfPages singlePage scale targetPage pageSelections = do
  let dir = takeDirectory path
  let pages = concatMap (pageSelectionToList 1 numberOfPages) pageSelections
  svgs <- liftIO $ forM pages $ \p -> return $ do
    let svgFile = dir </> "page-" ++ show p <.> "svg"
        pageFile = dir </> "page-" ++ show p <.> "pdf"
    (_,Just sout, Just serr, ph0) <- createProcess $ (proc "pdftk" [path, "cat", show p, "output", pageFile ]){std_out = CreatePipe, std_err = CreatePipe}
    forkIO $ do
      outStr <- hGetContents sout
      seq (length outStr) (return ())
    forkIO $ do
      errStr <- hGetContents serr
      seq (length errStr) (return ())
    waitForProcess ph0
    (_,_,_, ph1) <- createProcess $ proc "pdf2svg" [pageFile, svgFile]
    waitForProcess ph1
    svgH <- openFile svgFile ReadMode
    svgStr <- hGetContents svgH
    let RawSVG pos _ w h svg = stripSvgHeader svgStr
        result = RawSVG pos scale w h svg
    seq (length svgStr) $ hClose svgH
    removeFile pageFile
    removeFile svgFile
    return result
  addSvgPages singlePage targetPage 0 svgs
  liftIO $ removeFile path

addSvgPages _ _ _ [] = return ()
addSvgPages singlePage targetPage yshift (svg':ss) = do
  (RawSVG (x,y) scale w h svg) <- liftIO svg'
  cx <- get
  let cn = cxConnection cx
      wbID = cnWhiteboardID cn
      wbID' = WhiteboardPageID (topLevelID wbID) targetPage
  drawings <- liftIO $ evalStateT (W.withWhiteboard (W.takeAll) wbID') $ woWhiteboard $ cxWorld cx
  let drID' = maximum $ (-1) : (map (\d -> let ElementID _ drID = elementID d in drID) $
                                   filter (\d -> let ElementID uid _ = elementID d in uid == cnUserID cn)
                                          drawings)
      svgDrawing = [Drawing (ElementID (cnUserID cn) (drID' + 1),  RawSVG (x, y + yshift) scale w h svg)]
  liftIO $ evalStateT (W.withWhiteboard (W.insert svgDrawing) wbID') $ woWhiteboard $ cxWorld cx
  room <- liftIO $ evalStateT (I.getRoom wbID') $ woInteraction $ cxWorld cx
  (_, users, _) <- liftIO $ evalStateT I.roomInfo room
  mapM_ (sendPackage $ Drawings svgDrawing) users
  addSvgPages singlePage (if singlePage then targetPage else targetPage + 1)
                         (if singlePage then yshift + h else yshift)
                         ss

changePassword cn oldPass newPass = do
  let uid = cnUserID cn
  passwordHash <- runDatabase $ D.getPasswordHash uid
  case passwordHash of
    Just passwordHash' ->
      if checkPassword oldPass passwordHash' then do
        newHash <- liftIO $ C.hashPassword 10 (T.encodeUtf8 $ T.pack newPass)
        runDatabase $ D.updatePasswordHash uid (T.unpack $ T.decodeUtf8 $ newHash :: String)
        reply $ Success
      else
        reply $ Error IncorrectPassword
    Nothing -> 
      reply AccessDenied

deleteWhiteboard name = do
  mwbID <- runDatabase $ D.getWhiteboardID name
  case mwbID of
    Just wbID -> do
      cx <- get
      now <- liftIO $ getCurrentTime
      isArchived <- runDatabase $ D.isWhiteboardArchived (cnUserID $ cxConnection cx) wbID
      if isArchived then
        runDatabase $ D.updateArchiveExpirationDate (cnUserID $ cxConnection cx) wbID (addUTCTime (woArchiveDuration $ cxWorld cx) now)
      else
        runDatabase $ D.archiveWhiteboardAccess (cnUserID $ cxConnection cx) wbID (addUTCTime (woArchiveDuration $ cxWorld cx) now)
      runDatabase $ D.removeWhiteboardAccess (cnUserID $ cxConnection cx) wbID
      return ()
    Nothing ->
      reply SyntaxError

restoreWhiteboard name = do
  mwbID <- runDatabase $ D.getWhiteboardID name
  case mwbID of
    Just wbID -> do
      cx <- get
      hasAccess <- runDatabase $ D.hasWhiteboardAccess (cnUserID $ cxConnection cx) wbID
      when (not hasAccess) $ 
        runDatabase $ D.giveWhiteboardAccess (cnUserID $ cxConnection cx) wbID
      runDatabase $ D.removeArchiveAccess (cnUserID $ cxConnection cx) wbID
      return ()
    Nothing ->
      reply SyntaxError

loginUser cn cx uid = do
  tagM <- liftIO $ newMVar defaultUserTag{uID = Just uid}
  put cx{ cxConnection =
          AuthenticatedConnection
          { cnID = cnID cn
          , cnUserID = uid
          , cnSocket = cnSocket cn
          , cnTag = tagM
          , cnRole = Member
          }
        }
  runGlobalInteraction (I.authenticate (cnID cn) tagM)
  reply AccessGranted
  reply $ SetUserID uid

storePDF :: String -> StateT Context IO (String, Int)
storePDF pdfBase64 = do
  cx <- get
  let pdf = either (\_ -> BS.pack []) (\str -> str) $ BS64.decode $ T.encodeUtf8 $ T.pack pdfBase64
      pdfName = show $ (hash pdf :: Digest SHA1)
      pdfPath = W.pdfPath (cnWhiteboardID $ cxConnection cx) pdfName (woWhiteboard $ cxWorld cx)
  liftIO $ evalStateT (W.addPdf (cnWhiteboardID $ cxConnection cx) pdfName pdf) $
                      woWhiteboard $ cxWorld cx
  numberOfPages <- liftIO $ do
    nPages <- getNumberOfPages pdfPath
    return nPages
  return (pdfName, numberOfPages)

getNumberOfPages pdfPath = do
  (_, Just sout,_, _) <- createProcess $ (proc "pdfinfo" [pdfPath]){std_out = CreatePipe}
  outStr <- hGetContents sout
  let nPages = fromMaybe 0 $ mhead
        [ fromMaybe 0 $ readMaybe val
        | ln <- lines outStr
        , let (var, val) = case words ln of
                               (a:b:_) -> (a,b)
                               _ -> ("","")
        , var == "Pages:"
        ]
  seq (length outStr) $ return nPages
  

stripSvgHeader [] = RawSVG (0,0) 1.0 0 0 BS.empty
stripSvgHeader svg' 
  | "<?xml" `isPrefixOf` svg' = stripSvgHeader' $ dropWhile isSpace $ drop 2 $ dropWhile (/='?') $ drop 5 svg'
  | otherwise = stripSvgHeader $ tail svg'
  where
    stripSvgHeader' svg
      | null svg = RawSVG (0,0) 1.0 0 0 BS.empty
      | "<svg" `isPrefixOf` svg = 
        let attrs = M.fromList $ parseAttributeList $ takeWhile (/='>') $ drop 4 svg            
        in RawSVG (20,40) 1.0
                  (fromMaybe 0 $ M.lookup "width"  attrs >>= readUnit)
                  (fromMaybe 0 $ M.lookup "height" attrs >>= readUnit)
                  (T.encodeUtf8 $ T.pack $ stripEnd $ dropWhile isSpace $ drop 1 $ dropWhile (/='>') svg)
      | otherwise = stripSvgHeader' $ tail svg
    stripEnd svg
      | null svg = ""
      | "xlink:href" `isPrefixOf` svg = "href" ++ stripEnd (drop 10 svg)
      | "</svg>" `isPrefixOf` svg && (null $ filter (not . isSpace) $ drop 6 svg) = ""
      | otherwise = (head svg) : (stripEnd $ tail svg)

createWhiteboard name public = do
  cx <- get
  let cn = cxConnection cx
  tag <- liftIO $ readMVar $ cnTag cn
  (key, wbid, now) <- liftIO $ evalStateT (W.createWhiteboard (fromJust $ uID tag) name public)
                           (woWhiteboard $ cxWorld cx)
  owner <- runDatabase $ D.getUsername (fromJust $ uID tag)
  reply $ Whiteboards [WhiteboardInfo
    { wiKey = key
    , wiName = name
    , wiPublic = public
    , wiModified = now
    , wiPages = 1
    , wiOwner = owner
    , wiArchived = False
    }]
  when (cnRole cn == Member) $ case wbid of
    Just wid -> runDatabase $ D.giveWhiteboardAccess (fromJust $ uID tag) wid
    Nothing -> return ()

generateUniqueUserKey :: StateT Context IO String
generateUniqueUserKey = do
  drg <- liftIO $ getSystemDRG
  now <- liftIO $ getCurrentTime
  let (bs, _) = randomBytesGenerate 18 drg :: (BS.ByteString, SystemDRG)
  let key = T.unpack $ T.decodeUtf8 $ BS64.encode bs
  keyExists <- runDatabase $ D.isUserKeyValid key now
  if keyExists then
    generateUniqueUserKey
  else
    return key

createUserSession uid = do
  key <- generateUniqueUserKey
  now <- liftIO $ getCurrentTime
  cx <- get
  let duration = (woSessionDuration $ cxWorld cx)
  runDatabase $ D.createUserSession uid key (addUTCTime duration now) duration
  return key

getWhiteboardInfo name = do
  mwbID <- runDatabase $ D.getWhiteboardID name
  case mwbID of
    Just wbID -> do
      mwbInfo <- runDatabase $ D.getWhiteboardInfo wbID
      case mwbInfo of
        Just wbInfo -> reply $ Whiteboards [wbInfo]
        Nothing -> reply SyntaxError
    Nothing -> reply SyntaxError

joinWhiteboard whiteboardName page mPreviousID = do
  cx <- get
  mWhiteboardID <- liftIO $ evalStateT (W.accessWhiteboard whiteboardName page) (woWhiteboard $ cxWorld cx)
  case mWhiteboardID of
    Nothing -> do
      reply AccessDenied
    Just whiteboardID -> do
      let cn = cxConnection cx
      tag <- liftIO $ readMVar $ cnTag cn
      when (Just True == (fmap (>=0) $ uID tag)) $ do
        runDatabase $ \db -> do
          access <- D.hasWhiteboardAccess (fromJust $ uID tag) whiteboardID db
          when (not access) $ 
            D.giveWhiteboardAccess (fromJust $ uID tag) whiteboardID db
          archived <- D.isWhiteboardArchived (fromJust $ uID tag) whiteboardID db
          when archived $ 
            D.removeArchiveAccess (fromJust $ uID tag) whiteboardID db
      room <- runGlobalInteraction $ I.getRoom whiteboardID
      runStateT (I.joinGlobal $ cnID cn) room
      runStateT (I.join (cnID cn)) room
      case mPreviousID of
        Nothing -> return ()
        Just previousID -> do
          when (previousID /= whiteboardID) $ do
            when (topLevelID previousID /= topLevelID whiteboardID) $ do
              runLocalInteraction I.leaveGlobal
            runLocalInteraction $ I.leave
      let cx' =  cx{ cxConnection = 
              JoinedConnection
                { cnID = cnID cn
                , cnUserID = cnUserID cn
                , cnWhiteboardID = toPageID whiteboardID page
                , cnTag = cnTag $ cxConnection cx
                , cnRoom = room
                , cnSocket = cnSocket $ cxConnection cx
                , cnRole = cnRole cn
                }
            }
      put cx'
      others <- liftIO $ evalStateT (I.others $ cnID cn) room
      oTags <- runGlobalInteraction $ I.getTags others
      mapM_ (\t -> reply $ SetTag t) oTags
      mapM_ (sendPackage $ SetTag tag) others
      drawings <- runWhiteboard W.takeAll
      reply (Drawings drawings)

setUserTag :: UserTag -> StateT Context IO ()
setUserTag tag = do
  cx <- get
  liftIO $ modifyMVar_ (cnTag $ cxConnection cx) (\tag' -> return tag{uID = uID tag'})
  return ()

checkPassword :: String -> String -> Bool
checkPassword password passwordHash = C.validatePassword (BS8.pack password) (BS8.pack passwordHash)

runWhiteboard :: (StateT W.Page IO b) -> StateT Context IO b
runWhiteboard f = do
  cx <- get
  case cxConnection cx of
    c@JoinedConnection{} -> do
      let wbID = cnWhiteboardID c
      liftIO $ evalStateT (W.withWhiteboard f wbID) $ woWhiteboard $ cxWorld cx

runGlobalInteraction :: (StateT I.Context IO b) -> StateT Context IO b
runGlobalInteraction f = do
  context <- get
  liftIO $ evalStateT f (woInteraction $ cxWorld context)

runLocalInteraction :: (ConnectionID -> StateT I.Room IO b) -> StateT Context IO b
runLocalInteraction f = do
  context <- get
  case cxConnection context of
    c@(JoinedConnection{}) -> liftIO $ evalStateT (f $ cnID c) (cnRoom c)

runDatabase :: (D.Database -> IO b) -> StateT Context IO b
runDatabase f = do
  context <- get
  liftIO $ f (woDatabase $ cxWorld $ context)

reply :: Package -> StateT Context IO ()
reply message = do
  cx <- get
  let conn = cxConnection cx
  liftIO $ sendDataMessage (cnSocket conn) (Text (pack message) Nothing)

sendPackage :: Package -> ConnectionID -> StateT Context IO ()
sendPackage package connectionID = do
  cx <- get
  let msg = pack package
  connections <- liftIO $ readMVar $ woConnections $ cxWorld cx
  case connectionID `M.lookup` connections of
    Nothing     -> return ()
    Just socket -> liftIO $
      sendDataMessage socket (Text msg Nothing)

