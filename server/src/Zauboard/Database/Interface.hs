module Zauboard.Database.Interface
       ( ZauboardDB(..)
       , accessibleWhiteboardsQuery
       , archiveWhiteboardAccessQuery
       , archivedWhiteboardsQuery
       , cleanArchiveQuery
       , createUserQuery
       , createUserSessionQuery
       , createWhiteboardQuery
       , deleteUserQueries
       , deleteUserSessionQuery
       , deleteWhiteboardIfOrphanQuery
       , deleteWhiteboardQueries
       , getWhiteboardIDQuery
       , getWhiteboardInfoQuery
       , getModificationDateQuery
       , getPasswordHashQuery
       , getUserIDFromKeyQuery
       , getUserIDFromNameQuery
       , getUsernameQuery
       , giveWhiteboardAccessQuery
       , hasWhiteboardAccessQuery
       , isUserKeyValidQuery
       , isWhiteboardArchivedQuery
       , listUsersQuery
       , listWhiteboardsQuery
       , publicWhiteboardsQuery
       , removeArchiveAccessQuery
       , removeExpiredDataQueries
       , removeWhiteboardAccessQuery
       , renewUserSessionQuery
       , setupDatabaseQueries
       , updateArchiveExpirationDateQuery
       , updateModificationDateQuery
       , updateNumberOfPagesQuery
       , updatePasswordHashQuery
       )
where

import Zauboard
import Data.Time.Clock
import Data.Int


class ZauboardDB a where
  accessibleWhiteboards    :: UserID -> UTCTime -> a -> IO [WhiteboardInfo]
  archiveWhiteboardAccess  :: UserID  -> WhiteboardID -> UTCTime -> a -> IO ()
  archivedWhiteboards      :: UserID -> UTCTime -> a -> IO [WhiteboardInfo]
  boardExists              :: WhiteboardKey -> a -> IO Bool
  cleanArchive             :: UTCTime -> a -> IO ()
  close                    :: a -> IO ()
  createUserSession        :: UserID -> UserKey -> UTCTime -> NominalDiffTime -> a -> IO ()
  createUser               :: String -> String -> a -> IO ()
  createWhiteboard         :: WhiteboardKey -> WhiteboardName -> UserID -> UTCTime -> Bool -> a -> IO ()
  deleteUserSession        :: UserKey -> a -> IO ()
  deleteUser               :: UserID -> a -> IO ()
  deleteWhiteboardIfOrphan :: WhiteboardID -> UTCTime -> a -> IO ()
  deleteWhiteboard         :: WhiteboardID -> a -> IO ()
  getWhiteboardID          :: WhiteboardKey -> a -> IO (Maybe WhiteboardID)
  getWhiteboardInfo        :: WhiteboardID -> a -> IO (Maybe WhiteboardInfo)
  getModificationDate      :: WhiteboardID -> a -> IO (Maybe UTCTime)
  getPasswordHash          :: UserID -> a -> IO (Maybe String)
  getUserIDFromKey         :: UserKey -> a -> IO (Maybe UserID)
  getUserIDFromName        :: String -> a -> IO (Maybe UserID)
  getUsername              :: UserID  -> a -> IO (Maybe String)
  giveWhiteboardAccess     :: UserID  -> WhiteboardID -> a -> IO ()
  hasWhiteboardAccess      :: UserID -> WhiteboardID -> a -> IO Bool
  isUserKeyValid           :: UserKey -> UTCTime -> a -> IO Bool
  isWhiteboardArchived     :: UserID -> WhiteboardID -> a -> IO Bool
  listUsers                :: a -> IO [String]
  listWhiteboards          :: a -> IO [WhiteboardInfo]
  removeArchiveAccess      :: UserID -> WhiteboardID -> a -> IO ()
  removeExpiredData        :: UTCTime -> a -> IO ()
  removeWhiteboardAccess   :: UserID -> WhiteboardID -> a -> IO ()
  renewUserSession         :: UserKey -> UTCTime -> a -> IO ()
  updateArchiveExpirationDate :: UserID -> WhiteboardID -> UTCTime -> a -> IO ()
  updateModificationDate   :: WhiteboardID -> UTCTime -> a -> IO ()
  updateNumberOfPages      :: WhiteboardID -> Int64  -> a -> IO () 
  updatePasswordHash       :: UserID -> String -> a -> IO ()


createUserSessionQuery = 
  "INSERT INTO user_session (user_id, user_key, expire_date, duration) \
  \VALUES ( \
  \     $1 \
  \   , $2 \
  \   , $3 \
  \   , $4 );"
createUserQuery = 
  "INSERT INTO login\
  \(user_id, username, display_name, password_hash, access_level)\
  \VALUES ((SELECT IFNULL(MAX(user_id),-1)+1 FROM login), $1, $1, $2, 1);"
createWhiteboardQuery = 
  "INSERT INTO whiteboard (owner_id, board_id, board_name, board_key, is_moderated, is_public, last_modified, pages) \
  \VALUES ( \
  \    $1 \
  \  , (SELECT IFNULL(MAX(board_id),-1) + 1 FROM whiteboard) \
  \  , $2 \
  \  , $3 \
  \  , True \
  \  , $5 \
  \  , $4 \
  \  , 1 );"
deleteUserSessionQuery =
  "DELETE FROM user_session WHERE user_key = $1;"
deleteUserQueries = 
  [ "DELETE FROM whiteboard_access WHERE user_id = $1;"
  , "DELETE FROM user_session WHERE user_id = $1;"
  , "DELETE FROM login WHERE user_id = $1;"
  ]
deleteWhiteboardQueries = 
  [ "DELETE FROM whiteboard WHERE board_id = $1;"
  , "DELETE FROM whiteboard_access WHERE board_id = $1;"
  ]
getWhiteboardIDQuery = 
  "SELECT w.board_id \
  \FROM whiteboard AS w \
  \WHERE w.board_key = $1;"
getWhiteboardInfoQuery = 
  "SELECT w.board_name, w.board_key, l.username, w.pages, w.last_modified, w.is_public, FALSE \
  \FROM whiteboard AS w \
  \LEFT JOIN login AS l ON l.user_id = w.owner_id \
  \WHERE w.board_id = $1;"
getPasswordHashQuery = 
  "SELECT password_hash FROM login WHERE user_id = $1;"
getUserIDFromKeyQuery = 
  "SELECT user_id FROM user_session where user_key = $1;"
getUserIDFromNameQuery = 
  "SELECT user_id FROM login where username = $1;"
getUsernameQuery = 
  "SELECT l.username \
  \FROM login AS l \
  \WHERE l.user_id = $1;"
getModificationDateQuery = 
  "SELECT last_modified \
  \FROM whiteboard \
  \WHERE board_id = $1;"
giveWhiteboardAccessQuery = 
  "INSERT INTO whiteboard_access(user_id, board_id, is_moderator) VALUES\
  \( $1, $2, false);"
archiveWhiteboardAccessQuery = 
  "INSERT INTO whiteboard_access_archive(user_id, board_id, is_moderator, expire_date) VALUES\
  \( $1, $2, false, $3);"
hasWhiteboardAccessQuery = 
  "SELECT count(*) \
  \FROM whiteboard_access \
  \WHERE user_id = $1 AND board_id = $2;"
isWhiteboardArchivedQuery = 
  "SELECT count(*) \
  \FROM whiteboard_access_archive \
  \WHERE user_id = $1 AND board_id = $2;"
updateArchiveExpirationDateQuery = 
  "UPDATE whiteboard_access_archive \
  \SET expire_date = $3 \
  \WHERE user_id = $1 AND board_id = $2;"
isUserKeyValidQuery = 
  "SELECT count(*) \
  \FROM user_session \
  \WHERE user_key = $1 AND expire_date > $2;"
listUsersQuery = 
  "SELECT username \
  \FROM login;"
listWhiteboardsQuery = 
  "SELECT w.board_name, w.board_key, l.username, w.pages, w.last_modified, w.is_public, FALSE \
  \FROM whiteboard w \
  \LEFT JOIN login l on w.owner_id = l.user_id"
removeArchiveAccessQuery =
  "DELETE FROM whiteboard_access_archive WHERE board_id = $1 AND user_id = $2;"
removeExpiredDataQueries =
  [ "DELETE FROM whiteboard_access_archive WHERE expire_date < $1;"
  , "DELETE FROM user_session WHERE expire_date < $1;"
  ]
removeWhiteboardAccessQuery =
  "DELETE FROM whiteboard_access WHERE board_id = $1 AND user_id = $2;"
deleteWhiteboardIfOrphanQuery = 
  "DELETE \
  \FROM whiteboard AS w \
  \WHERE w.board_id = $1 AND w.board_id NOT IN (\
  \  SELECT a.board_id \
  \  FROM whiteboard_access AS a \
  \  WHERE a.board_id = $1 \
  \UNION ALL\
  \  SELECT a.board_id \
  \  FROM whiteboard_access_archive AS a \
  \  WHERE a.board_id = $1 AND a.expire_date > $2\
  \  );"
cleanArchiveQuery = 
  "DELETE \
  \FROM whiteboard_access_archive AS w \
  \WHERE w.expire_date < $1;"
renewUserSessionQuery = 
  "UPDATE user_session \
  \SET expire_date = $2 + duration \
  \WHERE user_key = $1;"
updateModificationDateQuery = 
  "UPDATE whiteboard \
  \SET last_modified=$2 \
  \WHERE board_id = $1 AND last_modified < $2;"
updateNumberOfPagesQuery = 
  "UPDATE whiteboard \
  \SET pages=$2 \
  \WHERE board_id = $1 AND pages < $2;"
updatePasswordHashQuery = 
  "UPDATE login \
  \SET password_hash = $1 \
  \WHERE user_id = $2;"
accessibleWhiteboardsQuery = 
  "SELECT w.board_name, w.board_key, l.username, w.pages, w.last_modified, w.is_public, FALSE \
  \FROM whiteboard w \
  \INNER JOIN whiteboard_access a on w.board_id = a.board_id \
  \INNER JOIN login l on w.owner_id = l.user_id \
  \WHERE a.user_id = $1 AND w.is_public = FALSE \
  \UNION \
  \SELECT w.board_name, w.board_key, l.username, w.pages, w.last_modified, w.is_public, TRUE \
  \FROM whiteboard w \
  \INNER JOIN whiteboard_access_archive a on w.board_id = a.board_id \
  \INNER JOIN login l on w.owner_id = l.user_id \
  \WHERE a.user_id = $1 AND w.is_public = FALSE AND a.expire_date > $2;"
archivedWhiteboardsQuery = 
  "SELECT w.board_name, w.board_key, l.username, w.pages, w.last_modified, w.is_public, TRUE \
  \FROM whiteboard w \
  \INNER JOIN whiteboard_access_archive a on w.board_id = a.board_id \
  \INNER JOIN login l on w.owner_id = l.user_id \
  \WHERE a.user_id = $1 AND w.is_public = FALSE AND a.expire_date > $2;"

publicWhiteboardsQuery = 
  "SELECT w.board_name, w.board_key, NULL, w.pages, w.last_modified, w.is_public, FALSE \
  \FROM whiteboard w \
  \WHERE w.is_public = TRUE;"

setupDatabaseQueries =
  [ "CREATE TABLE login\
    \ ( user_id int\
    \, username varchar(30)\
    \, display_name varchar(30)\
    \, password_hash varchar(255)\
    \, access_level smallint\
    \, PRIMARY KEY (username));"
  , "CREATE TABLE user_session\
    \( user_id int\
    \, user_key char(24)\
    \, expire_date bigint\
    \, duration bigint\
    \, PRIMARY KEY (user_id, user_key));"
  , "CREATE TABLE whiteboard\
    \( owner_id int\
    \, board_id int\
    \, board_name varchar(120)\
    \, board_key char(24)\
    \, is_moderated boolean\
    \, is_public boolean\
    \, last_modified bigint\
    \, pages int\
    \, PRIMARY KEY(board_id));"
  , "CREATE TABLE whiteboard_access\
    \( user_id int\
    \, board_id int\
    \, is_moderator boolean\
    \, PRIMARY KEY(user_id, board_id));"
  , "CREATE TABLE whiteboard_access_archive\
    \( user_id int\
    \, board_id int\
    \, is_moderator boolean\
    \, expire_date bigint\
    \, PRIMARY KEY(user_id, board_id));"
  , "CREATE TABLE invitation\
    \( invitation_key char(24)\
    \, access_level int\
    \, expire_date bigint\
    \, PRIMARY KEY(invitation_key));"
  , "CREATE TABLE metadata\
    \( attribute varchar(32)\
    \, value varchar(100)\
    \, PRIMARY KEY(attribute));"
  , "CREATE UNIQUE INDEX board_key_id\
    \ ON whiteboard (board_key)"
  ]

