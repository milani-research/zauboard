{-# LANGUAGE OverloadedStrings #-}

module Zauboard.Database.SQLite
       ( open
       , SQLiteDB(..)
       )
where

import Control.Concurrent.MVar
import Control.Monad.Except
import Control.Monad.Trans.Except
import Control.Monad.IO.Class (liftIO)
import Control.Exception
import System.Directory
import System.FilePath
import Database.SQLite.Simple hiding (close, open)
import qualified Database.SQLite.Simple as SQ (close, open)
import Data.Fixed
import Data.Maybe
import Data.Time.Clock
import Data.Time.Clock.System
import Data.Int
import Data.String

import Zauboard
import Zauboard.Database.Interface (ZauboardDB(..))
import qualified Zauboard.Database.Interface as D
import Zauboard.Utils

data SQLiteDB = 
  SQLiteDB
  { sqConnection                 :: Connection
  , sqMutex                      :: MVar ()
  }

data SQLException = SetupException String deriving Show

instance Exception SQLException

open :: FilePath -> ExceptT String IO SQLiteDB
open file = do
  db <- liftIO $ catch ( do
    createDirectoryIfMissing True $ takeDirectory file
    flEx <- doesFileExist file
    mutex <- newMVar ()
    conn <- if flEx then do
      conn' <- SQ.open file
      checkDatabase SQLiteDB{sqConnection = conn', sqMutex = mutex}
      return conn'
    else do
      c <- SQ.open file
      setupDB c
      return c
    return $ Right $ SQLiteDB{ sqConnection = conn, sqMutex = mutex}
    )
    (\e -> return $ Left $ show (e :: SQLException))
  case db of
    Right db' -> return db'
    Left err -> throwE err

checkDatabase db = do
  version <- runSQL db versionQuery [] :: IO (Maybe [String])
  case version of
    Just ["0"] -> do
      executeSQL db "ALTER TABLE whiteboard ADD COLUMN is_public boolean DEFAULT(FALSE);" []
      executeSQL db setMetadataQuery ["$1" := ("version" :: String), "$2" := ("1" :: String)]
    Just ["1"] -> do
      executeSQL db "CREATE TABLE whiteboard_access_archive\
                    \( user_id int\
                    \, board_id int\
                    \, is_moderator boolean\
                    \, expire_date bigint\
                    \, PRIMARY KEY(user_id, board_id));" []
      executeSQL db updateMetadataQuery ["$1" := ("version" :: String), "$2" := ("2" :: String)]
    Just ["2"] -> return ()
    _ -> throw $ SetupException ("Invalid version. Expected 0, 1 or 2, found: " ++ show version)
  
versionQuery                     = "SELECT value FROM metadata WHERE attribute = 'version';"
setMetadataQuery                 = "INSERT INTO metadata (attribute, value) VALUES ($1, $2);"
updateMetadataQuery              = "UPDATE metadata SET value = $2 WHERE attribute = $1;"
accessibleWhiteboardsQuery       = fromString D.accessibleWhiteboardsQuery
archivedWhiteboardsQuery         = fromString D.archivedWhiteboardsQuery
archiveWhiteboardAccessQuery     = fromString D.archiveWhiteboardAccessQuery
boardInfoQuery                   = fromString D.getWhiteboardInfoQuery
cleanArchiveQuery                = fromString D.cleanArchiveQuery
createUserQuery                  = fromString D.createUserQuery
createUserSessionQuery           = fromString D.createUserSessionQuery
createWhiteboardQuery            = fromString D.createWhiteboardQuery
deleteUserQueries                = map fromString D.deleteUserQueries
deleteUserSessionQuery           = fromString D.deleteUserSessionQuery
deleteWhiteboardIfOrphanQuery    = fromString D.deleteWhiteboardIfOrphanQuery
deleteWhiteboardQueries          = map fromString D.deleteWhiteboardQueries
getWhiteboardIDQuery             = fromString D.getWhiteboardIDQuery
getModificationDateQuery         = fromString D.getModificationDateQuery
getPasswordHashQuery             = fromString D.getPasswordHashQuery
getUserIDFromKeyQuery            = fromString D.getUserIDFromKeyQuery
getUserIDFromNameQuery           = fromString D.getUserIDFromNameQuery
getUsernameQuery                 = fromString D.getUsernameQuery
giveWhiteboardAccessQuery        = fromString D.giveWhiteboardAccessQuery
hasWhiteboardAccessQuery         = fromString D.hasWhiteboardAccessQuery
isUserKeyValidQuery              = fromString D.isUserKeyValidQuery
isWhiteboardArchivedQuery        = fromString D.isWhiteboardArchivedQuery
listUsersQuery                   = fromString D.listUsersQuery
listWhiteboardsQuery             = fromString D.listWhiteboardsQuery
publicWhiteboardsQuery           = fromString D.publicWhiteboardsQuery
removeArchiveAccessQuery         = fromString D.removeArchiveAccessQuery
removeExpiredDataQueries         = map fromString D.removeExpiredDataQueries
removeWhiteboardAccessQuery      = fromString D.removeWhiteboardAccessQuery
renewSessionQuery                = fromString D.renewUserSessionQuery
updateArchiveExpirationDateQuery = fromString D.updateArchiveExpirationDateQuery
updateModificationDateQuery      = fromString D.updateModificationDateQuery
updateNumberOfPagesQuery         = fromString D.updateNumberOfPagesQuery
updatePasswordHashQuery          = fromString D.updatePasswordHashQuery

setupDB conn = do
  mapM_ (execute_ conn . fromString) D.setupDatabaseQueries
  execute_ conn "INSERT INTO metadata (attribute, value) VALUES ('version', '2');"

instance ZauboardDB SQLiteDB where
  boardExists boardKey db = fmap isJust $ (runSQL db getWhiteboardIDQuery ["$1" := boardKey] :: IO (Maybe (Only Int)))
  isUserKeyValid userKey now db = 
    fmap (maybe False (( > (0 :: Int)) . fromOnly)) $
    runSQL db isUserKeyValidQuery ["$1" := userKey, "$2" := (systemSeconds $ utcToSystemTime now)]
  isWhiteboardArchived uid wbID db =
    fmap (maybe False (( > (0 :: Int)) . fromOnly)) $
    runSQL db isWhiteboardArchivedQuery ["$1" := uid, "$2" := topLevelID wbID]
  giveWhiteboardAccess ui wbID db =
    executeSQL db giveWhiteboardAccessQuery ["$1" := ui, "$2" := topLevelID wbID]
  archiveWhiteboardAccess ui wbID now db =
    executeSQL db archiveWhiteboardAccessQuery ["$1" := ui, "$2" := topLevelID wbID, "$3" := (systemSeconds $ utcToSystemTime now)]
  getUsername ui db =
    fmap (fmap fromOnly) $ runSQL db getUsernameQuery ["$1" := ui]
  getModificationDate wid db = 
    fmap (fmap (secondsToUTC . fromOnly)) $ runSQL db getModificationDateQuery ["$1" := topLevelID wid]
  updateArchiveExpirationDate uid wbID now db =
    executeSQL db updateArchiveExpirationDateQuery ["$1" := uid, "$2" := topLevelID wbID, "$3" := (systemSeconds $ utcToSystemTime now)]
  updateModificationDate wbID now db =
    executeSQL db updateModificationDateQuery [ "$2" := (systemSeconds $ utcToSystemTime now), "$1" := topLevelID wbID]
  updateNumberOfPages wbID pages db =
    executeSQL db updateNumberOfPagesQuery ["$1" := topLevelID wbID, "$2" := pages]
  getWhiteboardInfo wbID db =
    runSQL db boardInfoQuery ["$1" := topLevelID wbID]
  cleanArchive now db = 
    executeSQL db cleanArchiveQuery ["$1" := (systemSeconds $ utcToSystemTime now)]
  createUser un ph db =
    executeSQL db createUserQuery ["$1" := un, "$2" := ph]
  createUserSession ui uk now t db = 
    let MkFixed dt' = nominalDiffTimeToSeconds t
        dt = (fromIntegral $ dt' `div` 10^12) :: Int64
    in executeSQL db createUserSessionQuery [ "$1" := ui, "$2" := uk, "$3" := (systemSeconds $ utcToSystemTime now), "$4" := dt]
  createWhiteboard bn bk ui now public db =
    executeSQL db createWhiteboardQuery [ "$1" := ui,  "$2" := bn, "$3" := bk, "$4" := (systemSeconds $ utcToSystemTime now), "$5" := public]
  deleteWhiteboard wbID db = 
    executeSQLBlock db deleteWhiteboardQueries [ "$1" := topLevelID wbID ]
  deleteUser ui db            = executeSQLBlock db deleteUserQueries [ "$1" := ui ]
  deleteUserSession uk db     = executeSQL db deleteUserSessionQuery [ "$1" := uk ]
  updatePasswordHash ui ph db = executeSQL db updatePasswordHashQuery [ "$1" := ph, "$2" := ui ]
  getPasswordHash ui db       = fmap (fmap fromOnly) $ runSQL db getPasswordHashQuery ["$1" := ui]
  removeArchiveAccess ui wbID db = 
    executeSQL db removeArchiveAccessQuery ["$1" := topLevelID wbID, "$2" := ui]
  removeExpiredData now db = 
    executeSQLBlock db removeExpiredDataQueries [ "$1" := (systemSeconds $ utcToSystemTime now) ]
  removeWhiteboardAccess ui wbID db = 
    executeSQL db removeWhiteboardAccessQuery ["$1" := topLevelID wbID, "$2" := ui]
  deleteWhiteboardIfOrphan wbID now db =
    executeSQL db deleteWhiteboardIfOrphanQuery [ "$1" := topLevelID wbID, "$2" := (systemSeconds $ utcToSystemTime now)]
  renewUserSession uk now db = 
    executeSQL db renewSessionQuery [ "$1" := (systemSeconds $ utcToSystemTime now), "$2" := uk]
  hasWhiteboardAccess ui wbID db =
    fmap (maybe False (( > (0 :: Int)) . fromOnly)) $
         runSQL db hasWhiteboardAccessQuery [ "$1" := ui, "$2" := topLevelID wbID]
  listWhiteboards db = runSQLMany db listWhiteboardsQuery []
  listUsers db       = fmap (map fromOnly) $ runSQLMany db listUsersQuery []
  getUserIDFromKey uk db  = fmap (fmap fromOnly) $ runSQL db getUserIDFromKeyQuery  [ "$1" := uk ]
  getUserIDFromName un db = fmap (fmap fromOnly) $ runSQL db getUserIDFromNameQuery [ "$1" := un ]
  getWhiteboardID bk db        = fmap (fmap $ WhiteboardTopLevelID . fromOnly) $ 
    runSQL db getWhiteboardIDQuery [ "$1" :=  bk]
  accessibleWhiteboards ui now db = do
    private <- runSQLMany db accessibleWhiteboardsQuery [ "$1" := ui , "$2" := (systemSeconds $ utcToSystemTime now)]
    public  <- runSQLMany db publicWhiteboardsQuery []
    return $ private ++ public
  archivedWhiteboards ui now db = do
    runSQLMany db archivedWhiteboardsQuery [ "$1" := ui , "$2" := (systemSeconds $ utcToSystemTime now)]
  close db = SQ.close $ sqConnection db

runSQL db q param = do
  takeMVar $ sqMutex db
  r <- catch
    ( do
      res <- queryNamed (sqConnection db)
                         q
                         param
      return $ mhead res
    )
    ((\_ -> return Nothing) :: SQLError -> IO (Maybe a))
  putMVar (sqMutex db) ()
  return r

runSQLMany db q param = do
  takeMVar $ sqMutex db
  r <- catch
    ( do
      res <- queryNamed (sqConnection db)
                         q
                         param
      return $ res
    )
    ((\_ -> return []) :: SQLError -> IO [a])
  putMVar (sqMutex db) ()
  return r

executeSQL db q param = do
  takeMVar $ sqMutex db
  catch
    ( do
      executeNamed (sqConnection db)
                    q
                    param                        
    )
    ((\err -> print err >> return ()) :: SQLError -> IO ())
  putMVar (sqMutex db) ()
executeSQLBlock db qs param = do
  takeMVar $ sqMutex db
  catch
    ( do
      execute_ (sqConnection db) "BEGIN;" 
      mapM_ (\q -> executeNamed (sqConnection db)
                    q
                    param
            ) qs
      execute_ (sqConnection db) "COMMIT;" 
    )
    ((\_ -> return ()) :: SQLError -> IO ())
  putMVar (sqMutex db) ()
      
instance FromRow WhiteboardInfo where
  fromRow = makeWhiteboardInfo
            <$> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
            <*> field
    where
      makeWhiteboardInfo bn bk un p modified public archived = 
        WhiteboardInfo
          { wiName = bn
          , wiOwner = un
          , wiModified = secondsToUTC modified 
          , wiPages = p
          , wiKey = bk
          , wiPublic = public
          , wiArchived = archived
          }
