{-# LANGUAGE OverloadedStrings #-}
module Zauboard.Database.PostGreSQL
       ( PostGreDB(..)
       , open
       )
where

import qualified Hasql.Connection as PG
import qualified Hasql.Statement as PG
import qualified Hasql.Session as PG
import qualified Hasql.Encoders as PGE
import qualified Hasql.Decoders as PGD
import qualified Data.ByteString.Char8 as BU
import qualified Data.Text.Encoding as T
import qualified Data.Text          as T
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Except
import Data.Fixed
import Data.Functor.Contravariant
import Data.Int
import Data.Maybe
import Data.Text (pack, unpack)
import Data.Time.Clock
import Data.Time.Clock.System
import Zauboard
import Zauboard.Database.Interface
import Zauboard.Utils

type Time = Int64
-- type WhiteboardKey = String
type WhiteboardI = Int64

data PostGreDB = 
  PostGreDB
  { pgAccessibleWhiteboards       :: PG.Statement (UserID, Time) [WhiteboardInfo]
  , pgArchiveWhiteboardAccess     :: PG.Statement (UserID, WhiteboardI, Bool, Time) ()
  , pgArchivedWhiteboards         :: PG.Statement (UserID, Time) [WhiteboardInfo]
  , pgWhiteboardInfo              :: PG.Statement  WhiteboardI WhiteboardInfo
  , pgCleanArchive                :: PG.Statement  Time ()
  , pgConnection                  :: PG.Connection
  , pgCreateUser                  :: PG.Statement (String, String) ()
  , pgCreateUserSession           :: PG.Statement (UserID, UserKey, Time, Time) ()
  , pgCreateWhiteboard            :: PG.Statement (UserID, String, WhiteboardKey, Time, Bool) ()
  , pgDeleteUser                  :: PG.Statement  UserID ()
  , pgDeleteUserSession           :: PG.Statement  UserKey ()
  , pgDeleteWhiteboard            :: PG.Statement  WhiteboardI ()
  , pgGetWhiteboardID             :: PG.Statement  WhiteboardKey (Maybe Int64)
  , pgGetModificationDate         :: PG.Statement  WhiteboardI Int64
  , pgGetPasswordHash             :: PG.Statement  UserID String
  , pgGetUserIDFromKey            :: PG.Statement  UserKey UserID
  , pgGetUserIDFromName           :: PG.Statement  String UserID
  , pgGetUsername                 :: PG.Statement  UserID String
  , pgGiveWhiteboardAccess        :: PG.Statement (UserID, WhiteboardI, Bool) ()
  , pgHasWhiteboardAccess         :: PG.Statement (UserID, WhiteboardI) Int32
  , pgIsUserKeyValid              :: PG.Statement (UserKey, Time) Int32
  , pgIsWhiteboardArchived        :: PG.Statement (UserID, WhiteboardI) Int32
  , pgListUsers                   :: PG.Statement () [String]
  , pgListWhiteboards             :: PG.Statement () [WhiteboardInfo]
  , pgPublicWhiteboards           :: PG.Statement () [WhiteboardInfo]
  , pgRemoveArchiveAccess         :: PG.Statement (UserID, WhiteboardI) ()
  , pgRemoveExpiredData           :: PG.Statement (Time) ()
  , pgRemoveWhiteboardAccess      :: PG.Statement (UserID, WhiteboardI) ()
  , pgRemoveWhiteboardIfOrphan    :: PG.Statement (WhiteboardI, Time) ()
  , pgRenewUserSession            :: PG.Statement (UserKey, Time) ()
  , pgUpdateArchiveExpirationDate :: PG.Statement (UserID, WhiteboardI, Time) ()
  , pgUpdateModificationDate      :: PG.Statement (WhiteboardI, Time) ()
  , pgUpdateNumberOfPages         :: PG.Statement (WhiteboardI, Int64) ()
  , pgUpdatePasswordHash          :: PG.Statement (UserID, String) ()
  }

open :: String -> Int -> String -> String -> String -> ExceptT String IO PostGreDB
open host port user pass database = do
  eConn <- liftIO $ PG.acquire $ 
                    PG.settings (T.encodeUtf8 $ T.pack host)
                                (fromIntegral port)
                                (T.encodeUtf8 $ T.pack user)
                                (T.encodeUtf8 $ T.pack pass)
                                (T.encodeUtf8 $ T.pack database)
  case eConn of
    Left err -> fail $ show err
    Right conn -> return $ PostGreDB
      { pgConnection = conn
      --, pgWhiteboardExists = PG.statement
      --    "SELECT board_id\
      --    \FROM whiteboard\
      --    \WHERE board_key = $1;"
      --    (PGE.param (PGE.nullable PGE.text))
      --    (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int4)))
      --    True
      , pgArchiveWhiteboardAccess = PG.Statement
          (BU.pack archiveWhiteboardAccessQuery)
          ((fst4 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd4 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (thd4 >$< PGE.param (PGE.nonNullable PGE.bool)) <>
           (fth4 >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          (PGD.noResult)
          True       
      , pgGiveWhiteboardAccess = PG.Statement
          (BU.pack giveWhiteboardAccessQuery)
          ((fst3 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd3 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (thd3 >$< PGE.param (PGE.nonNullable PGE.bool)))
          (PGD.noResult)
          True       
      , pgHasWhiteboardAccess = PG.Statement
          (BU.pack hasWhiteboardAccessQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
           )
          (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int4)))
          True
      , pgGetWhiteboardID = PG.Statement
          (BU.pack getWhiteboardIDQuery)
          (pack >$< PGE.param (PGE.nonNullable PGE.text))
          (PGD.singleRow (PGD.column (PGD.nullable PGD.int8)))
          True
      , pgWhiteboardInfo = PG.Statement
          (BU.pack getWhiteboardInfoQuery)
          (PGE.param (PGE.nonNullable PGE.int8))
          (PGD.singleRow $ 
             makeWhiteboardInfo
             <$> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
             <*> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
             <*> ((fmap unpack) <$> PGD.column (PGD.nullable PGD.text))
             <*>  PGD.column (PGD.nonNullable PGD.int8)
             <*>  PGD.column (PGD.nonNullable PGD.int8)
             <*>  PGD.column (PGD.nonNullable PGD.bool)
             <*>  PGD.column (PGD.nonNullable PGD.bool)
          )
          True
      , pgGetUsername = PG.Statement
          (BU.pack getUsernameQuery)
          (PGE.param (PGE.nonNullable PGE.int8))
          (PGD.singleRow (unpack <$> PGD.column (PGD.nonNullable PGD.text)))
          True
      , pgGetModificationDate = PG.Statement
          (BU.pack getModificationDateQuery)
          (PGE.param (PGE.nonNullable PGE.int8))
          (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int8)))
          True
      , pgIsUserKeyValid = PG.Statement
          (BU.pack isUserKeyValidQuery)
          (((pack . fst) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8)))
          (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int4)))
          True
      , pgIsWhiteboardArchived = PG.Statement
          (BU.pack isWhiteboardArchivedQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8)))
          (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int4)))
          True
      , pgUpdateArchiveExpirationDate = PG.Statement
          (BU.pack updateArchiveExpirationDateQuery)
          ((fst3 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd3 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (thd3 >$< PGE.param (PGE.nonNullable PGE.int8)))
          (PGD.noResult)
          True
      , pgUpdateModificationDate = PG.Statement
          (BU.pack updateModificationDateQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8)))
          (PGD.noResult)
          True
      , pgUpdateNumberOfPages = PG.Statement
          (BU.pack updateNumberOfPagesQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8)))
          (PGD.noResult)
          True
      , pgCleanArchive = PG.Statement
          (BU.pack cleanArchiveQuery)
          (PGE.param (PGE.nonNullable PGE.int8))
          PGD.noResult
          True
      , pgCreateUser = PG.Statement
          (BU.pack createUserQuery)
          (((pack . fst) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           ((pack . snd) >$< PGE.param (PGE.nonNullable PGE.text))           
          )
          (PGD.noResult)
          True
      , pgCreateUserSession = PG.Statement
          (BU.pack createUserSessionQuery)
          ((fst4 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           ((pack . snd4) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           (thd4 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (fth4 >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          (PGD.noResult)
          True
      , pgCreateWhiteboard = PG.Statement
          (BU.pack createWhiteboardQuery)
          ((fst5 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           ((pack . snd5) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           ((pack . thd5) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           (fth5 >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (ffh5 >$< PGE.param (PGE.nonNullable PGE.bool))
          )
          (PGD.noResult)
          True
      , pgDeleteUser = PG.Statement
          (BU.pack $ "BEGIN;" ++ concat deleteUserQueries ++ "COMMIT;")
          (PGE.param (PGE.nonNullable PGE.int8))
          PGD.noResult
          True
      , pgDeleteUserSession = PG.Statement
          (BU.pack deleteUserSessionQuery)
          (pack >$< PGE.param (PGE.nonNullable PGE.text))
          PGD.noResult
          True
      , pgDeleteWhiteboard = PG.Statement
          (BU.pack $ "BEGIN;" ++ concat deleteWhiteboardQueries ++ "COMMIT;")
          (PGE.param (PGE.nonNullable PGE.int8))
          PGD.noResult
          True
      , pgGetPasswordHash = PG.Statement
          (BU.pack getPasswordHashQuery)
          (PGE.param (PGE.nonNullable PGE.int8))
          (PGD.singleRow (unpack <$> PGD.column (PGD.nonNullable PGD.text)))
          True
      , pgGetUserIDFromKey = PG.Statement
          (BU.pack getUserIDFromKeyQuery)
          (pack  >$< PGE.param (PGE.nonNullable PGE.text))
          (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int8)))
          True
      , pgGetUserIDFromName = PG.Statement
          (BU.pack getUserIDFromNameQuery)
          (pack  >$< PGE.param (PGE.nonNullable PGE.text))
          (PGD.singleRow (PGD.column (PGD.nonNullable PGD.int8)))
          True
      , pgRemoveArchiveAccess = PG.Statement
          (BU.pack removeArchiveAccessQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          PGD.noResult
          True
      , pgRemoveExpiredData = PG.Statement
          (BU.pack $ "BEGIN;" ++ concat removeExpiredDataQueries ++ "COMMIT;")
          (PGE.param (PGE.nonNullable PGE.int8))
          PGD.noResult
          True
      , pgRemoveWhiteboardAccess = PG.Statement
          (BU.pack removeWhiteboardAccessQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          PGD.noResult
          True
      , pgRemoveWhiteboardIfOrphan = PG.Statement
          (BU.pack deleteWhiteboardIfOrphanQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          PGD.noResult
          True
      , pgUpdatePasswordHash = PG.Statement
          (BU.pack updatePasswordHashQuery)
          (((pack . snd) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           (fst >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          PGD.noResult
          True
      , pgRenewUserSession = PG.Statement
          (BU.pack renewUserSessionQuery)
          (((pack . fst) >$< PGE.param (PGE.nonNullable PGE.text)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          PGD.noResult
          True
      , pgAccessibleWhiteboards = PG.Statement
          (BU.pack accessibleWhiteboardsQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          (PGD.rowList $ 
               makeWhiteboardInfo
               <$> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> ((fmap unpack) <$> PGD.column (PGD.nullable PGD.text))
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
          )
          True
      , pgArchivedWhiteboards = PG.Statement
          (BU.pack archivedWhiteboardsQuery)
          ((fst >$< PGE.param (PGE.nonNullable PGE.int8)) <>
           (snd >$< PGE.param (PGE.nonNullable PGE.int8))
          )
          (PGD.rowList $ 
               makeWhiteboardInfo
               <$> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> ((fmap unpack) <$> PGD.column (PGD.nullable PGD.text))
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
          )
          True
      , pgPublicWhiteboards = PG.Statement
          (BU.pack publicWhiteboardsQuery)
          (PGE.noParams)
          (PGD.rowList $ 
               makeWhiteboardInfo
               <$> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> ((fmap unpack) <$> PGD.column (PGD.nullable PGD.text))
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
          )
          True
      , pgListWhiteboards = PG.Statement
          (BU.pack listWhiteboardsQuery)
          (PGE.noParams)
          (PGD.rowList $ 
               makeWhiteboardInfo
               <$> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> (unpack <$> PGD.column (PGD.nonNullable PGD.text))
               <*> ((fmap unpack) <$> PGD.column (PGD.nullable PGD.text))
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.int8)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
               <*>  PGD.column (PGD.nonNullable PGD.bool)
          )
          True
      , pgListUsers = PG.Statement
          (BU.pack listUsersQuery)
          (PGE.noParams)
          (PGD.rowList $ 
              unpack <$> PGD.column (PGD.nonNullable PGD.text)
          )
          True
      }
  where
    fst3 (x,_,_) = x
    snd3 (_,y,_) = y
    thd3 (_,_,z) = z
    fst4 (x,_,_,_) = x
    snd4 (_,y,_,_) = y
    thd4 (_,_,z,_) = z
    fth4 (_,_,_,w) = w
    fst5 (x,_,_,_,_) = x
    snd5 (_,y,_,_,_) = y
    thd5 (_,_,z,_,_) = z
    fth5 (_,_,_,w,_) = w
    ffh5 (_,_,_,_,v) = v

makeWhiteboardInfo bn bk un p modified public archived = 
  WhiteboardInfo
    { wiName = bn
    , wiOwner = un
    , wiModified = systemToUTCTime $ MkSystemTime{systemSeconds = modified, systemNanoseconds = 0}
    , wiPages = p
    , wiKey = bk
    , wiPublic = public
    , wiArchived = archived
    }

instance ZauboardDB PostGreDB where
  archiveWhiteboardAccess uid wbID now db = do
    let bi = topLevelID wbID
    _ <- PG.run (PG.statement (uid, bi, True, systemSeconds $ utcToSystemTime now)
                                (pgArchiveWhiteboardAccess db))
                  (pgConnection db)
    return ()
  boardExists boardKey db = do
    res <- PG.run (PG.statement boardKey
                                (pgGetWhiteboardID db))
                  (pgConnection db)
    case res of
      Right c -> return $ isJust c
      Left _ -> return False
  cleanArchive now db = do
    PG.run (PG.statement (systemSeconds $ utcToSystemTime now) (pgCleanArchive db))
           (pgConnection db)
    return ()
  close db = PG.release $ pgConnection db
  isWhiteboardArchived uid wbID db = do
    let bi = topLevelID wbID
    res <- PG.run (PG.statement (uid, bi)
                                (pgIsWhiteboardArchived db))
                  (pgConnection db)
    case res of
      Right c -> return $ c > 0
      Left _ -> return False
  isUserKeyValid userKey now db = do
    let nowS = systemSeconds $ utcToSystemTime now
    res <- PG.run (PG.statement (userKey, nowS)
                                (pgIsUserKeyValid db))
                  (pgConnection db)
    case res of
      Right c -> return $ c > 0
      Left _ -> return False
  giveWhiteboardAccess uid wbID db = do
    let bi = topLevelID wbID
    _ <- PG.run (PG.statement (uid, bi, True)
                                (pgGiveWhiteboardAccess db))
                  (pgConnection db)
    return ()
  getUsername uid db = do
    res <- PG.run (PG.statement uid
                                (pgGetUsername db))
                  (pgConnection db)
    case res of
      Right str -> return $ Just str
      Left _ -> return Nothing
  updateArchiveExpirationDate uid wbID now db = do
    let bi = topLevelID wbID
        nowS = systemSeconds $ utcToSystemTime now
    res <- PG.run (PG.statement (uid, bi, nowS)
                                (pgUpdateArchiveExpirationDate db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  updateModificationDate wbID now db = do
    let bi = topLevelID wbID
        nowS = systemSeconds $ utcToSystemTime now
    res <- PG.run (PG.statement (bi, nowS)
                                (pgUpdateModificationDate db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  updateNumberOfPages wbID pages db = do
    let bi = topLevelID wbID
    res <- PG.run (PG.statement (bi, pages)
                                (pgUpdateNumberOfPages db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  getWhiteboardInfo wbID db = do
    let bi = topLevelID wbID
    res <- PG.run (PG.statement bi
                                (pgWhiteboardInfo db))
                  (pgConnection db)
    case res of
      Right i -> return $ Just i
      Left _ -> return Nothing
  createUserSession ui uk now t db = do
    let MkFixed dt' = nominalDiffTimeToSeconds t
        dt = fromIntegral $ dt' `div` 10^12
    res <- PG.run (PG.statement (ui, uk, systemSeconds $ utcToSystemTime now, dt)
                                (pgCreateUserSession db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  createUser un ph db = do
    res <- PG.run (PG.statement (un, ph)
                                (pgCreateUser db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  createWhiteboard bk bn ui now public db = do
    res <- PG.run (PG.statement (ui, bn, bk, systemSeconds $ utcToSystemTime now, public)
                                (pgCreateWhiteboard db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  deleteUserSession uk db = do
    res <- PG.run (PG.statement uk
                                (pgDeleteUserSession db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  deleteUser ui db = do
    res <- PG.run (PG.statement ui
                                (pgDeleteUser db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  deleteWhiteboard wbID db = do
    res <- PG.run (PG.statement (topLevelID wbID)
                                (pgDeleteWhiteboard db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  getWhiteboardID bk db = do
    res <- PG.run (PG.statement bk
                                (pgGetWhiteboardID db))
                  (pgConnection db)
    case res of
      Right i -> return $ fmap WhiteboardTopLevelID i
      Left _ -> return Nothing
  getPasswordHash ui db = do
    res <- PG.run (PG.statement ui
                                (pgGetPasswordHash db))
                  (pgConnection db)
    case res of
      Right i -> return $ Just i
      Left _ -> return Nothing
  getUserIDFromKey uk db = do
    res <- PG.run (PG.statement uk
                                (pgGetUserIDFromKey db))
                  (pgConnection db)
    case res of
      Right i -> return $ Just i
      Left _ -> return Nothing
  getUserIDFromName un db = do
    res <- PG.run (PG.statement un
                                (pgGetUserIDFromName db))
                  (pgConnection db)
    case res of
      Right i -> return $ Just i
      Left _ -> return Nothing
  getModificationDate wbID db = do
    res <- PG.run (PG.statement (topLevelID wbID)
                                (pgGetModificationDate db))
                  (pgConnection db)
    case res of
      Right i -> return $ Just $ secondsToUTC i
      Left _ -> return Nothing
  hasWhiteboardAccess ui wbID db = do
    res <- PG.run (PG.statement (ui, topLevelID wbID)
                                (pgHasWhiteboardAccess db))
                  (pgConnection db)
    case res of
      Right c -> return $ c > 0
      Left _ -> return False
  listWhiteboards db = do
    res <- PG.run (PG.statement ()
                                (pgListWhiteboards db))
                  (pgConnection db)
    case res of
      Right c -> return $ c
      _ -> return []
  listUsers db = do
    res <- PG.run (PG.statement ()
                                (pgListUsers db))
                  (pgConnection db)
    case res of
      Right c -> return $ c
      _ -> return []
  removeArchiveAccess ui wbID db = do
    res <- PG.run (PG.statement (ui, topLevelID wbID)
                                (pgRemoveArchiveAccess db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  removeExpiredData now db = do
    res <- PG.run (PG.statement (systemSeconds $ utcToSystemTime now)
                                (pgRemoveExpiredData db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  removeWhiteboardAccess ui wbID db = do
    res <- PG.run (PG.statement (ui, topLevelID wbID)
                                (pgRemoveWhiteboardAccess db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  deleteWhiteboardIfOrphan wbID now db = do
    res <- PG.run (PG.statement (topLevelID wbID, systemSeconds $ utcToSystemTime now)
                                (pgRemoveWhiteboardIfOrphan db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  updatePasswordHash ui ph db = do
    res <- PG.run (PG.statement (ui, ph)
                                (pgUpdatePasswordHash db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  renewUserSession uk now db = do
    res <- PG.run (PG.statement (uk, systemSeconds $ utcToSystemTime now)
                                (pgRenewUserSession db))
                  (pgConnection db)
    case res of
      Right _ -> return ()
      Left _ -> return ()
  accessibleWhiteboards ui now db = do
    res1 <- PG.run (PG.statement (ui, systemSeconds $ utcToSystemTime now)
                                (pgAccessibleWhiteboards db))
                  (pgConnection db)
    res2 <- PG.run (PG.statement ()
                                (pgPublicWhiteboards db))
                  (pgConnection db)
    case (res1, res2) of
      (Right c1, Right c2) -> return $ c1 ++ c2
      _ -> return []
  archivedWhiteboards ui now db = do
    res <- PG.run (PG.statement (ui, systemSeconds $ utcToSystemTime now)
                                (pgArchivedWhiteboards db))
                  (pgConnection db)
    case res of
      Right c -> return $ c
      _ -> return []
