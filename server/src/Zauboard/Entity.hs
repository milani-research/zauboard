module Zauboard.Entity where

import           Control.Concurrent
import           Control.Concurrent.MVar
import qualified Data.Map as M

type Position = (Double, Double)

type ConnectionID = Int
type UserID = Int
type DrawID = Int
type Color = String
type Drawing = (ElementID, DrawingElement)

data ElementID = ElementID UserID DrawID
data WhiteboardID = WhiteboardTopLevelID Int | WhiteboardPageID Int Int

data DrawingElement = 
  | Circle Position Double Color Double
  | Path [Position] [Double] Color Bool

data WhiteboardPage = 
  WhiteboardPage
  { pgDrawings :: M.Map ElementID Drawing
  , pgErased   :: M.Map ElementID Drawing
  }

data Whiteboard = 
  Whiteboard
  { wbNumPages :: Int
  , wbBookmarks :: M.Map String Int
  }

data Zauboard a = 
  Zauboard
  { zbBoards            :: M.Map a (MVar Whiteboard)
  , zbDatabase          :: Database a
  , zbConnections       :: M.Map ConnectionID (MVar WhiteboardPage)
  , zbFreeConnectionIds :: [ConnectionID]
  }
