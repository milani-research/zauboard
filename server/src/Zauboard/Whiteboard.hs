module Zauboard.Whiteboard
       ( Context(..)
       , Page
       , accessWhiteboard
       , createWhiteboard
       , clear
       , create
       , erase
       , addMissing
       , insert
       , insertIfMissing
       , info
       , listPages
       , loadPage
       , restore
       , store
       , takeAll
       , takeAllOnce
       , takeDrawings
       , unload
       , withWhiteboard
       , pageModificationDate
       , addPdf
       , pdfPath
       )
where

import           Zauboard
import           Zauboard.Utils
import qualified Zauboard.Drawing  as P
import qualified Zauboard.Database as D
import Control.Concurrent
import Control.Exception
import Control.Monad (when, forM)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Except
import Control.Monad.Trans.State
import Crypto.Random
import Data.Fixed
import Data.Maybe
import Data.Time
import qualified Data.ByteString         as BS
import qualified Data.ByteString.Lazy    as LBS
import qualified Data.ByteString.Base64  as BS64
import qualified Data.Map.Strict         as M
import qualified Data.Set                as S
import qualified Data.Text               as T
import qualified Data.Text.Encoding      as T
import qualified Data.Text.Lazy.Encoding as LT
import qualified Data.Text.Lazy          as LT
import System.Directory
import System.FilePath
import System.IO
import System.Process

data Context = 
  Context
  { cxPages         :: MVar (M.Map WhiteboardID Page)
  , cxPageDir       :: FilePath
  , cxDatabase      :: D.Database
  , cxAutosaveInterval :: Maybe NominalDiffTime
  , cxUnloadInterval   :: Maybe NominalDiffTime
  , cxXZCompression :: Bool
  }

data Page = 
  Page
  { pgDrawings         :: MVar (M.Map ElementID DrawingElement)
  , pgErased           :: MVar (M.Map ElementID DrawingElement)
  , pgMissing          :: MVar (S.Set ElementID)
  , pgActive           :: MVar Bool
  , pgValid            :: MVar Bool
  , pgModified         :: MVar Bool
  , pgLastModified     :: MVar UTCTime
  }

create db = do
  pagesM <- newMVar M.empty
  let cx = Context
            { cxPages = pagesM
            , cxDatabase = db
            , cxPageDir = "zauboard"
            , cxAutosaveInterval = Nothing
            , cxUnloadInterval = Nothing
            , cxXZCompression = True
            }
  liftIO $ forkIO $ updateModificationDatesLoop (secondsToNominalDiffTime $ MkFixed $ secondsToPico 60) cx
  return cx

accessWhiteboard whiteboardName page = do
  mID <- runDatabase $ D.getWhiteboardID whiteboardName
  return $ fmap (\w -> WhiteboardPageID (topLevelID w) page) mID

createWhiteboard ownerID wname isPublic = do
  key <- generateUniqueWhiteboardKey
  now <- liftIO $ getCurrentTime
  runDatabase $ D.createWhiteboard wname key ownerID now isPublic
  wbid <- runDatabase $ D.getWhiteboardID key
  return (key, wbid, now)

generateUniqueWhiteboardKey :: StateT Context IO String
generateUniqueWhiteboardKey = do
  drg <- liftIO $ getSystemDRG
  let (bs, _) = randomBytesGenerate 18 drg :: (BS.ByteString, SystemDRG)
  let key = T.unpack $ T.decodeUtf8 $ BS64.encode bs
  keyExists <- runDatabase $ D.boardExists key
  if keyExists then
    generateUniqueWhiteboardKey
  else
    return key


listPages whiteboardKey = do
  mID <- runDatabase $ D.getWhiteboardID whiteboardKey
  case mID of
    Just wid@(WhiteboardTopLevelID i) -> do
      minfo <- runDatabase $ D.getWhiteboardInfo wid
      return $ maybe [] (\pInfo -> [ WhiteboardPageID i p | p <- [1..wiPages pInfo]]) minfo
    Nothing -> return []

info key = do
  mID <- runDatabase $ D.getWhiteboardID key
  case mID of
    Just wid@(WhiteboardTopLevelID _) -> do
      minfo <- runDatabase $ D.getWhiteboardInfo wid
      return $ fmap (\j -> (j, wid)) minfo
    Nothing -> return Nothing

-- | Execute an action on the page with the given ID.
-- | Page is loaded or created as necessary.
withWhiteboard f whiteboardID = do
  cx <- get
  pages <- liftIO $ readMVar $ cxPages cx
  page <- case M.lookup whiteboardID pages of
    Nothing -> do
      pages' <- liftIO $ takeMVar $ cxPages cx
      -- Lookup again in case another thread loaded the page
      case M.lookup whiteboardID pages' of
        Nothing -> getPage whiteboardID pages'
        Just pg -> do
          liftIO $ modifyMVar_ (pgActive pg) (\_ -> return True)
          liftIO $ putMVar (cxPages cx) pages'
          return pg
    Just pg -> do
      liftIO $ modifyMVar_ (pgActive pg) (\_ -> return True)
      valid <- liftIO $ readMVar (pgValid pg)
      if valid then
        return pg
      else do
        pages' <- liftIO $ takeMVar $ cxPages cx
        getPage whiteboardID pages'
  liftIO $ evalStateT f page

setupAutosave whiteboardID page = do  
  cx <- get
  liftIO $ createDirectoryIfMissing True (cxPageDir cx)
  let busyFile = (cxPageDir cx) </> ".whiteboard-busy"
  busyFileExists <- liftIO $ doesFileExist busyFile
  when (not busyFileExists) $ liftIO $ do
    writeFile busyFile ""
  let mAutoI = cxAutosaveInterval cx
      mUnloadI = cxUnloadInterval cx
  when (isJust mAutoI) $
    liftIO $ (forkIO $ autosaveLoop whiteboardID page (fromJust mAutoI) cx) >> return ()
  when (isJust mUnloadI) $
    liftIO $ (forkIO $ unloadLoop busyFile whiteboardID page (cxPages cx) (max (fromJust mUnloadI) (fromMaybe 0 mAutoI))) >> return ()
  return ()

updateModificationDatesLoop interval cx = do
  repeatLoop interval $ do
    pagesM <- readMVar $ cxPages cx
    forM (M.assocs pagesM) $ \(wid@(WhiteboardPageID _ pageI), page) -> do
      lastModified <- readMVar (pgLastModified page)
      D.updateModificationDate wid lastModified (cxDatabase cx)
      D.updateNumberOfPages wid pageI (cxDatabase cx)
    return True

autosaveLoop whiteboardID page interval cx = 
  repeatLoop interval $ do
    modified <- readMVar $ pgModified page
    when modified $ do
      modifyMVar_ (pgModified page) (\_ -> return False)
      drawings <- readMVar $ pgDrawings page
      evalStateT (store whiteboardID $ map Drawing $ M.assocs drawings) cx
    valid <- readMVar (pgValid page)
    return valid

unloadLoop busyFile whiteboardID page pageM interval = 
  repeatLoop interval $ do
    active <- takeMVar $ pgActive page
    when (not active) $ do
      unloadPage busyFile page pageM whiteboardID
      return ()
    putMVar (pgActive page) False
    readMVar (pgValid page)

unload whiteboardID = do
  cx <- get
  let busyFile = (cxPageDir cx) </> ".whiteboard-busy"
  withWhiteboard (get >>= (\page -> liftIO $ unloadPage busyFile page (cxPages cx) whiteboardID)) whiteboardID

unloadPage busyFile page pageM whiteboardID = do
  takeMVar (pgValid page)
  modifyMVar_ (pgModified page) (\_ -> return False)
  pages <- takeMVar pageM
  putMVar (pgValid page) False
  takeMVar $ pgDrawings page
  takeMVar $ pgErased page
  let m' = (M.delete whiteboardID pages)
      busy = M.null m'
  when (not busy) $ liftIO $ do
    exists <- doesFileExist busyFile
    when exists $ removeFile busyFile
  seq m' $ putMVar pageM m'

addMissing eids = 
  withPage (\page -> modifyMVar_ (pgMissing page) 
                              (\ds -> return $ foldr (\e s -> S.insert e s) ds eids
                              ))
insertIfMissing drawings = 
  withPage (\page -> modifyMVar_ (pgDrawings page) 
                              (\dm -> do
                                missing <- takeMVar (pgMissing page)
                                putMVar (pgMissing page)
                                        (foldr (\(Drawing (e,d)) s -> S.delete e s) missing drawings)
                                let found = filter (\(Drawing (k,d)) -> k `S.member` missing) drawings
                                return $ foldr (\(Drawing (e,d)) m -> M.insert e d m)
                                            dm
                                            found) >> return [])

insert drawings =
  withPage (\page -> modifyMVar_ (pgDrawings page) 
                              (\dm -> return $ foldr (\(Drawing (k,d)) m -> M.insert k d m)
                                            dm
                                            drawings) >> return [])

erase   = swapDrawings pgDrawings pgErased

restore = swapDrawings pgErased pgDrawings

swapDrawings from to eIDs = do
  missing <- withPage (\page -> do
    m <- readMVar $ from page
    let movedDrawings = [ (e, fromJust md) | e <- eIDs, let md = e `M.lookup` m, isJust md]
        missing = [ e | e <- eIDs, let md = e `M.lookup` m, isNothing md]
    modifyMVar_ (to page)   (\toM -> return $ foldr (\(e,d) m' -> M.insert e d m') toM   movedDrawings)
    modifyMVar_ (from page) (\fromM -> return $ foldr (\(e,_) m' -> M.delete e m') fromM movedDrawings)
    return missing
    )
  return missing

clear :: StateT Page IO ()
clear = do
  withPage (\page -> do
    drawings <- takeMVar (pgDrawings page)
    putMVar (pgDrawings page) M.empty
    modifyMVar_ (pgErased page) (\m -> return $ M.union drawings m)
    )

withPage :: (Page -> IO a) -> StateT Page IO a
withPage f = do
  page <- get
  liftIO $ do
    takeMVar $ pgModified page
    takeMVar $ pgLastModified page
    out <- f page
    now <- getCurrentTime
    putMVar (pgLastModified page) now
    putMVar (pgModified page) True
    return out

takeDrawings eIDs = do
  withPage (\page -> do
    drawings <- readMVar $ pgDrawings page
    return $ foundAndMissing [] [] $
              [ (e, md)
              | e <- eIDs
              , let md = M.lookup e drawings
              ]
    )
  where
    foundAndMissing found missing [] = (found, missing)
    foundAndMissing found missing ((e, md):es)
      | isJust md = foundAndMissing (Drawing (e, fromJust md) : found) missing es
      | otherwise = foundAndMissing found (e:missing) es

takeAll :: StateT Page IO [Drawing]
takeAll = withPage (\page -> fmap (map Drawing . M.assocs) $ readMVar (pgDrawings page))

takeAllOnce :: WhiteboardID -> StateT Context IO [Drawing]
takeAllOnce whiteboardID = do
  withWhiteboardNoCache
    (\pg -> liftIO $ evalStateT takeAll pg)
    (fmap (fromMaybe [] . fmap ((map Drawing . M.assocs) . snd)) $ loadPage whiteboardID)
    whiteboardID

--  pages <- liftIO $ readMVar $ cxPages cx
--  drawings <- case M.lookup whiteboardID pages of
--    Nothing -> fmap (fromMaybe [] . fmap ((map Drawing . M.assocs) . snd)) $ loadPage whiteboardID
--    Just pg -> do
--      valid <- liftIO $ readMVar (pgValid pg)
--      if valid then
--        liftIO $ evalStateT takeAll pg
--      else
--        fmap (fromMaybe [] . fmap ((map Drawing . M.assocs) . snd)) $ loadPage whiteboardID
--  return drawings

getPage whiteboardID pages = do
  cx <- get
  pg <- liftIO $ do
    drawingsM <- newMVar M.empty
    erasedM   <- newMVar M.empty
    missingM  <- newMVar S.empty
    modifiedM <- newMVar False
    activeM   <- newMVar True
    validM    <- newMVar True
    lastModifiedM <- newEmptyMVar
    takeMVar drawingsM
    return $ Page{ pgDrawings = drawingsM
                 , pgErased = erasedM
                 , pgMissing = missingM
                 , pgModified = modifiedM
                 , pgActive = activeM
                 , pgValid = validM
                 , pgLastModified = lastModifiedM
                 }
  liftIO $ putMVar (cxPages cx) (M.insert whiteboardID pg pages)
  storedPage <- loadPage whiteboardID
  (lastModified, drawings) <- case storedPage of 
    Nothing -> do
      now <- liftIO $ getCurrentTime
      return (now, M.empty)
    Just result -> return result
  liftIO $ putMVar (pgDrawings pg) drawings
  liftIO $ putMVar (pgLastModified pg) lastModified
  setupAutosave whiteboardID pg
  return pg

loadPage (WhiteboardPageID wbID pgID) = do
  cx <- get
  let pagePath' = cxPageDir cx </> (show wbID) </> "pages" </> (show pgID)
      pagePath
        | cxXZCompression cx = pagePath' <.> "xz"
        | otherwise = pagePath'
  pageExists <- liftIO $ doesFileExist pagePath
  if pageExists then do
    bstr <- liftIO $ if cxXZCompression cx then do
              res <- try $ do
                (_, Just sout, Just serr, _) <- createProcess (proc "xz" ["--decompress", "--keep", "--stdout", pagePath]){std_out = CreatePipe, std_err = CreatePipe}
                forkIO $ do
                  err <- hGetContents serr
                  seq (length err) (return ())
                out <- LBS.hGetContents sout
                return out
              case res of
                Left err -> do
                  putStrLn $ "Whiteboard " ++ show (WhiteboardPageID wbID pgID)
                  print (err :: SomeException)
                  return LBS.empty
                Right out -> return out
           else
              LBS.readFile pagePath
    lastModified <- liftIO $ do
      mtime <- try $ getModificationTime pagePath
      case mtime of
        Left err -> seq (err :: SomeException) $ getCurrentTime
        Right t -> return t
    let txt = LT.decodeUtf8 bstr
        str = LT.unpack $ LT.decodeUtf8 $ LBS.take 3 bstr
        mdrawings = case str of
          ('[':_) -> P.loadDrawings "0" bstr
          _ -> let (v,_) = span (/='\n') str in P.loadDrawings v (LT.encodeUtf8 $ LT.drop 1 $ LT.dropWhile (/='\n') txt)
    return $ case mdrawings of
      Nothing -> Nothing
      Just drawingsList ->
        seq (length drawingsList) $ Just (lastModified, toMap drawingsList)
  else
    return Nothing

addPdf :: WhiteboardID -> String -> BS.ByteString -> StateT Context IO ()
addPdf wid name pdf = do
  cx <- get
  let path = pdfPath wid name cx
  liftIO $ createDirectoryIfMissing True (takeDirectory path)
  liftIO $ BS.writeFile path pdf  

pdfPath :: WhiteboardID -> String -> Context -> FilePath
pdfPath wbID pdfName cx = 
  let pageDir = cxPageDir cx
  in pageDir </> (show $ topLevelID wbID) </> "pdf" </> pdfName <.> "pdf"

pageModificationDate :: WhiteboardID -> StateT Context IO UTCTime
pageModificationDate whiteboardID@(WhiteboardPageID wbID pgID) = do
  cx <- get
  withWhiteboardNoCache
    (\pg -> liftIO $ readMVar $ pgLastModified pg)
    (do
     let pagePath' = cxPageDir cx </> (show wbID) </> "pages" </> (show pgID)
         pagePath
           | cxXZCompression cx = pagePath' <.> "xz"
           | otherwise = pagePath'
     liftIO $ do
       mtime <- try $ getModificationTime pagePath
       case mtime of
         Left err -> seq (err :: SomeException) $ getCurrentTime
         Right t -> return t
    )
    whiteboardID

withWhiteboardNoCache fCached fNotCached whiteboardID = do
  cx <- get
  pages <- liftIO $ readMVar $ cxPages cx
  result <- case M.lookup whiteboardID pages of
    Nothing -> fNotCached
    Just pg -> do
      valid <- liftIO $ readMVar (pgValid pg)
      if valid then
        fCached pg
      else
        fNotCached
  return result
  
toMap = foldr (\d -> M.insert (elementID d) (drawing d)) M.empty

store (WhiteboardPageID wbID pgID) drawings = do
  cx <- get
  let dir = (cxPageDir cx) </> (show wbID) </> "pages"
      flPath = dir </> (show pgID)
  liftIO $ createDirectoryIfMissing True dir
  liftIO $
    if cxXZCompression cx then do
      xzCompress (flPath <.> "xz") str
    else 
      writeFile flPath str >> return ()
  where
    str = "1\n" ++ P.packDrawings drawings

xzCompress fl str = do
  (Just hIn,Just hOut,_,ph) <- createProcess (proc "xz" ["--compress","-"]){std_out = CreatePipe, std_in = CreatePipe}
  forkIO $ do
    hPutStr hIn str
    hClose hIn
  hFl <- openFile fl WriteMode
  cStr <- BS.hGetContents hOut
  BS.hPut hFl cStr
  hClose hFl
  waitForProcess ph
  return ()

runDatabase f = do
  cx <- get
  liftIO $ f (cxDatabase cx)


