module Zauboard.Interaction
       ( Context
       , Room
       , authenticate
       , finishedRequest
       , getRoom
       , gotRequest
       , create
       , join
       , joinGlobal
       , leave
       , leaveGlobal
       , pending
       , provide
       , roomInfo
       , others
       , setTag
       , getTags
       )
where

import Zauboard

import           Control.Monad.Except hiding (join)
import           Control.Concurrent.MVar -- .Strict
import           Control.Monad.Trans.State
import           Control.Monad.IO.Class (liftIO)
import           Data.Maybe
import qualified Data.Map.Strict as M
import qualified Data.Set        as S

data Room = 
    Room
    { roPending           :: MVar (M.Map ElementID [ConnectionID])
    , roLocalConnections  :: MVar (S.Set ConnectionID)
    , roGlobalConnections :: MVar (S.Set ConnectionID)
    , roBlockPending      :: MVar ()
    }
data Context =
  Context
    { cxUsers       :: MVar (M.Map ConnectionID (MVar UserTag))
    , cxRooms       :: MVar (M.Map WhiteboardID Room)
    , cxConnections :: MVar (M.Map WhiteboardID (MVar (S.Set ConnectionID)))
    }

create = do
  users       <- newMVar M.empty
  rooms       <- newMVar M.empty
  connections <- newMVar M.empty
  return Context{cxUsers = users, cxRooms = rooms, cxConnections = connections}

roomInfo :: StateT Room IO (M.Map ElementID [ConnectionID], S.Set ConnectionID, S.Set ConnectionID)
roomInfo = do
  room <- get
  liftIO $ do
    pendingMap  <- readMVar $ roPending room
    localConns  <- readMVar $ roLocalConnections room
    globalConns <- readMVar $ roGlobalConnections room
    return (pendingMap, localConns, globalConns)

getRoom :: WhiteboardID -> StateT Context IO Room
getRoom whiteboardID = do
  cx <- get
  rooms <- liftIO $ takeMVar $ cxRooms cx
  case whiteboardID `M.lookup` rooms of
    Just room -> do
      liftIO $ putMVar (cxRooms cx) rooms
      return room
    Nothing -> do
      pendingMap <- liftIO $ newMVar M.empty
      -- users   <- liftIO $ newMVar M.empty
      connections <- liftIO $ takeMVar $ cxConnections cx
      globalConnections <- case (topLevel whiteboardID) `M.lookup` connections of
        Just conns -> do
          liftIO $ putMVar (cxConnections cx) connections
          return conns
        Nothing -> do
          gConns <- liftIO $ newMVar S.empty
          liftIO $ putMVar (cxConnections cx) (M.insert (topLevel whiteboardID) gConns connections)
          return gConns
      localConnections <- liftIO $ newMVar S.empty
      blockPending <- liftIO $ newMVar ()
      let room = Room { roPending = pendingMap
                      , roLocalConnections  = localConnections
                      , roGlobalConnections = globalConnections
                      , roBlockPending = blockPending
                      }
      liftIO $ putMVar (cxRooms cx) (M.insert whiteboardID room rooms)
      return room


setTag connectionID username tagFG tagBG = do
  cx <- get
  liftIO $ do
    users <- readMVar $ cxUsers cx
    case M.lookup connectionID users of
      Just tagM -> modifyMVar_ tagM
                               (\tag -> return tag{ uUserName = username
                                                  , uTagFG = tagFG
                                                  , uTagBG = tagBG
                                                  })
      _ -> return ()

getTags connectionIDs = do
  cx <- get
  liftIO $ do
    users <- readMVar $ cxUsers cx
    mapM readMVar
          [ fromJust mTag
          | i <- connectionIDs
          , let mTag = M.lookup i users
          , isJust mTag]

join connectionID = do
  room <- get
  liftIO $ modifyMVar_ (roLocalConnections room) (return . S.insert connectionID)

leave connectionID = do
  room <- get
  liftIO $ modifyMVar_ (roLocalConnections  room) (return . S.delete connectionID)

joinGlobal connectionID = do
  room <- get
  liftIO $ modifyMVar_ (roGlobalConnections room) (return . S.insert connectionID)
 
leaveGlobal connectionID = do
  room <- get
  liftIO $ modifyMVar_ (roGlobalConnections room) (return . (S.delete connectionID))

authenticate connectionID tagM = do
  -- let user = UserTag{uUserName = username, uTagBG = tagBG, uTagFG = tagFG}
  cx <- get
  liftIO $ do
    -- tagM <- newMVar user
    modifyMVar_ (cxUsers cx) (return . M.insert connectionID tagM)

provide drawings = do
  room <- get
  pendingD <- liftIO $ takeMVar (roPending room)
  let (resolved, pending') = solvePending drawings pendingD
  liftIO $ putMVar (roPending room) pending'
  return resolved

solvePending :: [Drawing] -> M.Map ElementID [ConnectionID] -> (M.Map ConnectionID [Drawing], M.Map ElementID [ConnectionID])
solvePending drawings pendingD
  | M.null pendingD = (M.empty, pendingD)
  | otherwise = 
    let eIDs = S.fromList $ map elementID drawings
        resolved = foldr (\(u,ds) m -> M.insertWith (++) u ds m) M.empty $ concat
          [ [ (u, [Drawing (eID, d)]) | u <- fromJust users]
          | Drawing (eID, d) <- drawings
          , let users = M.lookup eID pendingD
          , isJust users
          ]
    in (resolved, M.withoutKeys pendingD eIDs)

pending :: [ElementID] -> ConnectionID -> StateT Room IO  ()
pending missing connectionID = do
  room <- get
  liftIO $ modifyMVar_ (roPending room) $ \pendingMap -> do
    return $ foldr (\e m -> M.insertWith (++) e [connectionID] m) pendingMap missing
  return ()

gotRequest :: StateT Room IO ()
gotRequest = do
  room <- get
  liftIO $ takeMVar $ roBlockPending room

finishedRequest :: StateT Room IO ()
finishedRequest = do
  room <- get
  liftIO $ putMVar (roBlockPending room) ()

others :: ConnectionID -> StateT Room IO [ConnectionID]
others sender = do
  room <- get
  localConns <- liftIO $ readMVar $ roLocalConnections room
  return $ S.toList $ S.filter (/=sender) localConns

