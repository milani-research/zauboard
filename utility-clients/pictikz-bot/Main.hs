{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent
import Control.Concurrent.MVar
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.State
import Data.List
import Data.Maybe
import Network.Socket (withSocketsDo)
import qualified Data.Map             as M
import qualified Data.Text            as T
import qualified Data.Text.IO         as T
import qualified Network.URI          as N
import qualified Network.WebSockets   as WS
import qualified Wuss                 as WSS
import System.Directory
import System.Environment
import System.IO
import System.Process
import Text.Read (readMaybe)
import Zauboard
import Zauboard.Export.SVG
import Zauboard.Package

type Whiteboard = M.Map ElementID DrawingElement

data Context = Context
  { cxWhiteboardsM :: MVar (M.Map PageID (MVar Whiteboard))
  , cxRemovedWhiteboardsM :: MVar (M.Map PageID (MVar Whiteboard))
  , cxPageM :: MVar PageID
  , cxOutputThread :: MVar (Maybe ThreadId)
  }

data Config = Config
  { cfURL        :: Maybe String
  , cfUsername   :: Maybe String
  , cfPassword   :: Maybe String
  , cfToken      :: Maybe String
  , cfBoardKey   :: Maybe String
  , cfPage       :: Maybe PageID
  , cfError      :: Maybe String
  , cfContext    :: Context    
  }

emptyConfig = Config
  { cfURL = Nothing
  , cfUsername = Nothing
  , cfPassword = Nothing
  , cfToken = Nothing
  , cfBoardKey = Nothing
  , cfError = Nothing
  }

data Reply a = Reply a | NoReply | Quit

main = do
  args <- getArgs
  cx <- newContext
  let conf = parseArgs emptyConfig{cfContext = cx} args
  case cfError conf of
    Just err -> putStrLn err
    Nothing -> connect conf

newContext = do
  whiteboardsM <- newMVar M.empty
  removedM     <- newMVar M.empty
  pageM        <- newMVar 1
  outputThreadM <- newMVar Nothing
  return Context
    { cxWhiteboardsM = whiteboardsM
    , cxRemovedWhiteboardsM = removedM
    , cxPageM = pageM
    , cxOutputThread = outputThreadM
    }

parseArgs conf [] = conf
parseArgs conf args = 
  let (conf', args') = case args of
                         ("--username":uname:args1) -> (conf{cfUsername = Just uname},   args1)
                         ("--password":pwd:args1)   -> (conf{cfPassword = Just pwd},     args1)
                         ("--token":token:args1)    -> (conf{cfToken = Just token},      args1)
                         (('-':arg):args1)          -> (conf{cfError = Just $ "Unknown option " ++ '-':arg}, [])
                         (str:args1)                -> (conf{cfURL = Just str},          args1)
  in parseArgs conf' args'

client :: Config -> WS.ClientApp ()
client conf conn = do
  WS.sendTextData conn $ LoginAsGuest
  pkg0 <- WS.receiveData conn :: IO Package -- access granted
  pkg1 <- WS.receiveData conn :: IO Package -- userid
  case cfBoardKey conf of
    Just key -> do
      let page = fromMaybe 1 $ cfPage conf
      WS.sendTextData conn $ JoinWhiteboard key page
      runWhiteboard conf $ switchPage page
      forever $ do
        pkg <- WS.receiveData conn
        -- let pkg = parsePackage msg
        forkIO $ serverOutput conf $ (pkg :: Package)
        -- takeMVar quitM
      WS.sendClose conn ("" :: T.Text)
    Nothing -> return ()

serverOutput conf pkg = do
  case pkg of
    Drawings drawings -> runWhiteboard conf $ add drawings    >> refresh
    Provide drawings  -> runWhiteboard conf $ add drawings    >> refresh
    Restore eIDs      -> runWhiteboard conf $ restore eIDs    >> refresh
    Erase eIDs        -> runWhiteboard conf $ remove  eIDs    >> refresh
    Clear             -> runWhiteboard conf $ removeAll       >> refresh
    SwitchPage page   -> runWhiteboard conf $ switchPage page >> refresh
    _                 -> return ()
 
add drawings = do
  cx <- get
  liftIO $ do
    page <- readMVar $ cxPageM cx
    whiteboards <- readMVar $ cxWhiteboardsM cx
    modifyMVar_ (whiteboards M.! page)
                (\ds -> return $ foldr (\(Drawing (eID, d)) m -> M.insert eID d m) ds drawings)

remove eids = do
  cx <- get
  liftIO $ do
    page <- readMVar $ cxPageM cx
    whiteboards <- readMVar $ cxWhiteboardsM cx
    removedWhiteboards <- readMVar $ cxRemovedWhiteboardsM cx
    elements <- modifyMVar (whiteboards M.! page)
                           (\ds -> 
                                let es = map fromJust $ filter isJust $ map (\e -> M.lookup e ds) eids
                                in
                                return ( foldr (\eID m -> M.delete eID m) ds eids
                                       , zipWith (\e d -> Drawing (e,d)) eids es)
                           )
    modifyMVar_ (removedWhiteboards M.! page)
                (\ds -> return $ foldr (\(Drawing (eID, d)) m -> M.insert eID d m) ds elements)
              
removeAll = do
  cx <- get
  liftIO $ do
    page <- readMVar $ cxPageM cx
    whiteboards <- readMVar $ cxWhiteboardsM cx
    removedWhiteboards <- readMVar $ cxRemovedWhiteboardsM cx
    elements <- modifyMVar (whiteboards M.! page)
                           (\ds -> 
                            return ( M.empty
                                   , map Drawing $ M.assocs ds )
                           )
    modifyMVar_ (removedWhiteboards M.! page)
                (\ds -> return $ foldr (\(Drawing (eID, d)) m -> M.insert eID d m) ds elements)

restore eids = do
  cx <- get
  liftIO $ do
    page <- readMVar $ cxPageM cx
    whiteboards <- readMVar $ cxWhiteboardsM cx
    removedWhiteboards <- readMVar $ cxRemovedWhiteboardsM cx
    elements <- modifyMVar (removedWhiteboards M.! page)
                           (\ds -> 
                            let es = map fromJust $ filter isJust $ map (\e -> M.lookup e ds) eids
                            in
                            return ( foldr (\eID m -> M.delete eID m) ds eids
                                   , zipWith (\e d -> Drawing (e,d)) eids es
                                   )
                           )
    modifyMVar_ (whiteboards M.! page)
                (\ds -> return $ foldr (\(Drawing (eID, d)) m -> M.insert eID d m) ds elements)

switchPage page = do
  cx <- get
  liftIO $ do
    modifyMVar_ (cxPageM cx) (\p -> return page)
    modifyMVar_ (cxWhiteboardsM cx)
                (\wb ->  
                  if page `M.member` wb then
                    return wb
                  else do
                    whiteboardM <- newMVar M.empty
                    return $ M.insert page whiteboardM wb
                )

refresh = do
  cx <- get
  liftIO $ do
    page <- readMVar $ cxPageM cx
    drawings <- do
      wbM <- fmap (M.! page) $ readMVar $ cxWhiteboardsM cx
      wb <- readMVar wbM
      return $ M.elems wb
    let svg = drawingsToSVG drawings
    modifyMVar_ (cxOutputThread cx)
      (\mid -> do
        case mid of
          Nothing -> return ()
          Just tid -> killThread tid
        writeFile "whiteboard.svg" svg
        fmap Just $ forkIO $ pictikz "whiteboard.svg" "whiteboard.tex" "whiteboard.pdf"
      )

pictikz svg tex pdf = do
  confExists <- doesFileExist "pictikz.conf"
  (_,Just sout, Just serr, ph) <- 
    createProcess (proc "pictikz" $
                        (if confExists then ["--config", "pictikz.conf"] else []) ++
                        ["--standalone", svg]
                  ){std_out = CreatePipe, std_err = CreatePipe}
  forkIO $ do
    errStr <- hGetContents serr
    putStrLn errStr
  texStr <- hGetContents sout
  writeFile tex texStr
  latex tex pdf

latex tex pdf = do
  (_,Just sout, Just serr, ph) <- 
    createProcess (proc "latexmk" $
                        ["-pdf", tex]
                  ){std_out = CreatePipe, std_err = CreatePipe}
  forkIO $ do
    errStr <- hGetContents serr
    putStrLn errStr
  outStr <- hGetContents sout
  putStrLn outStr

runWhiteboard :: (Monad m) => Config -> (StateT Context m a) -> m a
runWhiteboard conf f = evalStateT f (cfContext conf)

connect conf
  | isNothing $ cfURL conf = putStrLn "Please provide the URL of a Zauboard whiteboard."
  | otherwise = do
    let Just url = cfURL conf
        -- (port, domain, path, queryVars) = parseURL url
        uri = fromJust $ N.parseURI url
        query = queryMap $ N.query uri
        conf' = conf{ cfBoardKey = M.lookup "board_key" query
                    , cfPage = (M.lookup "page" query) >>= readMaybe
                    }
        host = fromMaybe "" $ fmap N.uriRegName $ N.uriAuthority uri
        protocol = N.uriScheme uri
        port = fromMaybe (if protocol == "https" then 443 else 80) $ do
                  mport <- fmap N.uriPort $ N.uriAuthority uri
                  readMaybe mport
        path = N.uriPath uri
    -- print (port, domain, path, queryVars)
    withSocketsDo $ 
      if N.uriScheme uri == "https" then
        WSS.runSecureClient host (fromIntegral port) path (client conf')
      else
        WS.runClient host (fromIntegral port) path (client conf')

queryMap str = M.fromList $ evalState queryVars str

--parseURL :: String -> (Int, String, String, [(String, String)])
--parseURL = evalState (liftM4 (,,,) protocol domain path queryVars)

--protocol = do
--  str <- get
--  let (prot, rs) = span (/=':') str
--      domPort = readMaybe $ takeWhile (/='/') $ dropWhile (/=':') rs
--  put $ dropWhile (=='/') $ tail rs
--  return $ case domPort of
--    Just p -> p
--    Nothing -> case prot of
--                 "https" -> 443
--                 _ -> 80
--domain = do
--  str <- get
--  let (dm, rs) = span (/='/') str
--  put rs
--  return $ takeWhile (/=':') dm
--
--path = do
--  str <- get
--  let (pth, rs) = span (/='?') str
--  put $ drop 1 rs
--  return pth

queryVars = do
  str <- get
  let (var, rs) = span (/='=') str
  if null var then
    return []
  else do
    let (val, rs') = span (/='&') rs
    put $ drop 1 rs'
    vars <- queryVars
    return $ (N.unEscapeString var, N.unEscapeString $ drop 1val) : vars

  
