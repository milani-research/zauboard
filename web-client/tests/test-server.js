var startArgs = getArgs();
var PORT = 8081; //Port for the app with                                         (--port=PORT)

var express = require('express');

// Command line arguments
if (startArgs["port"])
	PORT = startArgs["port"];

var app = express();
app.use(express.static(__dirname));
var serverApp = require('http').Server(app);
serverApp.listen(PORT);

function getArgs() {
	const args = {}
	process.argv
		.slice(2, process.argv.length)
		.forEach(arg => {
			// long arg
			if (arg.slice(0, 2) === '--') {
				const longArg = arg.split('=')
				args[longArg[0].slice(2, longArg[0].length)] = longArg[1]
			}
			// flags
			else if (arg[0] === '-') {
				const flags = arg.slice(1, arg.length).split('')
				flags.forEach(flag => {
					args[flag] = true
				})
			}
		})
	return args
}

process.on('unhandledRejection', error => {
	// Will print "unhandledRejection err is not defined"
	console.log('unhandledRejection', error.message);
})

console.log("Test server running on port:" + PORT);
console.log("http://localhost:8081");
