#!/usr/bin/env fish
if test ! -e node_modules
	ln -s ../node_modules
end
if test ! -e public
	ln -s ../public
end

node test-server.js
