import {EventHandler, ZoomAndPanHandler, PointerInteraction, PathTool, ToolType, Cursor} from "../public/js/interaction.js";
import {Viewport} from "../public/js/viewport.js";
import {SVGPainter} from "../public/js/svgPainter.js";
import {CanvasPainter} from "../public/js/canvasPainter.js";
import {ActionType, DrawingElement, makePathElement} from "../public/js/drawingElement.js";

class DummyDebug
{
	logValues(){}
}

window.debug = new DummyDebug();

export class DummyUser
{
	constructor()
	{
	}

	updateDrawId()
	{
	}

	setPosition()
	{
	}

	eraseAroundLine()
	{
	}

	hideDrawings()
	{
	}

	insertDrawing(drawing_el)
	{
		console.log(drawing_el);
	}

  moveDrawings()
  {
  }

	duplicateDrawings()
	{
	}

	clear()
	{
	}

	undo()
	{
	}
}

class DummyWhiteboard
{
	constructor()
	{
	}
	pointerMove()
	{
	}
	pointerUp()
	{
	}
	pointerDown()
	{}
	setPointerInteraction(pointer_interaction)
	{
		this.pointer_interaction = pointer_interaction;
	}
}

class DummyZoomAndPan
{
	constructor()
	{
	}
}

class DummyCommunication
{
	construct()
	{
	}
	cursorUpdate(){}
}

var sample_container = document.getElementById('comparison-area');
var svgns = "http://www.w3.org/2000/svg";
var radius_factor = 5;
var thickness_factor = 4;
var raw_color  = "#ff0000";
var poly_color = "#00ff00";
var new_color  = "#afaf00";
var drawing_container = document.getElementById('draw-area');
var canvas_container = document.getElementById('draw-canvas');
var canvas_container_element = document.getElementById('draw-canvas-element');

var drawing_width = drawing_container.clientWidth;
var drawing_height = drawing_container.clientHeight;

var viewport = new Viewport(drawing_width / 2, drawing_height / 2, drawing_width, drawing_height, 1);
var screen_painter = new CanvasPainter(1, {canvas_container : 'sample-drawing-canvas-container', canvas_element : 'sample-drawing-canvas'});
var cursor_container = document.createElement('div');
cursor_container.style = "height: 100%; width: 100%;";
var cursor = new Cursor(1, "#000", cursor_container);
screen_painter.start(drawing_container, viewport);
var svg_painter = new SVGPainter(); // TODO: Setup SVGPainter for samples
var dummy_user = new DummyUser();

// Setup sample drawing area
var overlay_container = document.getElementById('overlay-container');
var svg_container = document.getElementById('draw-id');
var pointers = [];
var zoom_and_pan = new DummyZoomAndPan();
var whiteboard = new DummyWhiteboard();
var cursor = new Cursor(1.0, '#0a0', cursor_container);
var pointer_interaction = new PointerInteraction(
	ToolType.pen,
	pointers,
	zoom_and_pan,
	viewport,
	screen_painter,
	svg_container,
	overlay_container,
	cursor,
	dummy_user,
	whiteboard);
whiteboard.setPointerInteraction(pointer_interaction);
var dummy_communication = new DummyCommunication();
var eventHandler = new EventHandler(whiteboard, viewport, pointer_interaction, zoom_and_pan, dummy_communication);
eventHandler.setupListeners(drawing_container);
pointer_interaction.setTool(ToolType.pen);
pointer_interaction.setColor('#0f0');

var sample_n = 1;
function addSample(sample_container, points)
{
	var sample_div = document.createElement('div');
	sample_div.setAttribute('class', 'sample-container');
	var raw_points_div = document.createElement('div');
	raw_points_div.setAttribute('class', 'sample');
	//raw_points_div.setAttribute('style', "border-bottom:1px solid #000;");
	var raw_points_header = document.createElement('h3');
	raw_points_header.setAttribute('class', 'sample');
	raw_points_header.innerHTML = 'Sample ' + sample_n;
	sample_n = sample_n + 1;
	raw_points_div.append(raw_points_header);
	var raw_points_svg = document.createElementNS(svgns, 'svg');
	raw_points_div.append(raw_points_svg);
	sample_div.append(raw_points_div);
	sample_container.append(sample_div);
	addRawSample(raw_points_svg, points);

	var drawn_points_div = document.createElement('div');
	var drawn_points_svg = document.createElementNS(svgns, 'svg');
	var bg_div = document.createElement('div');
	var overlay_div = document.createElement('div');
	sample_div.append(drawn_points_div);
	var svgPainter = new SVGPainter(
		{ background_div : 
			{ 
			}
		, overlay_div : 
			{
			}
		, svg_element :
			{
			}
		, svg_div : 
			{
				'class' : 'sample-drawn'
			}
		});
	drawn_points_div.setAttribute('overflow', 'clip');
	var h = raw_points_svg.getAttribute('height');
	var w = raw_points_svg.getAttribute('width');
	drawn_points_div.setAttribute('class', 'sample');
	drawn_points_div.setAttribute('style', 'width:' + w + 'px;max-height:' + h + 'px;overflow:clip;');
	//drawn_points_div.setAttribute('max-height', raw_points_svg.getAttribute('height'));
	//drawn_points_div.setAttribute('width', raw_points_svg.getAttribute('width'));
	//drawn_points_svg.setAttribute('viewBox', raw_points_svg.getAttribute('viewBox'));
	// var viewport = new Viewport(drawn_points_div.clientWidth / 2, drawn_points_div.clientHeight / 2, drawn_points_div.clientWidth, drawn_points_div.clientHeight, 1);
	var viewport = new Viewport( drawn_points_div.clientWidth / 2, drawn_points_div.clientHeight / 2, drawn_points_div.clientWidth, drawn_points_div.clientHeight, 1);
	svgPainter.start(raw_points_div, viewport);
	var thickness = [];
	var points_1 = [];
	points_1.push([points[0].x, points[0].y]);
	for(var i = 0 ; i < points.length ; i++)
	{
		thickness.push(points[i].pressure * thickness_factor);
		points_1.push([points[i].x, points[i].y]);
	}
	points_1.push([points[points.length - 1].x, points[points.length - 1].y]);
	var draw_el = makePathElement(ActionType.curve, points_1, new_color, thickness);
	svgPainter.draw(draw_el);

}

function addRawSample(raw_points_svg, points)
{
	var x0 = points[0].x;
	var x1 = points[0].x;
	var y0 = points[0].y;
	var y1 = points[0].y;
	for(var i = 0 ; i < points.length ; ++i)
	{
		var radius = points[i].pressure * radius_factor;
		var cx = points[i].x;
		var cy = points[i].y;
		var raw_point_circle = document.createElementNS(svgns, 'circle');
		raw_point_circle.setAttribute('stroke', raw_color);
		raw_point_circle.setAttribute('stroke-width', 0.0);
		raw_point_circle.setAttribute('cx', cx);
		raw_point_circle.setAttribute('cy', cy);
		raw_point_circle.setAttribute('r', radius);
		raw_point_circle.setAttribute('fill', raw_color);
		if(cx < x0)
		{
			x0 = cx;
		}
		if(cx > x1)
		{
			x1 = cx;
		}
		if(cy < y0)
		{
			y0 = cy;
		}
		if(cy > y1)
		{
			y1 = cy;
		}
		if(i < points.length - 1)
		{
			var x1 = points[i+1].x;
			var y1 = points[i+1].y;
			var raw_line = document.createElementNS(svgns, 'line');
			raw_line.setAttribute('stroke', poly_color);
			raw_line.setAttribute('fill', "none");
			raw_line.setAttribute('stroke-linecap', 'round');
			raw_line.setAttribute('x1', cx);
			raw_line.setAttribute('y1', cy);
			raw_line.setAttribute('x2', x1);
			raw_line.setAttribute('y2', y1);
			raw_line.setAttribute('stroke-width', radius);
			raw_points_svg.append(raw_line);
		}
		raw_points_svg.append(raw_point_circle);
		raw_points_svg.setAttribute('view_box', (x0 - radius_factor) + ' ' + (y0 - radius_factor) + ' ' + (x1 - x0 + radius_factor) + ' ' + (y1 - y0 + radius_factor));
		raw_points_svg.setAttribute('width', x1 - x0 + 2 * radius_factor);
		raw_points_svg.setAttribute('height', y1 - y0 + 2 * radius_factor);
	}
}



addSample(sample_container, [{x : 0, y : 0, pressure : 0.5}, {x : 10, y : 10, pressure : 0.75}, {x : 30, y : 5, pressure : 0.5}]);


