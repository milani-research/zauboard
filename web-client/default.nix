{ stdenv, lib, makeWrapper
}:

stdenv.mkDerivation
{
	pname = "zauboard-web-client";
	version = "2.3";
	src = ./.;

	buildInputs = [ makeWrapper ];

	installPhase = ''
		mkdir -p $out/share/zauboard/public
		cp -r public/index.html public/js public/css public/images public/favicon.ico $out/share/zauboard/public/
	'';
}

