/* -----------
 KEYBINDINGS
----------- */ 

//> defmod is "command" on OS X and "ctrl" elsewhere
//Advanced Example: 'defmod-k j' -> For this to fire you have to first press both ctrl and k, and then j. 

var keybinds = {
    // 'key(s)' : 'function',
    'defmod-z' : 'undoStep', 
    'x' : 'setTool_recSelect',
    'm' : 'setTool_mouse',
    'p' : 'setTool_pen',
    'l' : 'setTool_line',
    'r' : 'setTool_rect',
    'c' : 'setTool_circle',
    'shift-f' : 'toggleLineRecCircle',
    'shift-x' : 'togglePenEraser',
    'shift-r' : 'toggleMainColors',
    // 'a' : 'setTool_text',
    'e' : 'setTool_eraser',
    ']' : 'thickness_bigger',
    '[' : 'thickness_smaller',
    'shift-c' : 'openColorPicker',
    'shift-1' : 'setDrawColorBlack',
    'shift-2' : 'setDrawColorBlue',
    'shift-3' : 'setDrawColorGreen',
    'shift-4' : 'setDrawColorYellow',
    'shift-5' : 'setDrawColorRed',
    'shift-k' : 'saveWhiteboardAsJson',
    'shift-i' : 'uploadWhiteboardToWebDav',
    'shift-j' : 'uploadJsonToWhiteboard',
    'shift-s' : 'shareWhiteboard',
    'tab' : 'hideShowControls',
//    'up' : 'moveDraggableUp',
//    'down' : 'moveDraggableDown',
//    'left' : 'moveDraggableLeft',
//    'right' : 'moveDraggableRight',
    'defmod-enter' : 'dropDraggable',
    'shift-enter' : 'addToBackground',
    'escape' : 'cancelAllActions',
    'del' : 'deleteSelection'
}
