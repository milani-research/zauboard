import {getNewUrl, escapeHtml, checkPageRanges} from "./utils.js";

export class WhiteboardMenu
{
  constructor(container, base_url)
  {
		this.base_url = base_url;
		this.table_header = 
        '<div class="whiteboard-list" style="width:95%;max-height:70%;overflow:auto;">'
      + '<table class="whiteboard-list">'
		  + '<col style="width:45%">'
		  + '<col style="width:15%">'
		  + '<col style="width:3cm">'
		  + '<col style="width:2cm">'
			+ '<thead><tr>'
      + '<th class="whiteboard-list">Whiteboard</th>'
      + '<th class="whiteboard-list">Owner</th>'
      + '<th class="whiteboard-list">Last modification</th>'
      + '<th class="whiteboard-list"> </th>'
      + '</tr></thead><tbody>';
		this.table_footer = "</tbody></table></div>";
    this.html_header =
        "<div class=\"dimmed\">"
      + "<div class=\"userInfoDialog\" style=\"height:90%;width:90%;overflow:auto;\">"
      + "<form id=\"userInfoForm\">"
      + "<input id=\"whiteboardNameField\" class=\"welcome\" style=\"margin-left:0.1cm;padding-top:8px;padding-bottom:6px;margin-top:16px;margin-bottom:0;\" type=\"text\" name=\"whiteboardname\" maxlength=\"100\" placeholder=\"Whiteboard name\">"
      + "<button type=\"button\" class=\"inline\" onclick=\"createWhiteboardButton()\">+ New</button>"
      + '<button type="button" class="inline" style="right:1cm;top:0cm;position:absolute;" onclick="closeWhiteboardList();">Close</button></form><br>'
      + "<h2 class=\"dialogTitle\">Whiteboards</h2>"
		this.html_footer = "</div></div>";
		this.public_header = "<h2 class=\"dialogTitle\">Public whiteboards</h2>"
		this.archive_header = "<h2 class=\"dialogTitle\">Archived whiteboards</h2>"
		this.html = this.html_header + this.table_header + this.table_footer + this.html_footer;

    this.container = container;
  }

  popup()
  {
    this.container.innerHTML = this.html;

    this.name_field = document.getElementById("whiteboardNameField");
    this.warning    = document.getElementById("dialogWarning");
    this.name_field.innerHTML = "Whiteboard name";
		var _this = this;
    this.name_field.addEventListener('keyup', function (e) {
      _this.name_field.innerHTML=escapeHtml(_this.name_field.value.slice(0,100));
    });
    document.addEventListener('keydown', function (e) {
      // enter was pressed
      if(e.keyCode === 13)
      {
        e.preventDefault();
        createWhiteboardButton();
      }
    });
  }

  popout()
  {
    this.container.innerHTML = "";
  }

	createWhiteboard()
	{
		return this.name_field.value.slice(0,100);
	}

	onWhiteboards(whiteboards)
	{
		var sorted_whiteboards = whiteboards.sort(function(a,b){return (a.last_modified > b.last_modified) ? -1 : (b.last_modified > a.last_modified ? 1 : 0)});
		var user_table_content = "";
		var public_table_content = "";
		var archive_table_content = "";
		var user_rows = [];
		var public_rows = [];
		var archive_rows = [];
		var classes = ["whiteboard-list-even", "whiteboard-list-odd"];
		for(let j in sorted_whiteboards)
		{
			var wb = sorted_whiteboards[j];
			var url = getNewUrl(this.base_url, wb.key, wb.pages);
			var date = new Date(Number(wb.last_modified)*1000);
			var date_str = "" + date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2,"0") + "-" + date.getDate().toString().padStart(2,"0") + " " + date.getHours().toString().padStart(2,"0") + ":" + date.getMinutes().toString().padStart(2,"0");
			if(wb.is_public === true)
			{
				var cls = classes[public_rows.length % 2];
				public_rows.push('<tr class="' + cls + '"><td class="' + cls + '"><a class="whiteboard-list" href="' + url + '">' + escapeHtml(wb.name) + '</a></td>'
						+ '<td class="' + cls + '">' + wb.owner + "</td>"
						+ '<td class="' + cls + '" style="font-family:monospace;">' + date_str + '</td>'
					  + '<td class="' + cls + '"></td></tr>'
				);
			}
			else if(wb.is_archived === false)
			{
				var cls = classes[user_rows.length % 2];
				user_rows.push('<tr class="' + cls + '"><td class="' + cls + '"><a class="whiteboard-list" href="' + url + '">' + escapeHtml(wb.name) + '</a></td>'
						+ '<td class="' + cls + '">' + wb.owner + "</td>"
						+ '<td class="' + cls + '" style="font-family:monospace;">' + date_str + "</td>"
					  + '<td class="' + cls + '"><button type="button" class="whiteboard-list" onclick="removeWhiteboardButton(\'' + wb.key + '\')">Remove</button></td></tr>'
				);
			}
			else
			{
				var cls = classes[archive_rows.length % 2];
				archive_rows.push('<tr class="' + cls + '"><td class="' + cls + '"><a class="whiteboard-list" href="' + url + '">' + escapeHtml(wb.name) + '</a></td>'
						+ '<td class="' + cls + '">' + wb.owner + "</td>"
						+ '<td class="' + cls + '" style="font-family:monospace;">' + date_str + "</td>"
					  + '<td class="' + cls + '"><button type="button" class="whiteboard-list" onclick="restoreWhiteboardButton(\'' + wb.key + '\')">Restore</button></td></tr>'
				);
			}
		}
		user_table_content = user_rows.join("");
		public_table_content = public_rows.join("");
		archive_table_content = archive_rows.join("");
		this.html = this.html_header + this.table_header + user_table_content + this.table_footer +
			          (public_rows.length === 0 ? "" : (this.public_header + this.table_header + public_table_content + this.table_footer)) +
			          (archive_rows.length === 0 ? "" :(this.archive_header + this.table_header + archive_table_content + this.table_footer)) +
			          this.html_footer;
	}

}

export class LoginMenu
{
  constructor(container)
  {
    this.html =
        "<div class=\"dimmed\">"
      + "<div class=\"userInfoDialog\" style=\"height:320px;\">"
      + "<h2 class=\"dialogTitle\">Sign in</h2>"
      + "<form id=\"userInfoForm\">"
      + "<input id=\"loginNameField\" class=\"welcome\" type=\"text\" name=\"username\" maxlength=\"32\" placeholder=\"Username\">"
      + "<input id=\"loginPasswordField\" class=\"welcome\" type=\"password\" name=\"password\" placeholder=\"Password\">"
      + "<br>"
      + '<p id="dialogWarning" class="dialogWarning"></p>'
      + "<button type=\"button\" class=\"mymenu\" style=\"right:39%;\" onclick=\"loginMenuButton('login')\">Sign in</button>"
		  + "<br><br><br>"
      + "<button type=\"button\" class=\"mymenu\" style=\"right:29%;\" onclick=\"loginMenuButton('guest login')\">Sign in as guest</button>"
      + "</form>"
      + "</div>"
      + "</div>";
    this.container = container;
  }

  popup()
  {
    this.container.innerHTML = this.html;

    this.name_field = document.getElementById("loginNameField");
    this.password   = document.getElementById("loginPasswordField");
    this.warning    = document.getElementById("dialogWarning");
    this.name_field.innerHTML = "Username";
		var _this = this;
    this.name_field.addEventListener('keyup', function (e) {
      _this.name_field.innerHTML=escapeHtml(_this.name_field.value.slice(0,32));
    });
    document.addEventListener('keydown', function (e) {
      // enter was pressed
      if(e.keyCode === 13)
      {
        e.preventDefault();
        _this.onLogin(_this.name_field.value.slice(0,32), _this.password.value);
      }
    });
  }

  popout()
  {
    this.container.innerHTML = "";
  }

  buttonPressed(buttonName)
  {
    switch(buttonName)
    {
      case "guest login":
        this.onGuestLogin();
        break;
      case "login":
        this.onLogin(this.name_field.value.slice(0,32), this.password.value);
        break;
    }
  }

  onAccessDenied()
  {
    this.warning.innerHTML = "Username or password is incorrect."
  }
}

export class ImportPDFMenu
{
	constructor(container)
	{
		this.html = '<div class="dimmed">'
      + '<div class="userInfoDialog" style="height:400px;width:90%;">'
      + '<h2 class="dialogTitle">Import PDF</h2>'
		  + '<p class="menu" id="numberOfPages">Uploading file...</p>'
      + '<form id="PDFImportForm">'
      + '<span class="menu">Pages to import:</span><input id="pagesToImportField" class="menu" style="margin-left:1cm;padding-top:8px;padding-bottom:6px;margin-top:16px;margin-bottom:0;" type="text" name="pagestoimport" maxlength="100" value="1-" placeholder="1-">'
		  + '<span id="pagesToImportError" class="menu-error"></span><br>'
      + '<span class="menu">Scale:</span><input id="scaleField" class="menu" style="margin-left:3.5cm;margin-right:0cm;width:2cm;padding-top:8px;padding-bottom:6px;margin-top:16px;margin-bottom:0;" type="number" name="pagestoimport" maxlength="100" value="200" placeholder="200"><span class="menu" style="margin-left:0cm;">%</span><br>'
      + '<span class="menu">To whiteboard page:</span><input id="targetPageField" class="menu" style="width:2cm;margin-left:0.1cm;padding-top:8px;padding-bottom:6px;margin-top:16px;margin-bottom:0;" type="number" name="targetpage" maxlength="100" value="1" placeholder="1"><br><br>'
		  // + '<input style="margin-left:5.5cm;" class="menu" type="checkbox" id="singlePageBox" name="singlePageBox" value="Use only one whiteboard page.">'
		  // + '<label for="singlePageBox" class="menu">Use only one whiteboard page.</label>'
      + '<button id="importButton" type="button" class="menu-disabled" style="left:5cm;bottom:5%;" onclick="">+ Import</button></form><br>'
      + '<button type="button" class="menu" style="left:8cm;bottom:5%;" onclick="cancelImportPDFButton()">Cancel</button></form><br></div></div>';
		this.container = container;
	}

	popup(current_page)
	{
		var _this = this;
    this.container.innerHTML = this.html;
		this.number_of_pages_element = document.getElementById("numberOfPages");
		this.selected_pages_element = document.getElementById("pagesToImportField");
		this.selected_pages_error_element = document.getElementById("pagesToImportError");
    this.selected_pages_element.addEventListener('keyup', function (e) {
      var r = 0, w = 0;
			var result = [];
			for(; r < _this.selected_pages_element.value.length ; r++)
			{
				var c = _this.selected_pages_element.value[r]
				if((c >= '0' && c <= '9') || c === '-' || c === ',' || c === ' ')
				{
					result.push(c);
				}
			}
			_this.selected_pages_element.value = result.join('');
    });
    this.selected_pages_element.addEventListener('keydown', function (e) {
			if(e.key.length === 1 && ((e.key >= 'a' && e.key <= 'z') || (e.key >= 'A' && e.key <= 'Z')))
				e.preventDefault();
		});
		this.single_page_element = document.getElementById("singlePageBox");
		this.scale_element = document.getElementById("scaleField");
		this.target_page_element = document.getElementById("targetPageField");
		this.target_page_element.setAttribute("placeholder", current_page);
		this.target_page_element.setAttribute("value", current_page);
		this.import_button_element = document.getElementById("importButton");
		this.pdf_ready = false;
	}

	popout()
	{
		this.container.innerHTML = "";
	}

	onPDFSummary(pdf_info)
	{
		this.number_of_pages_element.innerHTML = "Number of pages on PDF: " + pdf_info.numberOfPages;
		this.import_button_element.setAttribute("onclick", "importPDFButton()");
		this.import_button_element.setAttribute("class", "menu");
		this.pdf_ready = true;
		this.pdfID = pdf_info.pdfID;
		this.number_of_pages = pdf_info.numberOfPages;
	}

	getImportOptions()
	{
		var options = {};
		options.pdf_ready = this.pdf_ready;
		options.to_whiteboard_page = Number(this.target_page_element.value);
		options.pdf_pages = this.selected_pages_element.value;
		var check = checkPageRanges(options.pdf_pages, this.number_of_pages );
		if(check.ok === false)
		{
			this.selected_pages_error_element.innerHTML = check.message;
			options.pdf_ready = false;
			return options;
		}
		// TODO: Figure out how to make single page to work properly
		//options.single_page = this.single_page_element.checked;
		options.single_page = false;
		options.scale = Number(this.scale_element.value) / 100;
		options.pdfID = this.pdfID;
		return options;
	}

	buttonPressed(buttonName)
	{
	}
}

export class SettingsMenu
{
	constructor(container)
	{
		var _this = this;
		this.container = container;

		this.dim = createElement("div", {"class":"dimmed"});
		this.dialog = createElement("div", {"class":"userInfoDialog", "style":"overflow:auto;"});
		this.dim.append(this.dialog);
		this.dialog.append(createElement("h2", {"class":"dialogTitle"}, "Settings"));

		this.setupCursorSettings();
		this.setupPasswordSettings();

		this.ok_button = createElement("button", {"class" : "welcome", "type":"button", "onclick" : "updateSettings();"}, "Ok")
		this.dialog.append(this.ok_button);
		
	}

	setupCursorSettings()
	{
		var color_div = createElement("div", {"class":"settingsColorList"});
		const colors =
			[ {bg : "#000"   , fg : "#fff"}
			, {bg : "#ffc9c9", fg : "#d20000"}
			, {bg : "#bff7bb", fg : "#0e540c"}
			, {bg : "#d3d1fb", fg : "#0c0c74"}
			// second line
			, {bg : "#fff"   , fg : "#000"}
			, {bg : "#b8f9f2", fg : "#084f50"}
			, {bg : "#f6b8f9", fg : "#50084b"}
			, {bg : "#f7f9b8", fg : "#3f3e07"}
			];
		for(const color of colors)
		{
			color_div.append(createElement("button", 
				{"class":"welcomeColor"
				, "type":"button"
				, "style":"background-color:" + color.bg + "; color:" + color.fg + ";"
				, "onclick":"setUserBadgeColor('" + color.bg + "','" + color.fg + "');"
				}));
		}
		this.colors = colors;
		this.dialog.append(createElement("h3", {"class":"dialogSection"}, "Cursor"));
		this.default_name = this.randomName();
		this.user_name_element = createElement("input", {"id":"settingsUsernameField", "class":"welcome", "type":"text", "name":"username", "maxlength":"16", "placeholder":"Name"});
		this.user_name_element.addEventListener('keyup', function (e) {
			_this.cursor_tag_preview_element.innerHTML = escapeHtml(_this.user_name_element.value.slice(0,16));
		});
		this.user_name_element.addEventListener('keydown', function (e) {
			// enter was pressed
			if(e.keyCode === 13)
			{
				e.preventDefault();
				receiveUserInfo();
			}
		});

		this.default_color = Math.floor(Math.random() * colors.length);
		this.cursor_preview = createElement("div", {"class":"cursorPreview", "id":"cursorPreview", "style":"background:#000;position:absolute;top:105px;left:160px;"});
		this.cursor_tag_preview_element = createElement("div", {"class":"userbadgePreview", "id":"cursorTagPreview", "style":"background-color:" + colors[this.default_color].bg + ";color:" + colors[this.default_color].fg + ";"}, "Name");
		this.cursor_preview.append(this.cursor_tag_preview_element);

		var table_def = { attributes : {},
			  header : 
				{ attributes : {},
					columns : [ { attributes : { class : "settingsName"}, content : ""},
											{ attributes : {}, content : ""}]
				}};
		var rows_def = 
			[	
				{ attributes : {},
				  cells : 
						[ { attributes : { class : "settingsName"}, content : createElement("span", { class : "settings" }, "Name:")},
						  { attributes : {}, content : this.user_name_element}
						]
				},
				{ attributes : {},
				  cells : 
						[ { attributes : {class : "settingsName"}, content : createElement("span", { class : "settings" }, "Color:")},
						  { attributes : {}, content : color_div}
						]
				},
				{ attributes : {},
					cells : [{attributes : {class : "settingsName"}, content : createElement("span", { class : "settings"} , "Preview:")},
			             {attributes : {}, content : this.cursor_tag_preview_element}]
				}
		];
		var cursor_table = createTable(table_def, rows_def);
		this.dialog.appendChild(cursor_table);
	}

	setupPasswordSettings()
	{
		var _this = this;
    this.old_password_input = createElement("input", {"id":"settingsOldPassword", "class":"password", "type":"password", "name":"oldpassword", "maxlength":"100", "placeholder":"Current password"});
    this.new_password_input = createElement("input", {"id":"settingsNewPassword", "class":"password", "type":"password", "name":"newpassword", "maxlength":"100", "placeholder":"New password"});
    this.new_password_repeat_input = createElement("input", {"id":"settingsNewPasswordRepeat", "class":"password", "type":"password", "name":"newpasswordrepeat", "maxlength":"100", "placeholder":"Repeat new password"});

		this.new_password_input.addEventListener('keyup', function(e){_this.checkIfPasswordsMatch();} );
		this.new_password_repeat_input.addEventListener('keyup', function(e){_this.checkIfPasswordsMatch();} );

		this.password_message = createElement("span", { class : "dialogWarning" });
		this.password_section = createElement("h3", { class : "dialogSection" }, "Password");
		this.password_table = createTable(
			{ 
				attributes : {},
				header : 
				{ 
					attributes : {},
					columns : 
					[ { attributes : { class : "settingsName" }, content : ""},
						{ attributes : {}, content : ""}
					]
				}
			},
			[ { 
					attributes : {},
					cells :
						[
							{
								attributes : {class : "settingsName"},
								content : createElement("span", { class : "settings"}, "Current password:")
							},
							{
								attributes : {},
								content : this.old_password_input
							}
						]
				},
				{ 
					attributes : {},
					cells :
						[
							{
								attributes : {class : "settingsName"},
								content : createElement("span", { class : "settings"}, "New password:")
							},
							{
								attributes : {},
								content : this.new_password_input
							}
						]
				},
				{ 
					attributes : {},
					cells :
						[
							{
								attributes : {class : "settingsName"},
								content : createElement("span", { class : "settings"}, "Repeat password:")
							},
							{
								attributes : {},
								content : this.new_password_repeat_input
							}
						]
				},
				{ 
					attributes : {},
					cells :
						[
							{
								attributes : {class : "settingsName"},
								content : undefined
							},
							{
								attributes : {},
								content : this.password_message
							}
						]
				}
			]
		);
	}

	checkIfPasswordsMatch()
	{
		var repeat_pass = this.new_password_repeat_input.value;
		var new_pass = this.new_password_input.value;
		if(repeat_pass !== '' && new_pass !== '')
		{
			if(repeat_pass !== new_pass)
			{
				this.password_message.innerHTML = "Passwords do not match.";
			}
			else
			{
				this.password_message.innerHTML = "";
			}
		}
		else
		{
			this.password_message.innerHTML = "";
		}
	}

	hidePasswordSettings()
	{
		this.new_password_input.value = '';
		this.password_section.remove();
		this.password_table.remove();
	}

	showPasswordSettings()
	{
		this.password_message.innerHTML = "";
		this.dialog.appendChild(this.password_section);
		this.dialog.appendChild(this.password_table);
	}

	incorrectPassword()
	{
		this.password_message.innerHTML = "Incorrect password.";
	}

	getNewPassword()
	{
		var new_pass = this.new_password_input.value;
		var repeat_pass = this.new_password_repeat_input.value;
		var current_pass = this.old_password_input.value;
		if(new_pass !== '' && repeat_pass !== '' && current_pass !== '' && new_pass === repeat_pass)
		{
			return({current : current_pass, new_pass : new_pass});
		}
		else
		{
			return(undefined);
		}
	}

	clearPasswords()
	{
		this.new_password_input.value = '';
		this.new_password_repeat_input.value = '';
		this.old_password_input.value = '';
	}

	popup(user_tag, full_settings)
	{
		this.container.innerHTML = this.html;
		this.container.append(this.dim);
		if(typeof user_tag.badge_bg === 'undefined')
		{
			setUserBadgeColor(this.colors[this.default_color].bg, this.colors[this.default_color].fg)
		}
		
		this.cursor_tag_preview_element.setAttribute("style", "background-color:" + user_tag.badge_bg + ";color:" + user_tag.badge_fg + ";border:1px solid " + user_tag.badge_fg + ";");
		
		if(typeof user_tag.user_name !== 'undefined' && user_tag.user_name !== "")
		{
			this.user_name_element.value = user_tag.user_name;
			this.cursor_tag_preview_element.innerHTML = escapeHtml(user_tag.user_name);
		}
		else
		{
			this.cursor_tag_preview_element.innerHTML = escapeHtml(this.default_name);
		}
		if(full_settings === true)
		{
			this.showPasswordSettings();
		}
		else
		{
			this.hidePasswordSettings();
		}
	}

	popout()
	{
		this.container.innerHTML = '';
		this.clearPasswords();
	}

	setTagColor(bg,fg)
	{
		this.cursor_tag_preview_element.setAttribute('style',"color:" + fg + ';background-color:' + bg + ';border:1px solid ' + fg + ';');
	}

	getSettings(settings)
	{
		if(this.user_name_element.value === "")
		{
			settings.user_name = this.default_name;
		}
		else
		{
			settings.user_name = this.user_name_element.value;
		}
		return(settings);
	}

	randomName()
	{
		var adjectives = 
			[ "Wise"
			, "Bright"
			, "Calm"
			, "Fast"
			, "Gentle"
			, "Happy"
			, "Magic"
			, "Nice"
			, "Quick"
			, "Quiet"
			];
		var nouns =
			[ "River"
			, "Cloud"
			, "Fish"
			, "Fox"
			, "Flower"
			, "Lizard"
			, "Monk"
			, "Moon"
			, "Owl"
			, "Pen"
			, "Rainbow"
			, "Rock"
			, "Star"
			, "Sun"
			, "Tiger"
			, "Wind"
			, "Wizard"
			];
		var name = adjectives[Math.floor(Math.random() * adjectives.length)]
			         + " "
			         + nouns[Math.floor(Math.random() * nouns.length)];
		return(name);
	}
}

export class ExportPDFMenu
{
	constructor(container)
	{
		var _this = this;
		this.dim = createElement("div", {"class":"dimmed"});
		this.dialog = createElement("div", {"class":"userInfoDialog", "style":"height:200px;width:500px;"});
		/*this.user_name_element = document.getElementById("settingsUsernameField");*/
		this.dialog.append(createElement("h2", {"class":"dialogTitle"}, "Export to PDF"));
		this.dialog.append(createElement("span", {"class":"menu"}, "Pages to export:"));
		this.pages_to_export_input = createElement("input", {"class":"menu", "type":"text", "name":"pagestoexport", "maxlength":"100", "value":"1-", placeholder:"1-"});
		this.pages_to_export_input.addEventListener('keyup', function(e) {
      var r = 0, w = 0;
			var result = [];
			for(; r < _this.pages_to_export_input.value.length ; r++)
			{
				var c = _this.pages_to_export_input.value[r]
				if((c >= '0' && c <= '9') || c === '-' || c === ',' || c === ' ')
				{
					result.push(c);
				}
			}
			_this.pages_to_export_input.value = result.join('');

		});
    this.pages_to_export_input.addEventListener('keydown', function (e) {
			if(e.key.length === 1 && ((e.key >= 'a' && e.key <= 'z') || (e.key >= 'A' && e.key <= 'Z')))
				e.preventDefault();
		});
		this.dialog.append(this.pages_to_export_input);
		this.pages_to_export_error = createElement("p", {"class":"menu-error"});
		this.dialog.append(document.createElement("br"));
		this.dialog.append(this.pages_to_export_error);
		this.dialog.append(document.createElement("br"));
		this.download_container = createElement("div", {});
		this.download_button = createElement("button", {"class":"menu-disabled", "type":"button", "style":"left:2.1cm;bottom:5%;", "onclick":""}, "Download");
		this.download_anchor = createElement("a", {"class":"menu"});
		this.download_container.append(this.download_button);
		this.dialog.append(this.download_container);
		this.export_button = createElement("button", {"class":"menu", "type":"button", "style":"left:5.5cm;bottom:5%;", "onclick":"exportPDF();"}, "Export");
		this.dialog.append(this.export_button);
		this.cancel_button = createElement("button", {"class":"menu", "type":"button", "style":"left:8.0cm;bottom:5%;", "onclick":"cancelExportPDF();"}, "Cancel");
		this.dialog.append(this.cancel_button);
		this.dim.append(this.dialog);
		this.container = container;
	}

	popup(number_of_pages)
	{
		var _this = this;
    this.container.innerHTML = '';
		this.download_button.setAttribute("class", "menu-disabled");
		this.download_container.innerHTML = '';
		this.download_container.append(this.download_button);
		this.container.append(this.dim);
		this.pages_to_export_input.value = "1-" + number_of_pages;
		this.number_of_pages = number_of_pages;
	}

	popout()
	{
		this.container.innerHTML = "";
	}

	getExportOptions()
	{
		var options = {};
		options.pages = this.pages_to_export_input.value;
		var check = checkPageRanges(options.pages, this.number_of_pages );
		if(check.ok === false)
		{
			this.pages_to_export_error.innerHTML = check.message;
			options.ready = false;
			return options;
		}
		options.ready = true;
		return options;
	}

	setStatusExporting()
	{
		this.download_button.innerHTML = "Exporting...";

	}

	onPDFReady(href)
	{
		this.download_button.innerHTML = "Download";
		this.download_container.innerHTML = '';
		this.download_anchor.setAttribute("href", href);
		this.download_anchor.setAttribute("onclick", "cancelExportPDF();");
		this.download_anchor.append(this.download_button);
		this.download_button.setAttribute("class", "menu");
		this.download_container.append(this.download_anchor);
	}
}

export class DebugMenu
{
	constructor(container)
	{
		// background
		this.dim = createElement("div", {"class":"dimmed"});
		this.dialog = createElement("div", {"class":"debugDialog"});
		// title
		this.dialog.append(createElement("h2", {"class":"dialogTitle"}, "Debug"));
		this.dialog.append(createElement("span", {"class":"menu"}, "Enable debug:"));
		this.debug_level_checkboxes = createElement("div", {});
		for(let l in window.debug.available_levels)
		{
			var level = window.debug.available_levels[l];
			var box = createElement("input", 
				{ "type"  : "checkbox"
				, "class" : "dialogCheckbox"
				, "onchange" : "toggleDebugLevel(\"" + level + "\");"
				, "name"  : level
				, "value" : level
				})
			if(level in window.debug.debug_level)
				box.setAttribute("checked", "");
			this.debug_level_checkboxes.append(box);
			var label = createElement("label", { "class" : "menu"});
			label.innerHTML = level;
			this.debug_level_checkboxes.append(label);
		}
		// this.debug_levels_checkboxes = createElement("input", {"class":"menu", "type":"text", "name":"pagestoexport", "maxlength":"100", "value":"1-", placeholder:"1-"});
		this.dialog.append(this.debug_level_checkboxes);
		// buttons
		this.action_container = createElement("div", {});
		this.copy_button = createElement("button", {"class":"menu", "type":"button", "style":"left:2.1cm;bottom:5%;", "onclick":"copyDebugLog();"}, "Copy to clipboard");
		this.action_container.append(this.copy_button);
		this.dialog.append(this.action_container);
		this.clear_button = createElement("button", {"class":"menu", "type":"button", "style":"left:7.5cm;bottom:5%;", "onclick":"clearDebugLog();"}, "Clear");
		this.dialog.append(this.clear_button);
		this.close_button = createElement("button", {"class":"menu", "type":"button", "style":"left:9.75cm;bottom:5%;", "onclick":"closeDebug();"}, "Close");
		this.dialog.append(this.close_button);
		this.dim.append(this.dialog);
		// debug logs
		this.debug_logs = createElement("div", {"class" : "debugLog"});
		this.dialog.append(this.debug_logs);
		this.container = container;
	}

	popup()
	{
    this.container.innerHTML = '';
		//this.copy_button.setAttribute("class", "menu-disabled");
		this.action_container.innerHTML = '';
		this.action_container.append(this.copy_button);
		this.debug_logs.innerHTML = '';
		var content = [];
		for(let t in window.debug.logs)
		{
			content.push(window.debug.logs[t].replaceAll("\n","<br>"));
			content.push("\n<br>======\n<br>");
		}
		this.debug_logs.innerHTML = content.join("");
		this.container.append(this.dim);
	}

	clear()
	{
		this.debug_logs.innerHTML = '';
		window.debug.clear();
	}

	popout()
	{
		this.container.innerHTML = "";
	}
}

export class TopBar
{
	constructor(container)
	{
		this.container = container;
		this.active_thickness = 1;
		this.toolbar = document.createElement("div");
		this.toolbar.setAttribute("class", "topbar");
		this.toolbar.setAttribute("style", "position:fixed;");
		this.buttons = document.createElement("div");
		this.buttons.setAttribute("class", "btn-group");
		this.submenus = document.createElement("div");
		this.submenus.setAttribute("class", "submenus");
		this.next_page = document.createElement("button")
		this.next_page.setAttribute("class", "toolbar-button");
		this.next_page.setAttribute("title", "Next page");
		this.next_page.setAttribute("type", "button");
		this.next_page.setAttribute("onclick","nextPage();");
		var img = document.createElement("img");
		img.setAttribute("width", "18");
		img.setAttribute("height", "18");
		img.setAttribute("src", "./images/next-page.svg");
		this.next_page.append(img);

		this.previous_page = document.createElement("button")
		this.previous_page.setAttribute("class", "toolbar-button");
		this.previous_page.setAttribute("title", "Previous page");
		this.previous_page.setAttribute("type", "button");
		this.previous_page.setAttribute("onclick","previousPage();");
		img = document.createElement("img");
		img.setAttribute("width", "18");
		img.setAttribute("height", "18");
		img.setAttribute("src", "./images/previous-page.svg");
		this.previous_page.append(img);

		this.page_number = document.createElement("button");
		this.page_number.setAttribute("class", "pageLocked");
		this.page_number_text = document.createElement("span");
		this.page_number.append(this.page_number_text);
		this.page_number_text.innerHTML = "1";

		this.follow_others = document.createElement("button");
		this.follow_others.setAttribute("class", "toolbar-button");
		this.follow_others.setAttribute("title", "Follow others");
		this.follow_others.setAttribute("type", "button");
		this.follow_others.setAttribute("onclick","toggleFollowOthers();");
		this.follow_others_img = document.createElement("img");
		this.follow_others_img.setAttribute("width", "24");
		this.follow_others_img.setAttribute("height", "24");
		this.follow_others_img.setAttribute("src", "./images/track-icon-on.svg");
		this.follow_others.append(this.follow_others_img);

		this.undo = document.createElement("button");
		this.undo.setAttribute("title", "Undo");
		this.undo.setAttribute("class", "toolbar-button");
		this.undo.setAttribute("onclick","undo();");
		img = document.createElement("img");
		img.setAttribute("width", "24");
		img.setAttribute("height", "24");
		img.setAttribute("src", "./images/undo.svg");
		this.undo.append(img);

		var submenus = [ { name : "Export to PDF", action : "openExportPDF();"}
			, { name : "Share link", action : "copyShareLink();"}
			, { name : "Erase whiteboard", action : "eraseWhiteboard();"}
			];
		if(window.debug.active)
		{
			submenus.push({name : "Debug", action : "openDebug();"});
		}
		this.whiteboard_menu = createSubmenu("Whiteboard", "openWhiteboardMenu();", "285px",
			submenus
			);
		this.erase_whiteboard_text = this.whiteboard_menu.submenus[2].children[0];
		this.erase_whiteboard_button = this.whiteboard_menu.submenus[2];
		//this.whiteboard_menu = document.createElement("button");
		//this.whiteboard_menu.setAttribute("class", "toolbar-menu");
		//this.whiteboard_menu.setAttribute("onclick", "openWhiteboardMenu();");
		//this.whiteboard_menu.setAttribute("onmouseover", "hoverSubmenu();");
		//this.whiteboard_menu.setAttribute("onmouseout", "leaveSubmenu();");
		//var span = document.createElement("span");
		//span.setAttribute("class", "toolbar-button");
		//span.innerHTML = "Whiteboard";
		//img = document.createElement("img");
		//img.setAttribute("width", "8");
		//img.setAttribute("height", "8");
		//img.setAttribute("src", "./images/arrow-down.svg");
		//this.whiteboard_menu.append(span);
		//this.whiteboard_menu.append(img);

		//this.whiteboard_submenu = document.createElement("div");
		//this.whiteboard_submenu.setAttribute("class", "submenu");
		//this.whiteboard_submenu.setAttribute("style", "left:216px;");
		//this.export_pdf = createSubmenuButton("Export to PDF", "openExportPDF();");
		//this.share_link = createSubmenuButton("Share link", "copyShareLink();");
		//this.erase_whiteboard = createSubmenuButton("Erase whiteboard", "eraseWhiteboard();");
		//this.erase_whiteboard_text = this.erase_whiteboard.children[0];

		//this.whiteboard_submenu.append(this.export_pdf);
		//this.whiteboard_submenu.append(this.share_link);
		//this.whiteboard_submenu.append(this.erase_whiteboard);

		this.user_menu = document.createElement("button");
		this.user_menu.setAttribute("class", "toolbar-menu");
		this.user_menu.setAttribute("onclick", "openUserMenu();");
		this.user_menu.setAttribute("onmouseover", "hoverSubmenu();");
		this.user_menu.setAttribute("onmouseout", "leaveSubmenu();");
		var span = document.createElement("span");
		span.setAttribute("class", "toolbar-button");
		span.innerHTML = "User";
		img = document.createElement("img");
		img.setAttribute("width", "8");
		img.setAttribute("height", "8");
		img.setAttribute("src", "./images/arrow-down.svg");
		this.user_menu.append(span);
		this.user_menu.append(img);

		this.user_submenu = document.createElement("div");
		this.user_submenu.setAttribute("class", "submenu");
		this.user_submenu.setAttribute("style", "left:410px;");
		this.settings = createSubmenuButton("Settings", "settings();");
		this.logout = createSubmenuButton("Log out", "logout();");
		this.list_whiteboards = createSubmenuButton("My whiteboards", "listWhiteboards();");
		this.user_submenu.append(this.list_whiteboards);
		this.user_submenu.append(this.settings);
		this.user_submenu.append(this.logout);

    this.view_menu = createSubmenu("View", "openViewMenu();", "216px", 
                                      [ { name : "Enable zooming", action : "toggleZooming();"}
                                      , { name : "Center on drawing", action : "toggleCenterOnDrawing();"}
                                      ]);
    this.view_menu.icons = [];
    for(var i = 0 ; i < this.view_menu.submenus.length ; ++i)
    {
      var img = document.createElement("img");
      img.setAttribute("width", "10");
      img.setAttribute("height", "10");
      img.setAttribute("src", "./images/checkbox-empty.svg");
      this.view_menu.submenus[i].prepend(img);
      this.view_menu.icons.push(img);
    }
		this.zooming_icon = this.view_menu.icons[0];
		this.center_on_drawing_icon = this.view_menu.icons[1];

		var thickness = [1.25, 1.75, 2.25, 5];
		var thickness_text = ["thin", "medium", "thick", "very thick"];
		this.thickness_button = [];
		this.thickness_circle = [];
		for(var i = 0; i < thickness.length ; ++i)
		{
			var button = document.createElement("button");
			button.setAttribute("type", "button");
			button.setAttribute("class", "thickness-button");
			button.setAttribute("title", thickness_text[i]);
			button.setAttribute("onclick", "setThickness(" + thickness[i] + ", " + i + ");");
			var svgns = "http://www.w3.org/2000/svg";
			var svg = document.createElementNS(svgns, 'svg');
			svg.setAttribute("width", "12px");
			svg.setAttribute("height", "28px");
			var circle = document.createElementNS(svgns, "circle");
			circle.setAttribute("stroke", "none");
			circle.setAttribute("fill", "#ccc");
			circle.setAttribute("stroke-width", "0");
			circle.setAttribute("cx", "6");
			circle.setAttribute("cy", "16");
			circle.setAttribute("r", thickness[i]);
			svg.append(circle);
			button.append(svg);
			this.thickness_button.push(button);
			this.thickness_circle.push(circle);
		}

		this.color_picker = document.createElement("div");
		this.color_picker.setAttribute("title", "Colorpicker");
		this.color_picker.setAttribute("class", "colorpicker");

		this.color_picker_inner = document.createElement("div");
		this.color_picker_inner.setAttribute("id", "whiteboardColorpicker");
		this.color_picker_inner.setAttribute("value", "#000000");
		this.color_picker_inner.setAttribute("class", "colorpicker-inner");
		this.color_picker.append(this.color_picker_inner);

		this.buttons.append(this.previous_page);
		this.buttons.append(this.next_page);
		this.buttons.append(this.page_number);
		this.buttons.append(this.follow_others);
		this.buttons.append(this.undo);
		for(var i = 0 ; i < this.thickness_button.length ; ++i)
		{
			this.buttons.append(this.thickness_button[i]);
		}
		this.buttons.append(this.color_picker);
		this.buttons.append(this.view_menu.menu);
		this.buttons.append(this.whiteboard_menu.menu);
		this.buttons.append(this.user_menu);
		this.toolbar.append(this.buttons);
		this.container.append(this.submenus);
	}

	followOthersOn()
	{
		this.follow_others_img.setAttribute("src", "./images/track-icon-on.svg");
	}

	followOthersOff()
	{
		this.follow_others_img.setAttribute("src", "./images/track-icon-off.svg");
	}

	setup()
	{
		this.container.append(this.toolbar);
	}

	setPage(page_number)
	{
		this.page_number_text.innerHTML=page_number;
	}

	setThickness(i)
	{
		this.thickness_circle[this.active_thickness].setAttribute("fill","#ccc");
		this.thickness_circle[i].setAttribute("fill","#aaf");
		this.active_thickness = i;
	}

	// TODO: Remove page lock
	setPageLock(page_lock)
	{
	}


	openWhiteboardMenu()
	{
		this.submenus.innerHTML = '';
		this.eraseWhiteboard();
		this.submenus.append(this.whiteboard_menu.container);
	}

	openUserMenu()
	{
		this.submenus.innerHTML = '';
		this.submenus.append(this.user_submenu);
	}

	openViewMenu()
	{
		this.submenus.innerHTML = '';
		this.submenus.append(this.view_menu.container);
	}

	hideSubmenus()
	{
		this.submenus.innerHTML = '';
		if(this.hover_interval !== undefined)
		{
			window.clearInterval(this.hover_interval);
			this.hover_interval = undefined;
		}
	}

	confirmEraseWhiteboard()
	{
		this.erase_whiteboard_text.innerHTML = "Confirm erase";
		this.erase_whiteboard_button.setAttribute("onclick", "confirmEraseWhiteboard();");
	}

	eraseWhiteboard()
	{
		this.erase_whiteboard_text.innerHTML = "Erase whiteboard";
		this.erase_whiteboard_button.setAttribute("onclick", "eraseWhiteboard();");
	}

	hoverSubmenu()
	{
		if(this.hover_interval !== undefined)
		{
			window.clearInterval(this.hover_interval);
			this.hover_interval = undefined;
		}
	}

	setZooming(zooming)
	{
		if(zooming)
		{
			this.zooming_icon.setAttribute("src", "./images/checkbox-checked.svg");
		}
		else
		{
			this.zooming_icon.setAttribute("src", "./images/checkbox-empty.svg");
		}
	}

	setCenterOnDrawing(flag)
	{
		if(flag)
		{
			this.center_on_drawing_icon.setAttribute("src", "./images/checkbox-checked.svg");
		}
		else
		{
			this.center_on_drawing_icon.setAttribute("src", "./images/checkbox-empty.svg");
		}
	}

	leaveSubmenu()
	{
		this.hover_interval = window.setInterval(
			((menu) => {return (() => {menu.hideSubmenus();})})(this), 1000);
	}
}

export class Toolbar
{
	constructor(container)
	{
		this.container = container;
		var icon, button;
		// Box selection
		icon = createElement("img", {src : "./images/box-selection-tool.svg"});
		this.box_selection_button = createButton("Box selection", "whiteboardTool", "", 'setTool("boxSelection");');
		this.box_selection_button.append(icon);
		this.container.append(this.box_selection_button);
		// Line
		icon = createElement("img", {src : "./images/line-tool.svg"});
		this.line_button = createButton("Line", "whiteboardTool", "", 'setTool("line");');
		this.line_button.append(icon);
		this.container.append(this.line_button);
		// Rectangle
		icon = createElement("img", {src : "./images/rectangle-tool.svg"});
		this.rectangle_button = createButton("Rectangle", "whiteboardTool", "", 'setTool("rectangle");');
		this.rectangle_button.append(icon);
		this.container.append(this.rectangle_button);
		// Circle
		icon = createElement("img", {src : "./images/circle-tool.svg"});
		this.circle_button = createButton("Circle", "whiteboardTool", "", 'setTool("circle");');
		this.circle_button.append(icon);
		this.container.append(this.circle_button);
		// Pointer 
		icon = createElement("img", {src : "./images/mouse-tool.svg", width : 24});
		this.pointer_button = createButton("Pointer", "whiteboardTool", "", 'setTool("pointer");');
		this.pointer_button.append(icon);
		this.container.append(this.pointer_button);
		// Eraser
		icon = createElement("img", {src : "./images/eraser.svg", width : 24});
		this.eraser_button = createButton("Eraser", "whiteboardTool", "", 'setTool("eraser");');
		// this.eraser_button = createButton("Eraser", "whiteboardTool", "", 'setTool("eraser");');
		this.eraser_button.append(icon);
		this.container.append(this.eraser_button);
		// Pen
		icon = createElement("img", {src : "./images/pen.svg", width : 24});
		this.pen_button = createButton("Pen", "whiteboardTool", "", 'setTool("pen");');
		this.pen_button.append(icon);
		this.container.append(this.pen_button);
		this.active_tool = this.pen_button;

		// Colors
		this.colors = 
			[ { name : "White",   color : "#FFFFFF"}
			, { name : "Gray" ,   color : "#AAAAAA"}
			, { name : "Black",   color : "#000000"}
			, { name : "Red"   ,  color : "#ff5a5a"}
			, { name : "Green" ,  color : "#1AC013"}
			, { name : "Blue"  ,  color : "#5a73ff"}
			, { name : "Orange",  color : "#fc7114"}
			, { name : "Yellow",  color : "#d4b400"}
			, { name : "Magenta", color : "#ff5aff"}
			, { name : "Mint",    color : "#03d899"}
			, { name : "Cyan",    color : "#80cdff"}
			];
		for(var i = 0 ; i < this.colors.length ; ++i)
		{
			var icon_svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
			icon_svg.setAttribute("width", "24");
			icon_svg.setAttribute("height", "28");
			var icon = document.createElementNS("http://www.w3.org/2000/svg", "circle");
			icon.setAttribute("stroke", "#ffffff");
			icon.setAttribute("fill", this.colors[i].color);
			icon.setAttribute("stroke-width", "1.5");
			icon.setAttribute("cx", "12");
			icon.setAttribute("cy", "14");
			icon.setAttribute("r", "10");
			icon_svg.append(icon);
			var button = createButton(this.colors[i].name, "whiteboardBtn", "", "setColor(\"" + this.colors[i].color + "\");");
			button.append(icon_svg);
			this.container.append(button);
		}
	}

	setTool(tool)
	{
		this.active_tool.setAttribute("class", "whiteboardTool");
		switch(tool)
		{
			case "boxSelection":
				this.active_tool = this.box_selection_button;
				break;
			case "line":
				this.active_tool = this.line_button;
				break;
			case "rectangle":
				this.active_tool = this.rectangle_button;
				break;
			case "circle":
				this.active_tool = this.circle_button;
				break;
			case "pointer":
				this.active_tool = this.pointer_button;
				break;
			case "eraser":
				this.active_tool = this.eraser_button;
				break;
			case "pen":
				this.active_tool = this.pen_button;
				break;
		}
		this.active_tool.setAttribute("class", "active whiteboardTool");
	}
}

function createSubmenu(name, openAction, position, items)
{
  var menu = document.createElement("button");
  menu.setAttribute("class", "toolbar-menu");
  menu.setAttribute("onclick", openAction);
  menu.setAttribute("onmouseover", "hoverSubmenu();");
  menu.setAttribute("onmouseout", "leaveSubmenu();");
  var span = document.createElement("span");
  span.setAttribute("class", "toolbar-button");
  span.innerHTML = name;
  var img = document.createElement("img");
  img.setAttribute("width", "8");
  img.setAttribute("height", "8");
  img.setAttribute("src", "./images/arrow-down.svg");
  menu.append(span);
  menu.append(img);

	var container = document.createElement("div");
	container.setAttribute("class", "submenu");
	container.setAttribute("style", "left:" + position + ";");
  var submenus = [];
  for(var i = 0 ; i < items.length ; ++i)
  {
    var submenu_button = createSubmenuButton(items[i].name, items[i].action);
    container.append(submenu_button);
    submenus.push(submenu_button);
  }

  return {menu : menu, container : container, submenus : submenus};
}

function createSubmenuButton(text, callback)
{
	var button = document.createElement("button");
	button.setAttribute("class", "submenu");
	button.setAttribute("onclick", callback);
	button.setAttribute("onmouseover", "hoverSubmenu();");
	button.setAttribute("onmouseout", "leaveSubmenu();");
	var span = document.createElement("span");
	span.setAttribute("class", "toolbar-button");
	span.innerHTML = text;
	button.append(span);
	return button;
}

function createTable(table, rows)
{
	var table_el = createElement("table", table.attributes);
	var i = 0;
	for(let i in table.header.columns)
	{
		var col = table.header.columns[i];
		var column = createElement("col", col.attributes);
		table_el.appendChild(column);
		i++;
	}
	var thead = createElement("thead", table.header.attributes);
	var theadtr = document.createElement("tr");
	for(let i in table.header.columns)
	{
		var col = table.header.columns[i];
		var column = createElement("th", col.attributes, col.content);
		// column.innerHTML = col.content;
		theadtr.appendChild(column);
	}
	thead.appendChild(theadtr);
	table_el.appendChild(thead);
	for(let i in rows)
	{
		var row = rows[i];
		var trow = createElement("tr", row.attributes);
		createRow(row.cells, trow);
		table_el.appendChild(trow);
	}
	return table_el;
}

function createRow(cells, trow)
{
	for(let i in cells)
	{
		var cell = cells[i];
		var td = createElement("td", cell.attributes);
		if(typeof(cell.content) !== 'undefined')
		{
			td.appendChild(cell.content);
		}
		trow.appendChild(td);
	}
}

function createElement(name, attributes, innerHTML)
{
	var element = document.createElement(name);
	for(let attr in attributes)
	{
		element.setAttribute(attr, attributes[attr]);
	}
	if(typeof innerHTML !== 'undefined')
		element.innerHTML = innerHTML;
	return element;
}

function createButton(name, cssClass, innerHTML, callback)
{
	var button = createElement("button", {"class" : cssClass, "onpointerdown" : callback, title : name}, innerHTML);
	return button;
}
