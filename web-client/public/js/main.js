var queryVar = getQueryVariables();
var url = document.URL.substr(0, document.URL.lastIndexOf('/'));

import {setNewUrl, escapeHtml} from "./utils.js";
import {LoginMenu, WhiteboardMenu, ImportPDFMenu, ExportPDFMenu, DebugMenu, SettingsMenu, TopBar, Toolbar} from "./menu.js";
import {whiteboard} from "./whiteboard.js";
import {ToolType} from "./interaction.js";
import {Communication} from "./communication.js";
import {DrawingElement} from "./drawingElement.js";
import {Debugger} from "./debug.js";

window.cancelExportPDF = cancelExportPDF;
window.cancelImportPDFButton = cancelImportPDFButton;
window.confirmEraseWhiteboard = confirmEraseWhiteboard;
window.copyShareLink = copyShareLink;
window.copyDebugLog = copyDebugLog;
window.closeWhiteboardList = closeWhiteboardList;
window.closeDebug = closeDebug;
window.clearDebugLog = clearDebugLog;
window.createWhiteboardButton = createWhiteboardButton;
window.dropFile = dropFile;
window.eraseWhiteboard = eraseWhiteboard;
window.exportPDF = exportPDF;
window.hoverSubmenu = hoverSubmenu;
window.importPDFButton = importPDFButton;
window.leaveSubmenu = leaveSubmenu;
window.listWhiteboards = listWhiteboards;
window.loginMenuButton = loginMenuButton;
window.logout = logout;
window.nextPage = nextPage;
window.openExportPDF = openExportPDF;
window.openDebug = openDebug;
window.openUserMenu = openUserMenu;
window.openViewMenu = openViewMenu;
window.openWhiteboardMenu = openWhiteboardMenu;
window.previousPage = previousPage;
window.receiveUserInfo = receiveUserInfo;
window.removeWhiteboardButton = removeWhiteboardButton;
window.restoreWhiteboardButton = restoreWhiteboardButton;
window.setThickness = setThickness;
window.setColor = function (color)
{
	if(whiteboard.tool === ToolType['eraser'] || whiteboard.tool === ToolType['pointer'] || whiteboard.tool === ToolType['boxSelection'])
	{
		toolbar.setTool("pen");
		whiteboard.setTool("pen");
	}
	whiteboard.setDrawColor(color);
	$("#whiteboardColorpicker").css({ "background": color });
};
window.setTool = function (tool)
{
	toolbar.setTool(tool);
	whiteboard.setTool(tool);
}
window.settings = settings;
window.setUserBadgeColor = setUserBadgeColor;
window.toggleDebugLevel = function (level)
{
	window.debug.toggleLevel(level);
}
window.toggleFollowOthers = toggleFollowOthers;
window.toggleCenterOnDrawing = toggleCenterOnDrawing;
window.toggleZooming = toggleZooming;
window.undo = undo;
window.updateSettings = updateSettings;

var debug = new Debugger("zauboard-debug", true);
debug.clear();

window.debug = debug;

if(typeof queryVar["debug"] === 'string')
{
	if(queryVar["debug"] !== '1')
	{
		debug.disable();
	}
}
else
{
	debug.disable();
}

var userInfoScreen = document.getElementById("userInfoScreen");
var loginMenu = new LoginMenu(userInfoScreen);
var whiteboardMenu = new WhiteboardMenu(userInfoScreen, url);
var importPDFMenu = new ImportPDFMenu(userInfoScreen);
var exportPDFMenu = new ExportPDFMenu(userInfoScreen);
var debugMenu = new DebugMenu(userInfoScreen);
var topBar = new TopBar(document.getElementById("toolbar"));
var toolbar = new Toolbar(document.getElementById("sidepane"));
var settingsMenu = new SettingsMenu(userInfoScreen);
var date = new Date()
var user_id;
var whiteboard_name;
var board_key;
var access_token;
var pageLock = true;
var owner;
var lastReconnect = date.getTime();
var thickness_btn = "#thickness-2-btn";
var subdir = "";

var user_key = getCookieValue('zauboard-key');
var user_config = parseQueryVariables(getCookieValue('zauboard-config'));

if(typeof queryVar["board_key"] === 'string')
{
	board_key = queryVar["board_key"];
}
else
{
	access_token = queryVar["accesstoken"];
	access_token = access_token || "";
}

var page_number = Number(queryVar["page"]) || 1;
var my_username = queryVar["username"] || "guest";
var accessDenied = false;

// Custom Html Title
var title = queryVar["title"];
if (typeof title === 'string')
{
	document.title = decodeURIComponent(title);
}

topBar.setup();
topBar.setThickness(1);
topBar.setPage(page_number);

var communication = setupCommunication();
whiteboard.loadWhiteboard("whiteboardContainer", {
  whiteboardId: "",
  username: btoa(my_username),
  user_id: user_id,
  prefix: subdir,
  communication: communication
});
setupWhiteboardSettings();

loginMenu.onGuestLogin = function ()
{
  communication.onAccessDenied = function()
  {
    loginMenu.onAccessDenied();
  }
  communication.onAccessGranted = afterLogin;
	communication.onClose = function(event)
	{
		communication.loginAsGuest();
	}
  communication.loginAsGuest();
}
loginMenu.onLogin = function (username, password)
{
  communication.onAccessDenied = function()
  {
    loginMenu.onAccessDenied();
  }
  communication.onAccessGranted = afterLogin;
	communication.onClose = function(event)
	{
  	communication.loginWithUserPassword(username, password);
	}
  communication.loginWithUserPassword(username, password);
}
function loginMenuButton(button)
{
	loginMenu.buttonPressed(button);
}
function createWhiteboardButton()
{
	var whiteboard_name = whiteboardMenu.createWhiteboard();
  communication.createWhiteboard(whiteboard_name, false);
  communication.listWhiteboards();
}
function removeWhiteboardButton(whiteboard_key)
{
  communication.removeWhiteboard(whiteboard_key);
  communication.listWhiteboards();
}
function restoreWhiteboardButton(whiteboard_key)
{
  communication.restoreWhiteboard(whiteboard_key);
  communication.listWhiteboards();
}

whiteboardMenu.onCreateWhiteboard = function(whiteboard_name)
{
	communication.createWhiteboard(whiteboard_name);
}
//whiteboard.settings.communication = communication;

function setupCommunication()
{
	var communication = new Communication();
	communication.onSwitchPage = function(page)
	{
		if(pageLock && page_number !== page)
		{
			page_number = page;
			topBar.setPage(page_number);
			setNewUrl(url, board_key, page_number);
      reloadWhiteboard();
		}
	}
	communication.onDrawings = function(drawings)
	{
		for(var i = 0 ; i < drawings.length ; ++i)
    {
      var dr = new DrawingElement(drawings[i]);
			whiteboard.incomingDrawing(dr);
    }
	}
	communication.onProvide = function(drawings)
	{
		for(var i = 0 ; i < drawings.length ; ++i)
			whiteboard.insertDrawing(drawings[i]);
	}
	communication.onRestore = function(ids)
	{
		whiteboard.restoreDrawings(ids);
	}
	communication.onErase = function(ids)
	{
		whiteboard.eraseDrawings(ids);
	}
	communication.onRequest = function(ids)
	{
		var drawings = whiteboard.getDrawings(ids);
		if(drawings.length > 0)
			communication.provide(drawings);
	}
  communication.onProvideUserToken = function(token)
  {
    user_key = token;
    setCookie("zauboard-key", token);
  }
	communication.onSetUserID = function(uid)
	{
		accessDenied = false;
		user_id = uid;
		whiteboard.setUserId(user_id);
	}
	communication.onSetTag = function(tag)
	{
		whiteboard.setTag(tag.id, tag);
	}

  communication.onCursorUpdate = function(cx, cy, uid)
  {
    whiteboard.cursorUpdate(cx,cy,uid);
  }
	communication.onAccessDenied = function()
	{
    user_key = '';
		setCookie("zauboard-key","");
		loginMenu.popup();
	}
	communication.onAccessGranted = function()
	{
		// to renew cookie expire date
		if(typeof user_key !== 'undefined')
			setCookie("zauboard-key", user_key);
		afterLogin();
	}
	communication.onClose = function(event)
	{
		communication.loginWithUserToken(user_key);
	}
	communication.onInit = function()
	{
    if(typeof user_key !== 'undefined' && user_key !== "")
    {
			communication.loginWithUserToken(user_key);
    }
    else if(typeof board_key !== 'undefined' && board_key !== "")
    {
			communication.loginAsGuest();
    }
	}
	communication.onClear = function()
	{
		whiteboard.clear();
	}
	communication.onOpen = function()
	{
    communication.onInit();
	}
  communication.onWhiteboards = function(whiteboards)
  {
		whiteboardMenu.onWhiteboards(whiteboards);
    keymage.popScope("whiteboard");
		whiteboardMenu.popup();
  }
	communication.onPdfSummary = function(summary)
	{
		importPDFMenu.onPDFSummary(summary);
	}
  communication.onSaidFailure = function()
  {
    communication.state = "open";
  }
	communication.onIncorrectPassword = function()
	{
		console.log("Incorrect pass");
		settingsMenu.incorrectPassword();
	}

	var urlSplit = url.split("/");
	subdir = "";
	for (var i = 3; i < urlSplit.length; i++)
	{
		subdir = subdir + '/' + urlSplit[i];
	}
	if (subdir !== "")
		communication.connect(window.location.host + subdir + '/', window.location.protocol); //Connect even if we are in a subdir behind a reverse proxy
	else
		communication.connect(window.location.host, window.location.protocol);
	return communication;
}


function reloadWhiteboard()
{
	var whiteboardNode = document.getElementById("whiteboardContainer");
	whiteboardNode.textContent = '';
	whiteboard.loadWhiteboard("whiteboardContainer", { //Load the whiteboard
		whiteboardId: "",
		username: btoa(my_username),
		user_id: user_id,
		prefix: subdir,
		communication: communication
	});
	if(typeof whiteboard.thickness === 'undefined')
		whiteboard.setStrokeThickness(2); // default thickness

	// request whiteboard from server
	if(typeof board_key === 'string')
	{
		communication.joinWhiteboard(board_key, page_number);
	}
	setupWhiteboardSettings();
}

function setupKeyboardShortcuts ()
{
	//Handle key actions

	var shortcutFunctions = {
		clearWhiteboard: function () { whiteboard.localUser.clear(); },
		undoStep: function () { whiteboard.undoWhiteboardClick(); },
		setTool_mouse: function () { setTool("pointer");},
		setTool_recSelect: function () { $(".whiteboardTool[tool=recSelect]").click(); },
		setTool_pen: function () {
			setTool("pen");
			whiteboard.redrawMouseCursor();
		},
		setTool_line: function () { setTool("line");},
		setTool_rect: function () { setTool("rectangle");},
		setTool_circle: function () { setTool("circle");},
		// setTool_text: function () {  $(".whiteboardTool[tool=text]").click(); },
		setTool_eraser: function () {
			setTool("eraser");
			whiteboard.redrawMouseCursor();
		},
		thickness_bigger: function () {
			whiteboard.setStrokeThickness(whiteboard.thickness + 1);
			whiteboard.redrawMouseCursor();
		},
		thickness_smaller: function () {
			whiteboard.setStrokeThickness(whiteboard.thickness - 1);
			whiteboard.redrawMouseCursor();
		},
		openColorPicker: function () { $("#whiteboardColorpicker").click(); },
		saveWhiteboardAsImage: function () { $("#saveAsImageBtn").click(); },
		saveWhiteboardAsJson: function () { $("#saveAsJSONBtn").click(); },
		uploadWhiteboardToWebDav: function () { $("#uploadWebDavBtn").click(); },
		uploadJsonToWhiteboard: function () { $("#uploadJsonBtn").click(); },
		shareWhiteboard: function () { $("#shareWhiteboardBtn").click(); },
		hideShowControls: function () { $("#minMaxBtn").click(); },

		setDrawColorBlack: function () {
			whiteboard.setDrawColor("black");
			whiteboard.redrawMouseCursor();
			$("#whiteboardColorpicker").css({ "background": "black" });
		},
		setDrawColorRed: function () {
			whiteboard.setDrawColor("red");
			whiteboard.redrawMouseCursor();
			$("#whiteboardColorpicker").css({ "background": "red" });
		},
		setDrawColorGreen: function () {
			whiteboard.setDrawColor("green");
			whiteboard.redrawMouseCursor();
			$("#whiteboardColorpicker").css({ "background": "green" });
		},
		setDrawColorBlue: function () {
			whiteboard.setDrawColor("blue");
			whiteboard.redrawMouseCursor();
			$("#whiteboardColorpicker").css({ "background": "blue" });
		},
		setDrawColorYellow: function () {
			whiteboard.setDrawColor("yellow");
			whiteboard.redrawMouseCursor();
			$("#whiteboardColorpicker").css({ "background": "yellow" });
		},

		toggleLineRecCircle: function () {
			var activeTool = $(".whiteboardTool.active").attr("tool");
			if (activeTool === "line")
				setTool("rectangle");
			else if (activeTool === "rectangle")
				setTool("circle");
			else
				setTool("line");
		},
		togglePenEraser: function () {
			
			var activeTool = $(".whiteboardTool.active").attr("tool");
			if (activeTool === "pen")
				setTool("eraser");
			else
				setTool("pen");
		},
		toggleMainColors: function () {
			var bgColor = $("#whiteboardColorpicker")[0].style.backgroundColor;
			if (bgColor == "blue")
				shortcutFunctions.setDrawColorGreen();
			else if (bgColor == "green")
				shortcutFunctions.setDrawColorYellow();
			else if (bgColor == "yellow")
				shortcutFunctions.setDrawColorRed();
			else if (bgColor == "red")
				shortcutFunctions.setDrawColorBlack();
			else
				shortcutFunctions.setDrawColorBlue();
			
		},

		moveDraggableUp: function () {
			var elm = whiteboard.tool == "text" ? $("#" + whiteboard.latestActiveTextBoxId) : $(".dragMe")[0];
			var p = $(elm).position();
			$(elm).css({ top: p.top - 5, left: p.left })
		},
		moveDraggableDown: function () {
			var elm = whiteboard.tool == "text" ? $("#" + whiteboard.latestActiveTextBoxId) : $(".dragMe")[0];
			var p = $(elm).position();
			$(elm).css({ top: p.top + 5, left: p.left })
		},
		moveDraggableLeft: function () {
			var elm = whiteboard.tool == "text" ? $("#" + whiteboard.latestActiveTextBoxId) : $(".dragMe")[0];
			var p = $(elm).position();
			$(elm).css({ top: p.top, left: p.left - 5 })
		},
		moveDraggableRight: function () {
			var elm = whiteboard.tool == "text" ? $("#" + whiteboard.latestActiveTextBoxId) : $(".dragMe")[0];
			var p = $(elm).position();
			$(elm).css({ top: p.top, left: p.left + 5 })
		},
		dropDraggable: function () {
			$($(".dragMe")[0]).find('.addToCanvasBtn').click();
		},
		addToBackground: function () {
			$($(".dragMe")[0]).find('.addToBackgroundBtn').click();
		},
		cancelAllActions: function () { whiteboard.escKeyAction(); },
		deleteSelection: function () { whiteboard.delKeyAction(); },
	}

	//Load keybindings from keybinds.js to given functions
	for (var i in keybinds) {
		if (shortcutFunctions[keybinds[i]])
			keymage("whiteboard", i, shortcutFunctions[keybinds[i]], { preventDefault: true });
		else
			console.error("function you want to keybind on key:", i, "named:", keybinds[i], "is not available!")
	}
}

$(document).ready(function () {
	setNewUrl(url, board_key, page_number);
	if (queryVar["webdav"] == "true") {
		$("#uploadWebDavBtn").show();
	}

	topBar.setPageLock(pageLock);

	/*----------------/
	Whiteboard actions
	/----------------*/

	
	if((typeof user_key === 'undefined' || user_key === "") && (typeof board_key === 'undefined' || board_key === ""))
	{
    loginMenu.popup();
	}
	else
	{
		communication.init();
	}

	setTool("pen");

	// save image to json containing steps
	$("#saveAsJSONBtn").click(function () {
		var imgData = whiteboard.getImageDataJson();

		var w = window.open('about:blank'); //Firefox will not allow downloads without extra window
		setTimeout(function () { //FireFox seems to require a setTimeout for this to work.
			var a = document.createElement('a');
			a.href = window.URL.createObjectURL(new Blob([imgData], { type: 'text/json' }));
			a.download = 'whiteboard.json';
			w.document.body.appendChild(a);
			a.click();
			w.document.body.removeChild(a);
			setTimeout(function () { w.close(); }, 100);
		}, 0);
	});

	$("#uploadWebDavBtn").click(function () {
		if ($(".webdavUploadBtn").length > 0)
			return;

		var webdavserver = localStorage.getItem('webdavserver') || "";
		var webdavpath = localStorage.getItem('webdavpath') || "/";
		var webdavusername = localStorage.getItem('webdavusername') || "";
		var webdavpassword = localStorage.getItem('webdavpassword') || "";
		var webDavHtml = $('<div>' +
		'<table>' +
		'<tr>' +
		'<td>Server URL:</td>' +
		'<td><input class="webdavserver" type="text" value="' + webdavserver + '" placeholder="https://yourserver.com/remote.php/webdav/"></td>' +
		'<td></td>' +
		'</tr>' +
		'<tr>' +
		'<td>Path:</td>' +
		'<td><input class="webdavpath" type="text" placeholder="folder" value="' + webdavpath + '"></td>' +
		'<td style="font-size: 0.7em;"><i>path always have to start & end with "/"</i></td>' +
		'</tr>' +
		'<tr>' +
		'<td>Username:</td>' +
		'<td><input class="webdavusername" type="text" value="' + webdavusername + '" placeholder="username"></td>' +
		'<td style="font-size: 0.7em;"></td>' +
		'</tr>' +
		'<tr>' +
		'<td>Password:</td>' +
		'<td><input class="webdavpassword" type="password" value="' + webdavpassword + '" placeholder="password"></td>' +
		'<td style="font-size: 0.7em;"></td>' +
		'</tr>' +
		'<tr>' +
		'<td style="font-size: 0.7em;" colspan="3">Note: You have to generate and use app credentials if you have 2 Factor Auth activated on your dav/nextcloud server!</td>' +
		'</tr>' +
		'<tr>' +
		'<td></td>' +
		'<td colspan="2"><span class="loadingWebdavText" style="display:none;">Saving to webdav, please wait...</span><button class="modalBtn webdavUploadBtn"><i class="fas fa-upload"></i> Start Upload</button></td>' +
		'</tr>' +
		'</table>' +
		'</div>');
		webDavHtml.find(".webdavUploadBtn").click(function () {
			var webdavserver = webDavHtml.find(".webdavserver").val();
			localStorage.setItem('webdavserver', webdavserver);
			var webdavpath = webDavHtml.find(".webdavpath").val();
			localStorage.setItem('webdavpath', webdavpath);
			var webdavusername = webDavHtml.find(".webdavusername").val();
			localStorage.setItem('webdavusername', webdavusername);
			var webdavpassword = webDavHtml.find(".webdavpassword").val();
			localStorage.setItem('webdavpassword', webdavpassword);
			var base64data = whiteboard.getImageDataBase64();
			var webdavaccess = {
				webdavserver: webdavserver,
				webdavpath: webdavpath,
				webdavusername: webdavusername,
				webdavpassword: webdavpassword
			}
			webDavHtml.find(".loadingWebdavText").show();
			webDavHtml.find(".webdavUploadBtn").hide();
			saveWhiteboardToWebdav(base64data, webdavaccess, function (err) {
				if (err)
				{
					webDavHtml.find(".loadingWebdavText").hide();
					webDavHtml.find(".webdavUploadBtn").show();
				}
				else
				{
					webDavHtml.parents(".basicalert").remove();
				}
			});
		})
		showBasicAlert(webDavHtml, {
			header: "Save to Webdav",
			okBtnText: "cancel",
			headercolor: "#0082c9"
		})
	});

	// upload json containing steps
	$("#uploadJsonBtn").click(function () {
		$("#myFile").click();
	});

	$("#shareWhiteboardBtn").click(function () {
		var url = window.location.href;
		var s = url.indexOf("&username=") !== -1 ? "&username=" : "username="; //Remove username from url
		var urlSlpit = url.split(s);
		var urlStart = urlSlpit[0];
		if (urlSlpit.length > 1) {
			var endSplit = urlSlpit[1].split("&");
			endSplit = endSplit.splice(1, 1);
			urlStart += "&" + endSplit.join("&");
		}
		$("<textarea/>").appendTo("body").val(urlStart).select().each(function () {
			document.execCommand('copy');
		}).remove();
		showBasicAlert("Copied Whiteboard-URL to clipboard.", { hideAfter: 2 })
	});

	var btnsMini = false;
	$("#minMaxBtn").click(function () {
		if (!btnsMini)
		{
			$("#toolbar").find(".btn-group:not(.minGroup)").hide();
			$(this).find("#minBtn").hide();
			$(this).find("#maxBtn").show();
		}
		else
		{
			$("#toolbar").find(".btn-group").show();
			$(this).find("#minBtn").show();
			$(this).find("#maxBtn").hide();
		}
		btnsMini = !btnsMini;
	})

	// load json to whiteboard
	$("#myFile").on("change", function () {
		var file = document.getElementById("myFile").files[0];
		var reader = new FileReader();
		reader.onload = function (e) {
			try
			{
				var j = JSON.parse(e.target.result);
				whiteboard.loadJsonData(j);
			}
			catch (e)
			{
				showBasicAlert("File was not a valid JSON!");
			}
		};
		reader.readAsText(file);
		$(this).val("");
	});

	$('#whiteboardContainer').on("dragleave", function (e) {
		e.preventDefault();
		e.stopPropagation();
		dragCounter--;
		if (dragCounter === 0)
		{
			whiteboard.dropIndicator.hide();
		}
	});

	$('#whiteboardColorpicker').colorPicker({
		renderCallback: function (elm) {
		whiteboard.setDrawColor(elm.val());
		}
	});
});

//Prevent site from changing tab on drag&drop
window.addEventListener("dragover", function (e) {
	e = e || event;
	e.preventDefault();
}, false);
window.addEventListener("drop", function (e) {
	e = e || event;
	e.preventDefault();
}, false);

function uploadImgAndAddToWhiteboard(base64data) {
	var date = (+new Date());
	$.ajax({
		type: 'POST',
		url: document.URL.substr(0, document.URL.lastIndexOf('/')) + '/upload',
		data: {
			'imagedata': base64data,
			'whiteboardId': "", //TODO: properly fix this
			'date': date,
			'access_token': access_token
		},
		success: function (msg) {
			var filename = whiteboardId + "_" + date + ".png";
			whiteboard.addImgToCanvasByUrl(document.URL.substr(0, document.URL.lastIndexOf('/')) + "/uploads/" + filename); //Add image to canvas
		},
		error: function (err) {
			showBasicAlert("Failed to upload frame: " + JSON.stringify(err));
		}
	});
}

function copyToClipboard(str) {

	var fallback = true;
	if(typeof navigator !== undefined)
	{
		if("clipboard" in navigator)
		{
			navigator.clipboard.writeText(str);
			fallback = false;
		}
	}
	if(fallback)
	{
		var textArea = document.createElement("textarea");
	  textArea.value = str;
  
		// Avoid scrolling to bottom
		textArea.style.top = "0";
		textArea.style.left = "0";
		textArea.style.position = "fixed";

		document.body.appendChild(textArea);
		textArea.focus();
		textArea.select();
    document.execCommand('copy');
		textArea.remove();
	}
}

function saveWhiteboardToWebdav(base64data, webdavaccess, callback) {
	var date = (+new Date());
	$.ajax({
		type: 'POST',
		url: document.URL.substr(0, document.URL.lastIndexOf('/')) + '/upload',
		data: {
			'imagedata': base64data,
			'whiteboardId': whiteboardId,//TODO: properly fix this
			'date': date,
			'at': access_token,
			'webdavaccess': JSON.stringify(webdavaccess)
		},
		success: function (msg)
		{
			showBasicAlert("Whiteboard was saved to Webdav!", {
				headercolor: "#5c9e5c"
				});
			callback();
		},
		error: function (err) {
			if (err.status == 403)
			{
				showBasicAlert("Could not connect to Webdav folder! Please check the credentials and paths and try again!");
			}
			else
			{
				showBasicAlert("Unknown Webdav error! ", err);
			}
			callback(err);
		}
	});
}

// verify if filename refers to an image
function isImageFileName(filename) {
	var extension = filename.split(".")[filename.split(".").length - 1];
	var known_extensions = ["png", "jpg", "jpeg", "gif", "tiff", "bmp", "webp"];
	return known_extensions.includes(extension.toLowerCase());
}

// verify if given url is url to an image
function isValidImageUrl(url, callback) {
	var img = new Image();
	var timer = null;
	img.onerror = img.onabort = function () {
		clearTimeout(timer);
		callback(false);
	};
	img.onload = function () {
		clearTimeout(timer);
		callback(true);
	};
	timer = setTimeout(function () {
		callback(false);
	}, 2000);
	img.src = url;
}

// handle pasting from clipboard
window.addEventListener("paste", function (e) {
	if ($(".basicalert").length > 0) {
		return;
	}
	if (e.clipboardData)
	{
		var items = e.clipboardData.items;
		var imgItemFound = false;
		if (items)
		{
			// Loop through all items, looking for any kind of image
			for (var i = 0; i < items.length; i++)
			{
				if (items[i].type.indexOf("image") !== -1)
				{
					imgItemFound = true;
					// We need to represent the image as a file,
					var blob = items[i].getAsFile();

					var reader = new window.FileReader();
					reader.readAsDataURL(blob);
					reader.onloadend = function ()
					{
						base64data = reader.result;
						uploadImgAndAddToWhiteboard(base64data);
					}
				}
			}
		}

		if (!imgItemFound && whiteboard.tool != "text")
		{
			showBasicAlert("Please drag-and-drop the image into the whiteboard.");
		}
	}
});

function showBasicAlert(html, newOptions) {
	var options = {
		header: "INFO MESSAGE",
		okBtnText: "Ok",
		headercolor: "#e6e6ff",
		hideAfter: false
	}
	if (newOptions) {
		for (var i in newOptions) {
		options[i] = newOptions[i];
		}
	}
	var alertHtml = $('<div class="basicalert" style="position:absolute; top:0px; left:0px; width:100%; top:70px; font-family: monospace;">' +
		'<div style="width: 30%; margin: auto; background: #e6e6ff; border-radius: 5px; font-size: 1.2em; border: 1px solid gray;">' +
		'<div style="border-bottom: 1px solid #99d; background: ' + options["headercolor"] + '; padding-left: 5px; font-size: 0.8em;">' + options["header"] + '</div>' +
		'<div style="padding: 10px;" class="htmlcontent"></div>' +
		'<div style="height: 20px; padding: 10px;"><button class="modalBtn okbtn" style="float: right;">' + options["okBtnText"] + '</button></div>' +
		'</div>' +
		'</div>');
	alertHtml.find(".htmlcontent").append(html);
	$("body").append(alertHtml);
	alertHtml.find(".okbtn").click(function () {
		alertHtml.remove();
	})
	if (options.hideAfter) {
		setTimeout(function () {
		alertHtml.find(".okbtn").click();
		}, 1000 * options.hideAfter)
	}
}

// get 'GET' parameters
function getQueryVariables() {
	var query = window.location.search.substring(1);
	return parseQueryVariables(query);
}

function parseQueryVariables(query)
{
	var result = {};
	var vars = query.split("&");
	for (var i = 0; i < vars.length; i++)
	{
		var pair = vars[i].split("=");
		result[pair[0]] = decodeURIComponent(pair[1]);
	}
	return result;
}

function getCookieValue(cookie) {
    var value = document.cookie.match('(^|[^;]+)\\s*' + cookie + '\\s*=\\s*([^;]+)');
    return value ? value.pop() : '';
}

function parseCookie()
{
	var result = {};
	var vars = document.cookie.split(';');
	for (var i = 0; i < vars.length; i++)
	{
		var pair = vars[i].split("=");
		if(pair.length == 1)
			result[""] = pair[0].trim();
		else
			result[pair[0].trim()] = pair[1].trim();
	}
	return result;
}

function setCookie(cname, cvalue)
{
	var expire = new Date();
	expire.setTime(expire.getTime() + 30*24*60*60*1000); // cookies expire after one month
	document.cookie = cname + "=" + cvalue + ";path=/;expires=" + expire.toUTCString() + ";SameSite=Lax;"
}

function askForUserInfo()
{
	var user_info_el = document.getElementById("userInfoScreen");
	var color_div = '<div class="welcomeColorList">';
	const colors =
		[ {bg : "#000", fg : "#fff"}
		, {bg : "#ffc9c9", fg :"#d20000"}
		, {bg : "#bff7bb", fg :"#0e540c"}
		, {bg : "#d3d1fb", fg :"#0c0c74"}
		// second line
		, {bg : "#fff", fg : "#000"}
		, {bg : "#b8f9f2", fg :"#084f50"}
		, {bg : "#f6b8f9", fg : "#50084b"}
		, {bg : "#f7f9b8", fg : "#3f3e07"}
		];
	for(const color of colors)
	{
		color_div += '<button type="button" class="welcomeColor" style="background-color:' + color.bg + ';border: 1px solid ' + color.fg + ';" onclick="setUserBadgeColor(\'' + color.bg + '\',\'' + color.fg + '\')"></button>';
	}
	if(typeof user_config.badge_bg === 'undefined')
		user_config.badge_bg = colors[0].bg;
	if(typeof user_config.badge_fg === 'undefined')
		user_config.badge_fg = colors[0].fg;
	color_div += "</div>";
	var html =
			"<div class=\"dimmed\">"
		+ "<div class=\"userInfoDialog\">"
		+ "<h2 class=\"dialogTitle\">Welcome</h2>"
		+ '<p class="dialogTitle" style="position:absolute;left:30%;">Cursor:</p>'
		+ '<div id="cursorPreview" class="cursorPreview" style="background:#000;">'
		+ '<div id="cursorTagPreview" class="cursorTagPreview" style="background-color:' + user_config.badge_bg + ';color:' + user_config.badge_fg + ';">Name</div></div>'
		+ "<form id=\"userInfoForm\">"
		+ "<input id=\"userInfoNameField\" class=\"welcome\" type=\"text\" name=\"username\" maxlength=\"16\" placeholder=\"Name\">"
		+ "<br>"
		+ '<p id="dialogWarning" class="dialogWarning"></p>'
		+ "<button type=\"button\" class=\"welcome\" onclick=\"receiveUserInfo()\">Ok</button>"
		+ "</form>"
		+ '<p class="dialogTitle">Choose a color</p>'
		+ color_div
		+ "</div>"
		+ "</div>";
	user_info_el.innerHTML = html;
	var name_field = document.getElementById("userInfoNameField");
	var name_tag = document.getElementById("cursorTagPreview");
	name_tag.innerHTML = "Name";
	name_field.addEventListener('keyup', function (e) {
		name_tag.innerHTML=escapeHtml(name_field.value.slice(0,16));
	});
	document.addEventListener('keydown', function (e) {
		// enter was pressed
		if(e.keyCode === 13)
		{
			e.preventDefault();
			receiveUserInfo();
		}
	});
}

function setUserBadgeColor(bg,fg)
{
	settingsMenu.setTagColor(bg,fg);
	user_config.badge_bg = bg;
	user_config.badge_fg = fg;
}

function receiveUserInfo()
{
	var form = document.getElementById("userInfoNameField");
	var dialog = document.getElementById("userInfoScreen");
	var name = form.value.slice(0,16);
	if(name !== "")
	{
		user_config.user_name = name;
		var user_info_el = document.getElementById("userInfoScreen");
		user_info_el.innerHTML = "";
		saveUserConfig();
		sendUserConfig(user_config);
	}
	else
	{
		var warning = document.getElementById("dialogWarning");
		warning.innerHTML="Please set your name."
	}
	setupWhiteboard();
	return "";
}

function saveUserConfig()
{
	setCookie("zauboard-config",
		"&user_name="  + user_config.user_name
		+ "&badge_bg=" + user_config.badge_bg
		+ "&badge_fg=" + user_config.badge_fg);
}

function setupWhiteboard()
{
	whiteboard.settings.badge_bg = user_config.badge_bg;
	whiteboard.settings.badge_fg = user_config.badge_fg;
	whiteboard.settings.user_name = user_config.user_name;
	whiteboard.settings.communication = communication;
	my_username = user_config.user_name;
	setupKeyboardShortcuts();
  keymage.setScope("whiteboard");
}
function setupWhiteboardSettings()
{
	var track = getCookieValue("track");
	if(track === "false")
	{
		whiteboard.setTracking(false);
		topBar.followOthersOff();
	}
	else
	{
		whiteboard.setTracking(true);
		topBar.followOthersOn();
	}
	//
	var zooming = getCookieValue("zooming");
	whiteboard.setZooming(!(zooming === "false"));
	topBar.setZooming(!(zooming === "false"));
	//
	var centerOnDrawing = getCookieValue("centerOnDrawing");
	whiteboard.setCenterOnDrawing(centerOnDrawing === "true");
	topBar.setCenterOnDrawing(centerOnDrawing === "true");
}

function sendUserConfig(user_config)
{
  communication.setTag(user_config.user_name, user_config.badge_fg, user_config.badge_bg);
  whiteboard.setOwnTag(user_config);
}

function reconnectLoop()
{
  var now = new Date().getTime();
  if (!accessDenied)
  {
    lastReconnect = now;
    accessDenied = true;
    // try to reconnect
    reloadWhiteboard();//joinWhiteboard();
  }
  else if(now - lastReconnect > 5000)
  {
    showBasicAlert("Access denied! Wrong accessToken!");
  }
}

function joinOnLogin()
{
  accessDenied = false;
  if(this.state === "open")
  {
    sendUserConfig(user_config);
    if(typeof board_key === 'undefined')
    {
      this.state = "join-any";
      this.listWhiteboards();
    }
    else
    {
      this.state = "joined";
      this.joinWhiteboard(board_key, page_number);
    }
  }
}

function afterLogin()
{
	loginMenu.popout();
	communication.onAccessDenied  = reconnectLoop;
	communication.onAccessGranted = joinOnLogin;
  if(typeof user_key === 'undefined' || user_key === "")
    communication.requestUserToken();
	setupWhiteboard();
	sendUserConfig(user_config);
	if(typeof user_config.badge_bg === 'undefined')
	{
		keymage.popScope("whiteboard");
		settingsMenu.popup(user_config, false);
		if(typeof board_key === "string")
		{
			communication.joinWhiteboard(board_key, page_number);
		}
	}
	else
	{
		if(typeof board_key === "string")
		{
			communication.joinWhiteboard(board_key, page_number);
		}
		else
		{
			communication.listWhiteboards();
		}
	}
}

function dropFile(e)
{
	e.preventDefault();
	if(filetype(e.dataTransfer.files[0]["name"]) === "pdf")
	{
		uploadPDF(e.dataTransfer.files[0]);
	}
}

function uploadPDF(file_obj)
{
	if(file_obj !== undefined)
	{
    keymage.popScope("whiteboard");
		importPDFMenu.popup(page_number);
		var reader = new FileReader();
		reader.onloadend = function()
		{
			// We need to remove the Data-URL declaration from the start before sending
			communication.uploadPDF(reader.result.slice("data:application/pdf;base64,".length,reader.result.length));
		}
		reader.readAsDataURL(file_obj);
	}
}

function filetype(fname)
{
	var ftype = "";
	for(var i = fname.length - 1 ; i >= 0 ; i--)
	{
		if(fname[i] === '.')
		{
			return fname.slice(i+1,fname.length);
		}
	}
}

function updateSettings()
{
	user_config = settingsMenu.getSettings(user_config);
	saveUserConfig();
	sendUserConfig(user_config);
	var change_password = settingsMenu.getNewPassword();
	if(typeof(change_password) !== 'undefined')
	{
		communication.onSuccess = function()
		{
			showBasicAlert("Password changed successfully.");
			settingsMenu.popout();
			keymage.setScope("whiteboard");
			if(typeof board_key !== "string" || board_key === "")
			{
				communication.listWhiteboards();
			}
			communication.onSuccess = function(){};
		};
		communication.changePassword(change_password.current, change_password.new_pass);
	}
	else
	{
		settingsMenu.popout();
		keymage.setScope("whiteboard");
		if(typeof board_key !== "string" || board_key === "")
		{
			communication.listWhiteboards();
		}
	}
}

function logout()
{
	board_key = undefined;
	user_key = undefined;
	topBar.hideSubmenus();
	communication.close();
	setCookie("zauboard-key","");
	keymage.popScope("whiteboard");
	loginMenu.popup();
}

function importPDFButton()
{
	var options = importPDFMenu.getImportOptions();
	if(options.pdf_ready)
	{
		communication.importPDF(options.pdfID, options.to_whiteboard_page, options.pdf_pages, options.scale, options.single_page);

		importPDFMenu.popout();
		keymage.setScope("whiteboard");
	}
}

function cancelImportPDFButton()
{
	importPDFMenu.popout();
	keymage.setScope("whiteboard");
}

function setThickness(thickness, i)
{
	whiteboard.setStrokeThickness(thickness);
	if(typeof i !== 'undefined')
		topBar.setThickness(i);
}

function nextPage()
{
	if(pageLock)
		communication.switchPage(page_number + 1);
	page_number++;
	setNewUrl(url, board_key, page_number);
	topBar.setPage(page_number);
	reloadWhiteboard();
}

function previousPage()
{
	if(page_number > 1)
	{
		if(pageLock)
		{
			communication.switchPage(page_number - 1);
		}
		page_number--;
		setNewUrl(url, board_key, page_number);
		topBar.setPage(page_number);
		reloadWhiteboard();
	}
}

function undo()
{
	whiteboard.undoWhiteboardClick();
}

function toggleFollowOthers()
{
	var tracking_on = whiteboard.toggleTracking();
	if(tracking_on)
	{
		topBar.followOthersOn();
		setCookie("track","true");
	}
	else
	{
		topBar.followOthersOff();
		setCookie("track","false");
	}
}

function openWhiteboardMenu()
{
	topBar.openWhiteboardMenu();
}
function closeWhiteboardList()
{
  whiteboardMenu.popout();
}
function openUserMenu()
{
	topBar.openUserMenu();
}
function openViewMenu()
{
	topBar.openViewMenu();
}
function toggleZooming()
{
	var zooming = whiteboard.toggleZooming();
	topBar.setZooming(zooming);
	setCookie("zooming",zooming ? "true" : "false");
}
function toggleCenterOnDrawing()
{
	var flag = whiteboard.toggleCenterOnDrawing();
	topBar.setCenterOnDrawing(flag);
	setCookie("centerOnDrawing",flag ? "true" : "false");
}
function settings()
{
	topBar.hideSubmenus();
	keymage.popScope("whiteboard");
	settingsMenu.popup(user_config, true);
}
function listWhiteboards()
{
	topBar.hideSubmenus();
  communication.onWhiteboards = function(whiteboards)
  {
		whiteboardMenu.onWhiteboards(whiteboards);
    keymage.popScope("whiteboard");
		whiteboardMenu.popup();
  }
	communication.listWhiteboards();
}
function openExportPDF()
{
	topBar.hideSubmenus();
	keymage.popScope("whiteboard");
	communication.onFileReady = (file_id, path) => {
		exportPDFMenu.onPDFReady(url + '/' + path);
	}
	communication.onWhiteboards = (whiteboards) => {
		exportPDFMenu.popup(whiteboards[0].pages);
	}
	communication.getWhiteboardInfo(board_key);
	
}
function exportPDF()
{
	exportPDFMenu.setStatusExporting();
	var options = exportPDFMenu.getExportOptions();
	if(options.ready === true)
	{
		communication.exportPDF(options.pages);
	}
}
function cancelExportPDF()
{
	exportPDFMenu.popout();
	keymage.setScope("whiteboard");
}
function eraseWhiteboard()
{
	topBar.confirmEraseWhiteboard();
}
function confirmEraseWhiteboard()
{
	topBar.eraseWhiteboard();
	topBar.hideSubmenus();
	whiteboard.localUser.clear();
}
function closeDebug()
{
	debugMenu.popout();
	keymage.setScope("whiteboard");
}
function openDebug()
{
	topBar.hideSubmenus();
	keymage.popScope("whiteboard");
	debugMenu.popup();
}
function clearDebugLog()
{
	debugMenu.clear();
}
function copyDebugLog()
{
	var str = "";
	for(let t in window.debug.logs)
	{
		str += window.debug.logs[t];
		str += "\n======\n";
	}
	copyToClipboard(str);
}
function hoverSubmenu()
{
	topBar.hoverSubmenu();
}
function leaveSubmenu()
{
	topBar.leaveSubmenu();
}

function copyShareLink()
{
  topBar.hideSubmenus();
  var url = window.location.href;
  var s = url.indexOf("&username=") !== -1 ? "&username=" : "username="; //Remove username from url
  var urlSlpit = url.split(s);
  var urlStart = urlSlpit[0];
  if (urlSlpit.length > 1) {
    var endSplit = urlSlpit[1].split("&");
    endSplit = endSplit.splice(1, 1);
    urlStart += "&" + endSplit.join("&");
  }
  $("<textarea/>").appendTo("body").val(urlStart).select().each(function () {
    document.execCommand('copy');
  }).remove();
  showBasicAlert("Copied Whiteboard-URL to clipboard.", { hideAfter: 2 })
}
// OLD

/*function leaveBoard() {
		signaling_socket.emit('leaveWhiteboard', { });
}*/

/*function joinWhiteboard(socket)
{
	if(typeof board_key === 'string')
		signaling_socket.emit('joinWhiteboard', { board_key: board_key, user_key: user_key, page: page_number, windowWidthHeight: { w: $(window).width(), h: $(window).height() } });
	else
		signaling_socket.emit('joinWhiteboard', { owner: owner, whiteboard_name: whiteboard_name, page: page_number, access_token: access_token, windowWidthHeight: { w: $(window).width(), h: $(window).height() } });
}*/

