export class Debugger
{
	constructor(name, active)
	{
		this.name = name;
		this.available_levels = ["panning", "pointer", "viewport"];
		this.debug_level = {};
		this.active = active;
		this.clear();
	}

	clear()
	{
		this.logs = {};
		for(let t in this.debug_level)
		{
			this.logs[t] = '';
		}
	}

	disable()
	{
		this.active = false;
		this.log = function(){};
		this.logValues = function(){};
	}

	enableLevel(level)
	{
		this.debug_level[level] = true;
	}

	disableLevel(level)
	{
		delete this.debug_level[level];
	}
	toggleLevel(level)
	{
		if(level in this.debug_level)
			this.disableLevel(level);
		else
			this.enableLevel(level);
	}


	log(type, msg)
	{
		if(type in this.debug_level)
		{
			this.logs[type] += (new Date()).getTime() + "," + type + "," + msg + "\n";
		}
	}

	logValues(type, values)
	{
		if(type in this.debug_level)
		{
			var msg = [];
			for(let v in values)
			{
				msg.push(v + " = " + values[v]);
			}
			this.log(type, msg.join(", "));
		}
	}
}
