import {ActionType, DrawingElement} from "./drawingElement.js";

function finishStart(painter)
{
	painter.svgElement.innerHTML = painter.bufferElement.innerHTML;
	painter.bgElement.innerHTML = '';
	painter.bufferElement.innerHTML = '';
	painter.canvas = painter.svgElement;
	painter.finished = true;
	painter.redrawEverything();
}

export class SVGPainter
{
	constructor(custom_attributes)
	{
		// whiteboard canvas
		this.lineCap = "round";
		this.svgns = "http://www.w3.org/2000/svg";
		this.svgDiv         = document.createElement('div');
		this.bgElement      = document.createElement('div');
		this.overlayElement = document.createElementNS(this.svgns, 'svg');
		this.svgElement     = document.createElementNS(this.svgns, 'svg');
		if(typeof custom_attributes.svg_div === 'undefined')
		{
			this.svgDiv.setAttribute('class', 'drawing-container');
		}
		else
		{
			for(let attr in custom_attributes.svg_div)
			{
				this.svgDiv.setAttribute(attr, custom_attributes.svg_div[attr]);
			}
		}
		if(typeof classes.background_element === 'undefined')
		{
			this.bgElement.setAttribute('id', 'whiteboard_BG_SVG');
		}
		else
		{
			for(let attr in custom_attributes.background_element)
			{
				this.bgElement.setAttribute(attr, custom_attributes.bgElement[attr]);
			}
		}
		if(typeof attributes.overlay_element === 'undefined')
		{
			this.overlayElement.setAttribute('id', 'overlaySVG');
			this.overlayElement.setAttribute('width', '100%');
			this.overlayElement.setAttribute('height', '100%');
			this.overlayElement.setAttribute('style', 'position:absolute;');
		}
		else
		{
			for(let attr in custom_attributes.overlay_element)
			{
				this.overlayElement.setAttribute(attr, custom_attributes.overlayElement[attr]);
			}
		}
		if(typeof attributes.svg_element === 'undefined')
		{
			this.svgElement.setAttribute('id', 'whiteboardSVG');
			this.svgElement.setAttribute('width', '100%');
			this.svgElement.setAttribute('height', '100%');
			this.svgElement.setAttribute('style', 'position:absolute;');
		}
		else
		{
			for(let attr in custom_attributes.svg_element)
			{
				this.svgElement.setAttribute(attr, custom_attributes.svgElement[attr]);
			}
		}
		this.svgDiv.append(this.bgElement);
		this.svgDiv.append(this.svgElement);
		this.svgDiv.append(this.overlayElement);
		this.element_ids = {};
		this.numElements = 0;
	}
	resize(width, height)
	{
		this.redrawEverything();
	}
	start(whiteboardContainer, viewport)
	{
		this.svgElement.innerHTML = '';
		this.bgElement.innerHTML = '';
		whiteboardContainer.append(this.svgDiv);
		this.canvas = this.svgElement;
		this.viewport = viewport;
		this.drawBuffer = [];
		this.finished = true;
		this.element_ids = {};
		this.numElements = 0;
		this.redrawEverything();
	}
	applyZoom(zoomDiff, worldX, worldY)
	{
		var oldZoomLog = this.viewport.zoom_log;
		this.viewport.zoom_log = this.viewport.zoom_log + zoomDiff;
		if(this.viewport.zoom_log < -3)
			this.viewport.zoom_log = -3;
		else if(this.viewport.zoom_log > 3)
			this.viewport.zoom_log = 3;
		zoomDiff = this.viewport.zoom_log - oldZoomLog;
		this.viewport.zoom = Math.pow(2, this.viewport.zoom_log);
		var zoomRatio = Math.pow(2, -zoomDiff);
		this.viewport.x = (this.viewport.x - worldX) * zoomRatio + worldX;
		this.viewport.y = (this.viewport.y - worldY) * zoomRatio + worldY;
	}
	redrawEverything()
	{
		var w = this.viewport.width / this.viewport.zoom;
		var h = this.viewport.height / this.viewport.zoom;
		this.canvas.setAttribute('viewBox',
			[ this.viewport.x - w/2
			, this.viewport.y - h/2
			, w
			, h
			].join(' '));
		this.overlayElement.setAttribute('viewBox',
			[ this.viewport.x - w/2
			, this.viewport.y - h/2
			, w
			, h
			].join(' '));
		var bgElements = this.bgElement.children;
		for(var i = 0 ; i < bgElements.length ; ++i)
		{
			bgElements[i].setAttribute('viewBox',
				[ this.viewport.x - w/2
				, this.viewport.y - h/2
				, w
				, h
				].join(' '));
		}
	}

	remove(user_id, draw_id)
	{
		if(user_id in this.element_ids)
		{
			var user = this.element_ids[user_id];
			if(draw_id in user)
			{
				var svg_ids = user[draw_id];
				for(var j = 0 ; j < svg_ids.length ; ++j)
				{
					var svg_el = document.getElementById("svg-" + svg_ids[j].svg_id);
					svg_el.parentNode.removeChild(svg_el);
				}
				delete user[draw_id];
			}
		}
	}

	removeElements(drawings)
	{
		for(var i = 0 ; i < drawings.length ; ++i)
		{
			this.remove(drawings[i].user_id, drawings[i].draw_id);
		}
	}

	bufferReady()
	{
		return true;
	}

	transformContext()
	{
	}    
	eraseRec(fromX, fromY, width, height, id)
	{
		var els = this.drawingContainer.hideWithinRectangle(fromX, fromY, width, height);
		for(var i = 0 ; i < els.length ; ++i)
		{
			var el = els[i];
			this.remove(el.user_id, el.draw_id);
		}
	}
	draw(el, el_id)
	{
		if(typeof el_id === 'undefined'/* && el.type !== ActionType.eraser*/)
		{
			el_id = this.numElements;
			this.numElements++;
			var draw_id = el.draw_id;
			var user_id = el.user_id;
			if(!(user_id in this.element_ids))
				this.element_ids[user_id] = {};
			var user = this.element_ids[user_id];
			if(!(draw_id in user))
				user[draw_id] = [];
			user[draw_id].push({"svg_id":el_id});
		}
		switch(el.type)
		{
			//case ActionType.eraser:
			//	this.drawEraserPath(el.points, el.thickness);
			//	break;
			case ActionType.path:
				this.drawPath(el.points, el.color, el.thickness, el_id);
				break;
			case ActionType.curve:
				this.drawCurve(el.points, el.controlPoints, el.color, el.thickness, el_id);
				break;
			case ActionType.circle:
				this.drawCircle(el.points[0][0], el.points[0][1], el.radius, el.color, el.thickness, el_id);
				break;
			case ActionType.rawSvg:
				this.drawRawSvg(el.points[0][0], el.points[0][1], el.width, el.height, el.scale, el.svg, el_id);
		}
	}
	drawPenLine(fromX, fromY, toX, toY, color, thickness, id)
	{
		var svg_line = document.createElementNS(this.svgns, 'line');
		svg_line.setAttribute('stroke', color);
		svg_line.setAttribute('fill', "none");
		svg_line.setAttribute('id', 'svg-' + id);
		svg_line.setAttribute('stroke-linecap', 'round');
		svg_line.setAttribute('x1', fromX);
		svg_line.setAttribute('y1', fromY);
		svg_line.setAttribute('x2', toX);
		svg_line.setAttribute('y2', toY);
		svg_line.setAttribute('stroke-width', thickness);
		this.canvas.append(svg_line);
	}
	drawEraserPath(points,thickness)
	{
		var len = points.length;
		for(var i = 0 ; i < len - 1; ++i)
		{
			var p0 = points[i];
			var p1 = points[i+1];
			var els = this.drawingContainer.hideWithinLine(p0[0],p0[1], p1[0], p1[1], thickness);
			for(var j = 0 ; j < els.length ; ++j)
			{
				this.remove(els[j].user_id, els[j].draw_id);
			}
		}
	}
	drawPath(points, color, thickness, id)
	{
		var len = points.length;
		var group = document.createElementNS(this.svgns, 'g');
		group.setAttribute('id', 'svg-' + id);
		for(var i = 0; i < len - 1 ; ++i)
		{
			var svg_line = document.createElementNS(this.svgns, 'line');
			svg_line.setAttribute('stroke', color);
			svg_line.setAttribute('fill', "none");
			svg_line.setAttribute('stroke-width', thickness[i % thickness.length]);
			svg_line.setAttribute('stroke-linecap', 'round');
			svg_line.setAttribute('x1', points[i][0]);
			svg_line.setAttribute('y1', points[i][1]);
			svg_line.setAttribute('x2', points[i+1][0]);
			svg_line.setAttribute('y2', points[i+1][1]);
			group.append(svg_line);
		}
		this.canvas.append(group);
	}
	drawCurve(points, controlPoints, color, thickness, id)
	{
		var len = points.length;
		var group = document.createElementNS(this.svgns, 'g');
		group.setAttribute('id', 'svg-' + id);
		var svg_curve = document.createElementNS(this.svgns, 'path');
		svg_curve.setAttribute('stroke', 'none');
		svg_curve.setAttribute('fill', color);

		var starting_point = 
				{ thickness : thickness[0] / 2
				, normal : [ -(controlPoints[0][1] - points[0][1])
					         , controlPoints[0][0] - points[0][0]
				           ]
				};
		var l_0 = Math.sqrt( starting_point.normal[0] * starting_point.normal[0]
		                   + starting_point.normal[1] * starting_point.normal[1]);
		if(l_0 !== 0)
		{
			starting_point.normal[0] = starting_point.normal[0] / l_0;
			starting_point.normal[1] = starting_point.normal[1] / l_0;
		}
		else if(len > 1)
		{
			starting_point.normal[0] = -(points[1][1] - points[0][1]);
			starting_point.normal[1] = points[1][0] - points[0][0];
			var l = Math.sqrt( starting_point.normal[0] * starting_point.normal[0]
		                   + starting_point.normal[1] * starting_point.normal[1]);
			if(l === 0)
			{
				starting_point.normal[0] = 1;
				starting_point.normal[1] = 0;
			}
			else
			{
				starting_point.normal[0] = starting_point.normal[0] / l;
				starting_point.normal[1] = starting_point.normal[1] / l;
			}
		}
		else
		{
				starting_point.normal[0] = 1;
				starting_point.normal[1] = 0;
		}

		starting_point.left = [ points[0][0] + starting_point.normal[0] * starting_point.thickness
		                      , points[0][1] + starting_point.normal[1] * starting_point.thickness
		];
		starting_point.right = [ points[0][0] - starting_point.normal[0] * starting_point.thickness
		                       , points[0][1] - starting_point.normal[1] * starting_point.thickness
		];

		var svg_points = 
				[ 'M'
				, starting_point.left[0]
				, ','
				, starting_point.left[1]
				, 'C'
				, controlPoints[0][0] + starting_point.normal[0] * starting_point.thickness
				, ','
				, controlPoints[0][1] + starting_point.normal[1] * starting_point.thickness
				];

		// "left" side of the curve
		for(var i = 1, j = 2; i < len - 2 ; ++i, j+=2)
		{
			var thick = thickness[i % thickness.length] / 2;
			var norm_x_0 = -(controlPoints[j][1] - points[i][1]);
			var norm_y_0 = controlPoints[j][0] - points[i][0];
			var l_0 = Math.sqrt(norm_x_0 * norm_x_0 + norm_y_0 * norm_y_0);
			if(l_0 !== 0)
			{
				norm_x_0 = norm_x_0 / l_0;
				norm_y_0 = norm_y_0 / l_0;
			}
			var norm_x_1 = (controlPoints[j+1][1] - points[i+1][1]);
			var norm_y_1 = -(controlPoints[j+1][0] - points[i+1][0]);
			var l_1 = Math.sqrt(norm_x_1 * norm_x_1 + norm_y_1 * norm_y_1);
			if(l_1 !== 0)
			{
				norm_x_1 = norm_x_1 / l_1;
				norm_y_1 = norm_y_1 / l_1;
			}

			svg_points.push
				( controlPoints[j-1][0] + norm_x_0 * thick
				, ','
				, controlPoints[j-1][1] + norm_y_0 * thick
				, points[i][0] + norm_x_0 * thick
				, ','
				, points[i][1] + norm_y_0 * thick
				, 'C'
				, controlPoints[j][0] + norm_x_0 * thick
				, ','
				, controlPoints[j][1] + norm_y_0 * thick
				);
		}
		// end of the curve
		var end_point = 
				{ thickness : thickness[(len - 2) % thickness.length] / 2
				, normal : [ -(controlPoints[2*len - 4][1] - points[len - 2][1])
					         , controlPoints[2*len - 4][0] - points[len - 2][0]
				           ]
				};
		var l_0 = Math.sqrt( end_point.normal[0] * end_point.normal[0]
		                   + end_point.normal[1] * end_point.normal[1]);
		end_point.control_left  = [controlPoints[2*len - 4][0], controlPoints[2*len - 4][1]];
		end_point.control_right = [controlPoints[2*len - 4][0], controlPoints[2*len - 4][1]];
		if(l_0 !== 0)
		{
			end_point.normal[0] = end_point.normal[0] / l_0;
			end_point.normal[1] = end_point.normal[1] / l_0;
		}
		else if(len > 2)
		{
			end_point.normal[0] = (points[len - 3][1] - points[len - 2][1]);
			end_point.normal[1] = -(points[len - 3][0] - points[len - 2][0]);
			var l = Math.sqrt( end_point.normal[0] * end_point.normal[0]
		                   + end_point.normal[1] * end_point.normal[1]);
			if(l === 0)
			{
				end_point.normal[0] = 1;
				end_point.normal[1] = 0;
			}
			else
			{
				end_point.normal[0] = end_point.normal[0] / l;
				end_point.normal[1] = end_point.normal[1] / l;
			}
			end_point.control_left = 
				[ points[len - 2][0] + (end_point.normal[0] - end_point.normal[1])
				  * end_point.thickness
				, points[len - 2][1] + (end_point.normal[1] + end_point.normal[0])
				  * end_point.thickness
			];
		}
		else
		{
			end_point.normal[0] = 1;
			end_point.normal[1] = 0;
		}
		end_point.left = [ points[len - 2][0] + end_point.normal[0] * end_point.thickness
		                 , points[len - 2][1] + end_point.normal[1] * end_point.thickness
		];
		end_point.right = [ points[len - 2][0] - end_point.normal[0] * end_point.thickness
		                  , points[len - 2][1] - end_point.normal[1] * end_point.thickness
		];

		svg_points.push
				( end_point.control_left[0]
				, ','
				, end_point.control_left[1]
				, end_point.left[0]
				, ','
				, end_point.left[1]
				, 'C'
				, end_point.left[0] + end_point.normal[1] * end_point.thickness
				, ','
				, end_point.left[1] - end_point.normal[0] * end_point.thickness
				, end_point.right[0] + end_point.normal[1] * end_point.thickness
				, ','
				, end_point.right[1] - end_point.normal[0] * end_point.thickness
				, end_point.right[0]
				, ','
				, end_point.right[1]
				, 'C'
				, controlPoints[2*len - 4][0] - end_point.normal[0] * end_point.thickness
				, ','
				, controlPoints[2*len - 4][1] - end_point.normal[1] * end_point.thickness
				);
		// "right" side
		for(var i = len - 3, j = 2 * i; i > 0; --i, j-=2)
		{
			var thick = thickness[i % thickness.length] / 2;
			var norm_x_0 = (controlPoints[j][1] - points[i][1]);
			var norm_y_0 = -(controlPoints[j][0] - points[i][0]);
			var l_0 = Math.sqrt(norm_x_0 * norm_x_0 + norm_y_0 * norm_y_0);
			if(l_0 !== 0)
			{
				norm_x_0 = norm_x_0 / l_0;
				norm_y_0 = norm_y_0 / l_0;
			}
			var norm_x_1 = -(controlPoints[j+1][1] - points[i+1][1]);
			var norm_y_1 = (controlPoints[j+1][0] - points[i+1][0]);
			var l_1 = Math.sqrt(norm_x_1 * norm_x_1 + norm_y_1 * norm_y_1);
			if(l_1 !== 0)
			{
				norm_x_1 = norm_x_1 / l_1;
				norm_y_1 = norm_y_1 / l_1;
			}

			svg_points.push
				( controlPoints[j][0] + norm_x_0 * thick
				, ','
				, controlPoints[j][1] + norm_y_0 * thick
				, points[i][0] + norm_x_0 * thick
				, ','
				, points[i][1] + norm_y_0 * thick
				, 'C'
				, controlPoints[j-1][0] + norm_x_0 * thick
				, ','
				, controlPoints[j-1][1] + norm_y_0 * thick
				);
		}
		svg_points.push
				( controlPoints[0][0] - starting_point.normal[0] * starting_point.thickness
				, ','
				, controlPoints[0][1] - starting_point.normal[1] * starting_point.thickness

				, starting_point.right[0]
				, ','
				, starting_point.right[1]
				, 'C'
				, starting_point.right[0] - starting_point.normal[1] * starting_point.thickness
				, ','
				, starting_point.right[1] + starting_point.normal[0] * starting_point.thickness

				, starting_point.left[0] - starting_point.normal[1] * starting_point.thickness
				, ','
				, starting_point.left[1] + starting_point.normal[0] * starting_point.thickness

				, starting_point.left[0]
				, ','
				, starting_point.left[1]
				);
		svg_curve.setAttribute('d', svg_points.join(' '));
		group.append(svg_curve);
		this.canvas.append(group);
	}
	drawEraserLine(fromX, fromY, toX, toY, thickness)
	{
		var els = this.drawingContainer.hideWithinLine(fromX,fromY, toX, toY, thickness);
		for(var j = 0 ; j < els.length ; ++j)
		{
			this.remove(els[j].user_id, els[j].draw_id);
		}
	}
	drawRec (fromX, fromY, toX, toY, color, thickness)
	{
		var rect = document.createElementNS(this.svgns, 'rect');
		rect.setAttribute('id', 'svg-' + id);
		rect.setAttribute('stroke', color);
		rect.setAttribute('stroke-width', thickness);
		rect.setAttribute('stroke-linecap', 'round');
		rect.setAttribute('fill', 'none');
		rect.setAttribute('x', Math.min(fromX, toX));
		rect.setAttribute('y', Math.min(fromY, toY));
		rect.setAttribute('width', Math.abs(toX - fromX));
		rect.setAttribute('height', Math.abs(toY - fromY));
		this.canvas.append(rect);
	}
	drawCircle (fromX, fromY, radius, color, thickness, id)
	{
		var circle = document.createElementNS(this.svgns, 'circle');
		circle.setAttribute('id', 'svg-' + id);
		circle.setAttribute('stroke', color);
		circle.setAttribute('stroke-width', thickness);
		circle.setAttribute('cx', fromX);
		circle.setAttribute('cy', fromY);
		circle.setAttribute('r', radius);
		circle.setAttribute('fill', 'none');
		this.canvas.append(circle);
	}
	drawRawSvg(x,y,w,h,scale,svg,id)
	{
		var svgEl = document.createElementNS(this.svgns, 'svg');
		svgEl.setAttribute('width', '100%');
		svgEl.setAttribute('height', '100%');
		svgEl.setAttribute('style', 'position:absolute;');
		var group = document.createElementNS(this.svgns, 'g');
		group.setAttribute('id', 'svg-' + id);
		group.setAttribute('transform', 'translate(' + x + ',' + y + ') scale(' + scale + ')');
		group.setAttribute('viewBox', [0,0,w,h].join(' '));
		group.innerHTML = svg;
		svgEl.append(group);
		this.bgElement.append(svgEl);
	}
	clearWhiteboard()
	{
		this.canvas.innerHTML = '';
		this.bgElement.innerHTML = '';
		this.drawBuffer = [];
		this.element_ids = {};
		this.numElements = 0;
	}
}

